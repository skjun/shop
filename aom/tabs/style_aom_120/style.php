<?php
/********************************************************************
Associate-O-Matic Tab Style: aom_120

File is included in css.tpl.php

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

// INFORMATION
$style['Id']        = "aom_120";
$style['Name']      = "AOM Style 120 (Navigation Menu)";
$style['Author']    = "Associate-O-Matic";
$style['Url']       = "http://www.associate-o-matic.com";

$style['Css'] = <<<CSS
#aom_navcontainer_{$style['Id']} {position:relative; top:0; left:0; margin:0 8px 0 0; font-family:{$this->css['TabTextFont']}; font-size:{$this->css['TabTextSize']};}
#aom_navlist_{$style['Id']} {white-space:nowrap; position:relative; float:Left; width:100%; padding:4px; margin:0; list-style:none; background:{$this->css['TabInactiveColor']}; border-top:2px solid {$this->css['TabInactiveBorderColor']}; border-bottom:2px solid {$this->css['TabInactiveBorderColor']};}
#aom_navlist_{$style['Id']} li {float:Left; margin:0 {$this->css['TabSpacing']}px 0 0; padding:0; }
#aom_navlist_{$style['Id']} a {display:block; padding:3px 6px; color:{$this->css['TabInactiveColorInverse']}; text-decoration:none; font-weight:normal; background:{$this->css['TabInactiveColor']}; margin:0; border:1px solid transparent; border-radius:4px; -webkit-border-radius:4px; -moz-border-radius:4px; CCborderRadius:4px; behavior:url('aom/css/PIE.htc');}
#aom_navlist_{$style['Id']} a#aom_navactive_{$style['Id']}:link, #aom_navlist_{$style['Id']} a:hover, #aom_navlist_{$style['Id']} a:active, #aom_navlist_{$style['Id']} a#aom_navactive_{$style['Id']}:visited {background:{$this->css['TabActiveColor']}; color:{$this->css['TabActiveColorInverse']}; border:1px solid {$this->css['TabActiveBorderColor']} !important;}
#aom_navlist_{$style['Id']} a:visited {color:{$this->css['TabInactiveColorInverse']};}
#aom_navlist_{$style['Id']} a:hover, #aom_navlist_{$style['Id']} a:active {color:{$this->css['TabActiveColorInverse']};}
CSS;
?>
