<?php
/********************************************************************
Associate-O-Matic Tab Style: aom_110

File is included in css.tpl.php

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

// INFORMATION
$style['Id']        = "aom_110";
$style['Name']      = "AOM Style 110 (Classic Squared Tabs)";
$style['Author']    = "Associate-O-Matic";
$style['Url']       = "http://www.associate-o-matic.com";

$style['Css'] = <<<CSS
#aom_navcontainer_{$style['Id']} {position:relative; margin:6px 0 0 10px; font-family:{$this->css['TabTextFont']}; font-size:{$this->css['TabTextSize']};}
#aom_navlist_{$style['Id']} {white-space:nowrap; position:relative; float:Left; width:100%; padding:0; margin:0; list-style:none;}
#aom_navlist_{$style['Id']} li {float:Left; margin:0; padding:0;}
#aom_navlist_{$style['Id']} a {display:block; padding:6px 12px; color:{$this->css['TabInactiveColorInverse']}; text-decoration:none; font-weight:normal; background:{$this->css['TabInactiveColor']}; margin:0 {$this->css['TabSpacing']}px 0 0; border-left:1px solid {$this->css['TabInactiveBorderColor']}; border-top:1px solid {$this->css['TabInactiveBorderColor']}; border-right:1px solid {$this->css['TabInactiveBorderColor']};}
#aom_navlist_{$style['Id']} a#aom_navactive_{$style['Id']}:link, #aom_navlist_{$style['Id']} a:hover, #aom_navlist_{$style['Id']} a:active, #aom_navlist_{$style['Id']} a#aom_navactive_{$style['Id']}:visited {background:{$this->css['TabActiveColor']}; color:{$this->css['TabActiveColorInverse']}; border-left:1px solid {$this->css['TabActiveBorderColor']}; border-top:1px solid {$this->css['TabActiveBorderColor']}; border-right:1px solid {$this->css['TabActiveBorderColor']};}
#aom_navlist_{$style['Id']} a:visited {color:{$this->css['TabInactiveColorInverse']};}
#aom_navlist_{$style['Id']} a:hover, #aom_navlist_{$style['Id']} a:active {color:{$this->css['TabActiveColorInverse']};}
CSS;
?>
