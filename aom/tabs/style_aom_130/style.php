<?php
/********************************************************************
Associate-O-Matic Tab Style: aom_130

File is included in css.tpl.php

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

// INFORMATION
$style['Id']        = "aom_130";
$style['Name']      = "AOM Style 130 (Tabs with Background Color)";
$style['Author']    = "Associate-O-Matic";
$style['Url']       = "http://www.associate-o-matic.com";

$style['Css'] = <<<CSS
#aom_navcontainer_{$style['Id']}, #aom_navcontainer_{$style['Id']} ul, #aom_navcontainer_{$style['Id']} ul li, #aom_navcontainer_{$style['Id']} ul li a {
    padding: 0;
    margin: 0;
    line-height: 1;
    font-family: {$this->css['TabTextFont']};
    font-size: {$this->css['TabTextSize']};
}

#aom_navcontainer_{$style['Id']}:before, #aom_navcontainer_{$style['Id']}:after, #aom_navcontainer_{$style['Id']} > ul:before, #aom_navcontainer_{$style['Id']} > ul:after {
    content: '';
    display: table;
}

#aom_navcontainer_{$style['Id']}:after, #aom_navcontainer_{$style['Id']} > ul:after {
    clear: both;
}

#aom_navcontainer_{$style['Id']} ul {
    background:{$this->css['TabInactiveColor']};
    height: 42px;
    vertical-align:bottom;
    border:1px solid {$this->css['TabInactiveColor']};
}

#aom_navcontainer_{$style['Id']} ul li {
    float: left;
    list-style: none;
}

#aom_navcontainer_{$style['Id']} ul li a {
    
    border-radius: 3px 3px 0 0;
    -webkit-border-radius: 3px 3px 0 0;
    -moz-border-radius: 3px 3px 0 0;
    CCborderRadius: 3px 3px 0 0;
    behavior:url('aom/css/PIE.htc');    
    display: block;
    height: 18px;
    padding: 12px 22px 0;
    margin: 4px {$this->css['TabSpacing']}px 0 0;
    text-decoration: none;
    font-size: 15px;
    color: white;
    text-shadow: 0 1px 1px rgba(0, 0, 0, .75);
}

#aom_navcontainer_{$style['Id']} ul li:first-child a {
    margin-left: 20px;  
}

#aom_navcontainer_{$style['Id']} ul li a:hover, #aom_navcontainer_{$style['Id']} ul a#aom_navactive_{$style['Id']} {
    background:{$this->css['TabActiveColor']} !important;
    border-top:1px solid {$this->css['TabActiveBorderColor']};
    border-left:1px solid {$this->css['TabActiveBorderColor']};
    border-right:1px solid {$this->css['TabActiveBorderColor']};
    display: inline-block;
    height: 28px;
    padding: 10px 21px 0;
    color: {$this->css['TabActiveColorInverse']};
    text-shadow: 0 1px 1px rgba(255, 255, 255, .35);
}
CSS;
?>
