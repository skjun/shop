<?php
/********************************************************************
Associate-O-Matic Tab Style: aom_999

File is included in css.tpl.php

The CSS IDs built into AOM for custom tabs are...
aom_navcontainer  (used in the DIV tag)
aom_navlist       (used in the UL tag)
aom_navactive     (used in the selected A link)

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

// INFORMATION
$style['Id']        = "aom_999";
$style['Name']      = "AOM Style 999 (Custom Style Tabs)";
$style['Author']    = "Associate-O-Matic";
$style['Url']       = "http://www.associate-o-matic.com";

/*
For dozens of additional tab styles see the CSS Tab Designer by OverZone Software
http://www.highdots.com/css-tab-designer/


Enter your custom tab CSS
*/
// place your custom CSS between these the two lines below
$style['Css'] = <<<CSS

CSS;
?>

