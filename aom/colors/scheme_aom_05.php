<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 05";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#A30006";
$scheme['AccentColor'] 			= "#F03C02";
$scheme['BgColor'] 				= "#C21A01";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#F03C02";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#A30006";
$scheme['LinkHoverColor'] 		= "#F03C02";
$scheme['LinkVisitedColor'] 	= "#F03C02";
$scheme['TabActiveColor'] 		= "#A30006"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#F03C02"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#A30006"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#F03C02"; // 5.4.0

?>