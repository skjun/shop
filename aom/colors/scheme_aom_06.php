<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 06";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#A92727";
$scheme['AccentColor'] 			= "#77181E";
$scheme['BgColor'] 				= "#DEE7E7";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#77181E";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#77181E";
$scheme['LinkHoverColor'] 		= "#A92727";
$scheme['LinkVisitedColor'] 	= "#A92727";
$scheme['TabActiveColor'] 		= "#A92727"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#77181E"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#A92727"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#77181E"; // 5.4.0

?>