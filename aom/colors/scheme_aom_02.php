<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 02";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#CDBB99";
$scheme['AccentColor'] 			= "#A37E58";
$scheme['BgColor'] 				= "#755C3B";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#A37E58";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#A37E58";
$scheme['LinkHoverColor'] 		= "#CDBB99";
$scheme['LinkVisitedColor'] 	= "#CDBB99";
$scheme['TabActiveColor'] 		= "#CDBB99"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#A37E58"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#CDBB99"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#A37E58"; // 5.4.0

?>