<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 17";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#ED303C";
$scheme['AccentColor'] 			= "#FF9C5B";
$scheme['BgColor'] 				= "#FAD089";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#F5634A";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#ED303C";
$scheme['LinkHoverColor'] 		= "#F5634A";
$scheme['LinkVisitedColor'] 	= "#F5634A";
$scheme['TabActiveColor'] 		= "#ED303C"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#FF9C5B"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#ED303C"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#FF9C5B"; // 5.4.0

?>