<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 10";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#6B5344";
$scheme['AccentColor'] 			= "#EB9F9F";
$scheme['BgColor'] 				= "#F8ECC9";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#F8ECC9";
$scheme['BoxBorderColor'] 		= "#EB9F9F";
$scheme['BoxBgColor'] 			= "#F8ECC9";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#6B5344";
$scheme['LinkHoverColor'] 		= "#EB9F9F";
$scheme['LinkVisitedColor'] 	= "#EB9F9F";
$scheme['TabActiveColor'] 		= "#6B5344"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#EB9F9F"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#6B5344"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#EB9F9F"; // 5.4.0

?>