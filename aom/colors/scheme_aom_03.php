<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 03";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#FFE545";
$scheme['AccentColor'] 			= "#FBB829";
$scheme['BgColor'] 				= "#FEFBAF";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#FFE545";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#FBB829";
$scheme['LinkHoverColor'] 		= "#FFE545";
$scheme['LinkVisitedColor'] 	= "#FFE545";
$scheme['TabActiveColor'] 		= "#FFE545"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#FBB829"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#FFE545"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#FBB829"; // 5.4.0

?>