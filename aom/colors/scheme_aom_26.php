<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 26";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#E97F02";
$scheme['AccentColor'] 			= "#BD1550";
$scheme['BgColor'] 				= "#490A3D";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#E97F02";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#BD1550";
$scheme['LinkHoverColor'] 		= "#E97F02";
$scheme['LinkVisitedColor'] 	= "#E97F02";
$scheme['TabActiveColor'] 		= "#E97F02"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#BD1550"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#E97F02"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#BD1550"; // 5.4.0

?>