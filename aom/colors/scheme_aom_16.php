<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 16";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#0B8C8F";
$scheme['AccentColor'] 			= "#CACF43";
$scheme['BgColor'] 				= "#FCF8BC";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#0B8C8F";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0B8C8F";
$scheme['LinkHoverColor'] 		= "#CACF43";
$scheme['LinkVisitedColor'] 	= "#CACF43";
$scheme['TabActiveColor'] 		= "#0B8C8F"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#CACF43"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#0B8C8F"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#CACF43"; // 5.4.0

?>