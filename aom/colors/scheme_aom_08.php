<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 08";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#0B486B";
$scheme['AccentColor'] 			= "#3B8686";
$scheme['BgColor'] 				= "#A8DBA8";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#0B486B";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0B486B";
$scheme['LinkHoverColor'] 		= "#3B8686";
$scheme['LinkVisitedColor'] 	= "#3B8686";
$scheme['TabActiveColor'] 		= "#0B486B"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#3B8686"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#0B486B"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#3B8686"; // 5.4.0

?>