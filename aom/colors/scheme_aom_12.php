<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 12";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#99173C";
$scheme['AccentColor'] 			= "#DCE9BE";
$scheme['BgColor'] 				= "#EFFFCD";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#99173C";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#99173C";
$scheme['LinkHoverColor'] 		= "#555152";
$scheme['LinkVisitedColor'] 	= "#555152";
$scheme['TabActiveColor'] 		= "#99173C"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#DCE9BE"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#99173C"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#DCE9BE"; // 5.4.0

?>