<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 24";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#535233";
$scheme['AccentColor'] 			= "#A5A36C";
$scheme['BgColor'] 				= "#F5F4D7";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#E0DFB1";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#535233";
$scheme['LinkHoverColor'] 		= "#A5A36C";
$scheme['LinkVisitedColor'] 	= "#A5A36C";
$scheme['TabActiveColor'] 		= "#535233"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#A5A36C"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#535233"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#A5A36C"; // 5.4.0

?>