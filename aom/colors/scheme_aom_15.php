<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 15";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#0E2430";
$scheme['AccentColor'] 			= "#FC3A51";
$scheme['BgColor'] 				= "#E8D5B9";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#0E2430";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0E2430";
$scheme['LinkHoverColor'] 		= "#FC3A51";
$scheme['LinkVisitedColor'] 	= "#FC3A51";
$scheme['TabActiveColor'] 		= "#0E2430"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#FC3A51"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#0E2430"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#FC3A51"; // 5.4.0

?>