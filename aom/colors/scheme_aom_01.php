<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 01";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#DEDEDE";
$scheme['AccentColor'] 			= "#303030";
$scheme['BgColor'] 				= "#4A4A4A";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#DEDEDE";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#050505";
$scheme['LinkHoverColor'] 		= "#4A4A4A";
$scheme['LinkVisitedColor'] 	= "#4A4A4A";
$scheme['TabActiveColor'] 		= "#DEDEDE"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#303030"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#DEDEDE"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#303030"; // 5.4.0

?>