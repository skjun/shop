<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 25";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#202A2C";
$scheme['AccentColor'] 			= "#BD481D";
$scheme['BgColor'] 				= "#CDE8B0";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#BD481D";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#202A2C";
$scheme['LinkHoverColor'] 		= "#81A882";
$scheme['LinkVisitedColor'] 	= "#81A882";
$scheme['TabActiveColor'] 		= "#202A2C"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#BD481D"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#202A2C"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#BD481D"; // 5.4.0

?>