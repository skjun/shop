<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 32";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#F8CA00";
$scheme['AccentColor'] 			= "#E97F02";
$scheme['BgColor'] 				= "#8A9B0F";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#E97F02";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#E97F02";
$scheme['LinkHoverColor'] 		= "#F8CA00";
$scheme['LinkVisitedColor'] 	= "#F8CA00";
$scheme['TabActiveColor'] 		= "#F8CA00"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#E97F02"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#F8CA00"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#E97F02"; // 5.4.0

?>