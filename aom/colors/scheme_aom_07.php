<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 07";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#413D3D";
$scheme['AccentColor'] 			= "#040004";
$scheme['BgColor'] 				= "#C8FF00";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#413D3D";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#040004";
$scheme['LinkHoverColor'] 		= "#413D3D";
$scheme['LinkVisitedColor'] 	= "#413D3D";
$scheme['TabActiveColor'] 		= "#413D3D"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#040004"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#413D3D"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#040004"; // 5.4.0

?>