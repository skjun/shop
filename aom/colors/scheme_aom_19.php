<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 19";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#91204D";
$scheme['AccentColor'] 			= "#E4844A";
$scheme['BgColor'] 				= "#E2F7CE";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#91204D";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#91204D";
$scheme['LinkHoverColor'] 		= "#E4844A";
$scheme['LinkVisitedColor'] 	= "#E4844A";
$scheme['TabActiveColor'] 		= "#91204D"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#E4844A"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#91204D"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#E4844A"; // 5.4.0

?>