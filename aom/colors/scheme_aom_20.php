<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 20";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#FF4242";
$scheme['AccentColor'] 			= "#E1EDB9";
$scheme['BgColor'] 				= "#F0F2EB";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#D4EE5E";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#FF4242";
$scheme['LinkHoverColor'] 		= "#D4EE5E";
$scheme['LinkVisitedColor'] 	= "#D4EE5E";
$scheme['TabActiveColor'] 		= "#FF4242"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#E1EDB9"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#FF4242"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#E1EDB9"; // 5.4.0

?>