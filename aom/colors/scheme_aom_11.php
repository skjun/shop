<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 11";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#99B2B7";
$scheme['AccentColor'] 			= "#948C75";
$scheme['BgColor'] 				= "#D5DED9";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#948C75";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#99B2B7";
$scheme['LinkHoverColor'] 		= "#D5DED9";
$scheme['LinkVisitedColor'] 	= "#D5DED9";
$scheme['TabActiveColor'] 		= "#99B2B7"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#948C75"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#99B2B7"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#948C75"; // 5.4.0

?>