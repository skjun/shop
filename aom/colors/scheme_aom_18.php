<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 18";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#3B8183";
$scheme['AccentColor'] 			= "#ED303C";
$scheme['BgColor'] 				= "#F5634A";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#3B8183";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#3B8183";
$scheme['LinkHoverColor'] 		= "#ED303C";
$scheme['LinkVisitedColor'] 	= "#ED303C";
$scheme['TabActiveColor'] 		= "#3B8183"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#ED303C"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#3B8183"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#ED303C"; // 5.4.0

?>