<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 22";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#C75233";
$scheme['AccentColor'] 			= "#C78933";
$scheme['BgColor'] 				= "#D6CEAA";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#C75233";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#C75233";
$scheme['LinkHoverColor'] 		= "#C78933";
$scheme['LinkVisitedColor'] 	= "#C78933";
$scheme['TabActiveColor'] 		= "#C75233"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#C78933"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#C75233"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#C78933"; // 5.4.0

?>