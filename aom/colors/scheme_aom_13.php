<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 13";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#CBE86B";
$scheme['AccentColor'] 			= "#F2E9E1";
$scheme['BgColor'] 				= "#1C140D";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#CBE86B";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#1C140D";
$scheme['LinkHoverColor'] 		= "#1C140D";
$scheme['LinkVisitedColor'] 	= "#1C140D";
$scheme['TabActiveColor'] 		= "#CBE86B"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#F2E9E1"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#CBE86B"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#F2E9E1"; // 5.4.0

?>