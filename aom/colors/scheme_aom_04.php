<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 04";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#0E4EAD";
$scheme['AccentColor'] 			= "#107FC9";
$scheme['BgColor'] 				= "#0C0F66";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#107FC9";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0E4EAD";
$scheme['LinkHoverColor'] 		= "#107FC9";
$scheme['LinkVisitedColor'] 	= "#107FC9";
$scheme['TabActiveColor'] 		= "#0E4EAD"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#107FC9"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#0E4EAD"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#107FC9"; // 5.4.0

?>