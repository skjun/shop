<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 29";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#B05574";
$scheme['AccentColor'] 			= "#F87E7B";
$scheme['BgColor'] 				= "#DCD1B4";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#F87E7B";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#B05574";
$scheme['LinkHoverColor'] 		= "#F87E7B";
$scheme['LinkVisitedColor'] 	= "#F87E7B";
$scheme['TabActiveColor'] 		= "#B05574"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#F87E7B"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#B05574"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#F87E7B"; // 5.4.0

?>