<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 33";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#DF151A";
$scheme['AccentColor'] 			= "#FD8603";
$scheme['BgColor'] 				= "#F4F328";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#DF151A";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#DF151A";
$scheme['LinkHoverColor'] 		= "#FD8603";
$scheme['LinkVisitedColor'] 	= "#FD8603";
$scheme['TabActiveColor'] 		= "#DF151A"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#FD8603"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#DF151A"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#FD8603"; // 5.4.0

?>