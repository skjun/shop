<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 23";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#FA4B00";
$scheme['AccentColor'] 			= "#CDBDAE";
$scheme['BgColor'] 				= "#1F1F1F";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#FA4B00";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0000CC";
$scheme['LinkHoverColor'] 		= "#1F1F1F";
$scheme['LinkVisitedColor'] 	= "#1F1F1F";
$scheme['TabActiveColor'] 		= "#FA4B00"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#CDBDAE"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#FA4B00"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#CDBDAE"; // 5.4.0

?>