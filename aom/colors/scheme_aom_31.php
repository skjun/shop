<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 31";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#53777A";
$scheme['AccentColor'] 			= "#D95B43";
$scheme['BgColor'] 				= "#ECD078";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#D95B43";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#53777A";
$scheme['LinkHoverColor'] 		= "#D95B43";
$scheme['LinkVisitedColor'] 	= "#D95B43";
$scheme['TabActiveColor'] 		= "#53777A"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#D95B43"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#53777A"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#D95B43"; // 5.4.0

?>