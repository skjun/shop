<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 09";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#C21A01";
$scheme['AccentColor'] 			= "#6B0103";
$scheme['BgColor'] 				= "#1C0113";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#C21A01";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#6B0103";
$scheme['LinkHoverColor'] 		= "#C21A01";
$scheme['LinkVisitedColor'] 	= "#C21A01";
$scheme['TabActiveColor'] 		= "#C21A01"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#6B0103"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#C21A01"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#6B0103"; // 5.4.0

?>