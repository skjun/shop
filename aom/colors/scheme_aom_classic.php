<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM Classic";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#0289C1";
$scheme['AccentColor'] 			= "#333333";
$scheme['BgColor'] 				= "#EAEAEA";
$scheme['BodyBorderColor']		= "#EAEAEA";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#0289C1";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#0289C1";
$scheme['LinkHoverColor'] 		= "#02AEF9";
$scheme['LinkVisitedColor'] 	= "#02AEF9";
$scheme['TabActiveColor'] 		= "#0289C1"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#333333"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#0289C1"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#333333"; // 5.4.0

?>