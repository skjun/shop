<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 34";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#F07241";
$scheme['AccentColor'] 			= "#601848";
$scheme['BgColor'] 				= "#480048";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#601848";
$scheme['BoxBgColor'] 			= "#F07241";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#480048";
$scheme['LinkHoverColor'] 		= "#601848";
$scheme['LinkVisitedColor'] 	= "#601848";
$scheme['TabActiveColor'] 		= "#F07241"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#601848"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#F07241"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#601848"; // 5.4.0

?>