<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 21";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#3E4147";
$scheme['AccentColor'] 			= "#DFBA69";
$scheme['BgColor'] 				= "#FFFEDF";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#3E4147";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#3E4147";
$scheme['LinkHoverColor'] 		= "#2A2C31";
$scheme['LinkVisitedColor'] 	= "#2A2C31";
$scheme['TabActiveColor'] 		= "#3E4147"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#DFBA69"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#3E4147"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#DFBA69"; // 5.4.0

?>