<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 35";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#78A8A8";
$scheme['AccentColor'] 			= "#A8C0C0";
$scheme['BgColor'] 				= "#BD4646";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#78A8A8";
$scheme['BoxBgColor'] 			= "#A8C0C0";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#999999";
$scheme['LinkColor'] 			= "#333333";
$scheme['LinkHoverColor'] 		= "#FFFFFF";
$scheme['LinkVisitedColor'] 	= "#FFFFFF";
$scheme['TabActiveColor'] 		= "#78A8A8"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#A8C0C0"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#78A8A8"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#A8C0C0"; // 5.4.0

?>