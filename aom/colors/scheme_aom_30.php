<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 30";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#FA6900";
$scheme['AccentColor'] 			= "#A7DBD8";
$scheme['BgColor'] 				= "#E0E4CC";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#A7DBD8";
$scheme['BoxBgColor'] 			= "#E0E4CC";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#FA6900";
$scheme['LinkHoverColor'] 		= "#F38630";
$scheme['LinkVisitedColor'] 	= "#F38630";
$scheme['TabActiveColor'] 		= "#FA6900"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#A7DBD8"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#FA6900"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#A7DBD8"; // 5.4.0

?>