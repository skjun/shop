<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 14";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#26ADE4";
$scheme['AccentColor'] 			= "#D1E751";
$scheme['BgColor'] 				= "#FFFFFF";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#000000";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#26ADE4";
$scheme['LinkHoverColor'] 		= "#4DBCE9";
$scheme['LinkVisitedColor'] 	= "#4DBCE9";
$scheme['TabActiveColor'] 		= "#26ADE4"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#D1E751"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#26ADE4"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#D1E751"; // 5.4.0

?>