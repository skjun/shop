<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 28";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#7D9E3C";
$scheme['AccentColor'] 			= "#73581D";
$scheme['BgColor'] 				= "#FFFEC0";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#7D9E3C";
$scheme['BoxBgColor'] 			= "#FFFFFF";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#7D9E3C";
$scheme['LinkHoverColor'] 		= "#73581D";
$scheme['LinkVisitedColor'] 	= "#73581D";
$scheme['TabActiveColor'] 		= "#7D9E3C"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#73581D"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#7D9E3C"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#73581D"; // 5.4.0

?>