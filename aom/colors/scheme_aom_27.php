<?php
// ASSOCIATE-O-MATIC COLOR SCHEME

// INFORMATION
$scheme['Name'] 				= "AOM 27";
$scheme['Author'] 				= "Associate-O-Matic";
$scheme['Url'] 					= "http://www.associate-o-matic.com/colorschemes";

// COLORS (required)
$scheme['MainColor'] 			= "#C0D860";
$scheme['AccentColor'] 			= "#789048";
$scheme['BgColor'] 				= "#F0F0D8";
$scheme['BodyBorderColor']		= "#FFFFFF";
$scheme['BodyBgColor'] 			= "#FFFFFF";
$scheme['BoxBorderColor'] 		= "#C0D860";
$scheme['BoxBgColor'] 			= "#F0F0D8";

// COLORS (optional)
$scheme['TextColor'] 			= "#000000";
$scheme['TextHighlightColor'] 	= "#990000";
$scheme['TextDarkColor'] 		= "#000000";
$scheme['TextLightColor'] 		= "#FFFFFF";
$scheme['LineColor'] 			= "#EAEAEA";
$scheme['LinkColor'] 			= "#789048";
$scheme['LinkHoverColor'] 		= "#C0D860";
$scheme['LinkVisitedColor'] 	= "#C0D860";
$scheme['TabActiveColor'] 		= "#C0D860"; // 5.4.0
$scheme['TabInactiveColor'] 	= "#789048"; // 5.4.0
$scheme['TabActiveBorderColor'] = "#C0D860"; // 5.4.0
$scheme['TabInactiveBorderColor'] = "#789048"; // 5.4.0


?>