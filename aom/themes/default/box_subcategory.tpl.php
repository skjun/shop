<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_subcategory.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['Subcategories']) ?>
<?php $idx = 1 ?>
<ul class="aom_sub">
<?php /* custom subcategories */ ?>
<?php if (isset($this->aom->site['Subcategories']['Custom'])): ?>
<?php foreach ($this->aom->site['Subcategories']['Custom'] AS $snode => $scat): ?>
		<li class="l1"><span class="aom_bb"><?php echo $this->aom->site['Bullet'] ?></span><span class="aom_list"><a href="<?php echo $scat['Url'] ?>"<?php echo $this->aom->nofollow['SB'] ?>><?php echo $scat['Name'] ?></a></span></li>
		<?php if (isset($this->aom->site['Subcategories']['PerColumn']) AND $idx%$this->aom->site['Subcategories']['PerColumn']==0): ?>
			</ul><ul class="aom_sub aom_spacer">
		<?php endif; ?>
		<?php $idx++ ?>
<?php endforeach; ?>
<?php endif; ?>
<?php /* amazon subcategories */ ?>
<?php if (isset($this->aom->site['Subcategories']['Amazon'])): ?>
<?php foreach ($this->aom->site['Subcategories']['Amazon'] AS $snode => $scat): ?>
	<?php if (isset($scat['Name'])): ?>
			<span class="aom_bb"><?php echo $scat['Name'] ?></span>
	<?php endif; ?>	
	<?php foreach ($scat['Children'] AS $child): ?>
			<li class="l1"><span class="aom_bb"><?php echo $this->aom->site['Bullet'] ?></span><span class="aom_list"><a href="<?php echo $child['Url'] ?>"<?php echo $this->aom->nofollow['SB'] ?>><?php echo $child['Name'] ?></a></span></li>
			<?php if (isset($this->aom->site['Subcategories']['PerColumn']) AND $idx%$this->aom->site['Subcategories']['PerColumn']==0): ?>
				</ul><ul class="aom_sub aom_spacer">
			<?php endif; ?>
			<?php $idx++ ?>
	<?php endforeach; ?>
<?php endforeach; ?>
<?php endif; ?>
</ul>
<div style="clear:both;"></div>