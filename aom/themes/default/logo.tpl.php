<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: logo.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php if (!empty($this->aom->site['Logo'])): ?>
<div id="aom_logo">
<a href="<?php echo $this->aom->buildUrl() ?>"><img alt="<?php echo $this->aom->site['SiteName'] ?>" src="<?php echo $this->aom->site['Logo'] ?>" border="0"></a>
</div>
<?php elseif (isset($this->aom->site['TextLogo']['Text'])): ?>
<div id="aom_logo">
<p class="aom_tl" align="<?php echo $this->aom->site['LogoAlignment'] ?>"><?php echo $this->aom->site['TextLogo']['Text'] ?></p>
</div>
<?php endif; ?>
