<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: tabs_custom.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Tabs']) ?>

<?php // TABS: CUSTOM ?>
<div id="aom_navcontainer">
	<ul id="aom_navlist">
	<?php foreach ($this->aom->t['Tabs'] as $tab): ?>
		<?php if ($tab['Active']=="Y"): ?>
			<li id="aom_navactive"><a href="<?php echo $tab['Url'] ?>"><span><?php echo $tab['Name'] ?></span></a></li>
		<?php else: ?>
			<li><a href="<?php echo $tab['Url'] ?>"><span><?php echo $tab['Name'] ?></span></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
</div>

