<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_information.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['InformationBox']) ?>

<ul>
<?php foreach($this->aom->site['InformationBox']['Link'] AS $link): ?>
	<li class="l1"><span class="aom_bb"><?php echo $this->aom->site['Bullet'] ?></span><span class="aom_list"><a href="<?php echo $link['Url'] ?>"<?php echo $this->aom->nofollow['IB'] ?><?php echo (isset($link['Target']) ? " target=\"".$link['Target']."\"" : "") ?>><?php echo $link['Label'] ?></a></span></li>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>