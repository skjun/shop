<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: home_mall.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['HomeMall']) ?>

<?php
$columns = 3;
$per_column = ceil($this->aom->cat_cnt/$columns)+1;
$index = 1;
$counter = 0;
?>

<?php if (isset($this->aom->t['HomeWelcomeHtml'])) : ?>
	<div class="aom_home_welcome"><?php echo $this->aom->t['HomeWelcomeHtml'] ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>
<?php if (isset($this->aom->t['HomeWelcomeFile'])) : ?>
	<div class="aom_home_welcome"><?php include($this->aom->t['HomeWelcomeFile']) ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>

<div id="aom_home_mall">
<?php foreach ($this->aom->t['HomeMall'] AS $category): ?>
		
	<?php if ($index == 1 OR $index%$per_column == 0): ?>
		<div class="aom_home_mall_col">
	<?php endif; ?>		
		
		<div class="aom_home_mall_cat">
		<?php if (isset($category['Icon'])): ?>
			<img src="<?php echo $category['Icon'] ?>" alt="" style="margin-right:10px;">
		<?php endif; ?>
		<a class="aom_il" href="<?php echo $category['Url'] ?>"><?php echo $category['Name'] ?></a>
		<?php if (isset($category['Subcategory'])): ?>
			<ul> 
			<?php foreach ($category['Subcategory'] AS $subcategory): ?>
				<li><span class="aom_st"><a href="<?php echo $subcategory['Url'] ?>"<?php echo $this->aom->nofollow['S'] ?>><?php echo $subcategory['Name'] ?></a></span></li>
			<?php endforeach; ?>
				<li><span class="aom_st"><a href="<?php echo $category['Url'] ?>"><?php echo $this->aom->str['270'] ?>...</a></span></li>
			</ul>
		<?php endif; ?>	
		
		<?php $counter++ ?>
		</div>

	<?php $index++; ?>
	
	<?php if ($index%$per_column==0 OR $counter == $this->aom->cat_cnt): ?>
		</div>
		<?php $index=1; ?>
	<?php endif; ?>
<?php endforeach; ?>
</div>
<div style="clear:both;"></div>