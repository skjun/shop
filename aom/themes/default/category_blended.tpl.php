<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: category_blended.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['CategoryBlended']) ?>
<?php $blended = $this->aom->t['CategoryBlended'] ?>

<div class="aom_category_blended<?php echo (isset($blended['Id']) ? " ".$blended['Id'] : "") ?>">
	<?php if (isset($blended['Name'])): ?><h1 id="aom_title_blended"><?php echo $blended['Name'] ?></h1><?php endif; ?>
	<?php $this->aom->displayContentVertical("C1"); ?>
	<?php if (isset($blended['Description'])): ?>
		<div><p><?php echo $blended['Description'] ?></p></div>
	<?php endif; ?>
	<?php foreach ($blended['Categories'] AS $category): ?>
		<div class="aom_category_blended">
		<?php if (isset($category['Icon'])): ?>
		<img src="<?php echo $blended['Icon'] ?>" alt="<?php echo $category['Name'] ?>">
		<?php endif; ?>
		<div><span class="aom_slb"><?php echo $category['Name'] ?></span> <a href="<?php echo $category['Url'] ?>"><?php echo $this->aom->str['29'] ?></a></div>
		<?php foreach ($category['Items'] AS $item): ?>
			<?php $this->aom->displayItem($item, "blended") ?>
		<?php endforeach; ?>
		<div style="clear:both;"></div>
		</div>
		<div class="aom_hr"></div>
	<?php endforeach; ?>
</div>
