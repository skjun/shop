<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: wordpress_item.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //print_r($this->t) ?>

<div style="width:100%; margin:3px 0; padding:6px; border:1px solid #eaeaea; text-align:left; clear:both; font-family:arial; font-size:12px;">
	<div style="clear:both;">
		<ul style="list-style:none; float:left; margin:0 0 0 -30px; padding:0;">
			<?php if (isset($this->t['Image']['Url'])): ?>
			<li style="width:<?php echo (isset($this->t['Image']['Width']) ? $this->t['Image']['Width']."px" : "auto" ) ?>; list-style-type:none; float:left; padding:0;"><a href="<?php echo $this->t['Link'] ?>"><img src="<?php echo $this->t['Image']['Url'] ?>"
				<?php if (isset($this->t['Image']['Width'])): ?>
				 width="<?php echo $this->t['Image']['Width'] ?>"
				<?php endif; ?>
				<?php if (isset($this->t['Image']['Height'])): ?>
				 height="<?php echo $this->t['Image']['Height'] ?>"
				<?php endif; ?>></a>
			</li>
			<?php endif; ?>
			<li style="list-style-type:none; float:left; margin-left:8px;" valign="top">
			<p style="font-size:14px; font-weight:bold; margin-bottom:8px; padding:0;"><a href="<?php echo $this->t['Link'] ?>"><?php echo $this->t['Title'] ?></a></p>
			<?php if (isset($this->t['Price']['BuyLabel']) AND isset($this->t['Price']['BuyPrice'])): ?>
				<?php if (isset($this->t['Price']['ListPrice'])): ?>
					<b><?php echo $this->t['Price']['ListLabel'] ?>:</b> <strike><?php echo $this->t['Price']['ListPrice'] ?></strike><br />
				<?php endif; ?>
				<b><?php echo $this->t['Price']['BuyLabel'] ?>:</b> <?php echo $this->t['Price']['BuyPrice'] ?><br />
				<?php if (isset($this->t['Price']['SavePrice'])): ?>
					 <b><?php echo $this->t['Price']['SaveLabel'] ?>:</b> <?php echo $this->t['Price']['SavePrice'] ?>
				<?php endif; ?>
				<?php if (isset($this->t['Price']['SavePct'])): ?>
				 (<?php echo $this->t['Price']['SavePct'] ?>)<br />
				<?php endif; ?>
				<?php if (isset($this->t['Marketplace']['Price'])): ?>
					<a href="<?php echo $this->t['Link'] ?>"><?php echo $this->t['Marketplace']['Label'] ?></a> <?php echo $this->t['Marketplace']['From'] ?> <?php echo $this->t['Marketplace']['Price'] ?>
				<?php endif; ?>
			<?php endif; ?>
			</li>
		</ul>
	</div>
	<div style="clear:both;"><?php echo $this->t['Description'] ?></div>
</div>
