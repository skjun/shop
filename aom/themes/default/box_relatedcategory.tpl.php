<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_relatedcategory.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['RelatedCategories']) ?>

<ul>
<?php foreach($this->aom->site['RelatedCategories'] AS $rcat): ?>
	<?php $m=0 ?>
	<?php foreach($rcat AS $rc): ?>
		<?php if ($m==0): ?>
			<li class="l1"><span class="aom_bb"><?php echo $this->aom->site['Bullet'] ?></span><a href="<?php echo $rc['Url'] ?>"<?php echo $this->aom->nofollow['RB'] ?>><?php echo $rc['Name'] ?></a></li>
		<?php else: ?>
			<li class="aom_r<?php echo $m ?>"><img src="<?php echo $this->tpath_img ?>/arrow_up.gif" border="0" width="15" height="11" align="middle" alt=""><a href="<?php echo $rc['Url'] ?>"<?php echo $this->aom->nofollow['RB'] ?>><?php echo $rc['Name'] ?></a></li>
		<?php endif; ?>
		<?php $m++; ?>
	<?php endforeach; ?>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>