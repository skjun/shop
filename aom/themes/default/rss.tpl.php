<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: rss.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?><?php //$this->aom->dump($this->aom->t['Rss']) ?>
<?php $rss = $this->aom->t['Rss'] ?>
<?php echo "<?xml version='1.0' encoding='UTF-8'?>" ?>
<rss version="2.0" xmlns:apple-wallpapers="http://www.apple.com/ilife/wallpapers" xmlns:cc="http://web.resource.org/cc/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:g-core="http://base.google.com/ns/1.0" xmlns:g-custom="http://base.google.com/cns/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:georss="http://www.georss.org/georss/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:media="http://search.yahoo.com/mrss/" xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/">
	<channel>
		<title><?php echo $rss['Title'] ?></title>
		<?php if (isset($rss['Link'])): ?><link><?php echo $rss['Link'] ?></link>
		<?php endif; ?>
<description><?php echo $rss['Description'] ?></description>
		<language><?php echo $rss['Language'] ?></language>
		<pubDate><?php echo $rss['PublicationDate'] ?></pubDate>
		<generator><?php echo $rss['Generator'] ?></generator>
		<ttl><?php echo $rss['CacheLifetime'] ?></ttl>
	<?php foreach ($rss['Items'] AS $item): ?>
	<item>
			<title><?php echo $item['Title'] ?></title>
			<link><?php echo $item['Link'] ?></link>
			<description>&lt;table&gt;&lt;tr&gt;<?php if (isset($item['Image']['Url'])): ?>&lt;td width=&quot;50&quot;&gt;&lt;a href=&quot;<?php echo $item['Link'] ?>&quot;&gt;&lt;img src=&quot;<?php echo $item['Image']['Url'] ?>&quot;<?php if (isset($item['Image']['Width'])): ?>width=&quot;<?php echo $item['Image']['Width'] ?>&quot;<?php endif; ?><?php if (isset($item['Image']['Height'])): ?>height=&quot;<?php echo $item['Image']['Height'] ?>&quot;<?php endif; ?>&gt;&lt;/a&gt;&lt;/td&gt;<?php endif; ?>&lt;td width=&quot;5&quot;&gt;&lt;/td&gt;<?php if (isset($item['Price']['BuyLabel']) AND isset($item['Price']['BuyPrice'])): ?>&lt;td valign=&quot;top&quot;&gt;<?php if (isset($item['Price']['ListPrice'])): ?>&lt;b&gt;<?php echo $item['Price']['ListLabel'] ?>:&lt;/b&gt; &lt;strike&gt;<?php echo $item['Price']['ListPrice'] ?>&lt;/strike&gt;&lt;br /&gt;<?php endif; ?>&lt;b&gt;<?php echo $item['Price']['BuyLabel'] ?>:&lt;/b&gt; <?php echo $item['Price']['BuyPrice'] ?>&lt;br /&gt;<?php if (isset($item['Price']['SavePrice'])): ?> &lt;b&gt;<?php echo $item['Price']['SaveLabel'] ?>:&lt;/b&gt; <?php echo $item['Price']['SavePrice'] ?><?php endif; ?><?php if (isset($item['Price']['SavePct'])): ?> (<?php echo $item['Price']['SavePct'] ?>)<?php endif; ?><?php if (isset($item['Marketplace']['Price'])): ?>&lt;br /&gt;&lt;a href=&quot;<?php echo $item['Link'] ?>&quot;&gt;<?php echo $item['Marketplace']['Label'] ?>&lt;/a&gt; <?php echo $item['Marketplace']['From'] ?> <?php echo $item['Marketplace']['Price'] ?><?php endif; ?>&lt;/td&gt;<?php endif; ?>&lt;/tr&gt;&lt;tr&gt;&lt;td colspan=&quot;3&quot;&gt;<?php echo $item['Description'] ?><?php if (isset($item['Footer'])) { echo $item['Footer']; } ?>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</description>
	<?php if (isset($item['PublicationDate'])): ?>
		<pubDate><?php echo $item['PublicationDate'] ?></pubDate>
	<?php endif; ?>
		<guid isPermaLink="false"><?php echo $item['Guid'] ?></guid>
		</item>
	<?php endforeach; ?></channel>
</rss>