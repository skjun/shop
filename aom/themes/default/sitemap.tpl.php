<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: sitemap.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Sitemap']) ?>
<?php $sitemap = $this->aom->t['Sitemap'] ?>

<div id="aom_page">
	<h1><?php echo $sitemap['Title'] ?></h1>
	
	<?php if (isset($sitemap['Categories'])): ?>
		<div><?php echo $this->aom->str['381'] ?></div>
		<ul>
		<?php foreach ($sitemap['Categories'] AS $category): ?>
			<li><a href="<?php echo $category['Url'] ?>"><?php echo $category['Name'] ?></a></li>
			<?php if (isset($category['Subcategories'])): ?>
				<ul>
				<?php // level 1 ?>
				<?php foreach ($category['Subcategories'] AS $s1): ?>
					<li><a href="<?php echo $s1['Url'] ?>"><?php echo $s1['Name'] ?></a></li>
						<?php if (isset($s1['Children'])): ?>
							<ul>
							<?php // level 2 ?>
							<?php foreach ($s1['Children'] AS $s2): ?>
								<li><a href="<?php echo $s2['Url'] ?>"><?php echo $s2['Name'] ?></a></li>
									<?php if (isset($s2['Children'])): ?>
										<ul>
										<?php // level 3 ?>
										<?php foreach ($s2['Children'] AS $s3): ?>
											<li><a href="<?php echo $s3['Url'] ?>"><?php echo $s3['Name'] ?></a></li>
												<?php if (isset($s3['Children'])): ?>
													<ul>
													<?php // level 4 ?>
													<?php foreach ($s3['Children'] AS $s4): ?>
														<li><a href="<?php echo $s4['Url'] ?>"><?php echo $s4['Name'] ?></a></li>
															
													<?php endforeach; ?>
													</ul>
												<?php endif; ?>
										<?php endforeach; ?>
										</ul>
									<?php endif; ?>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
				<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	
	<?php if (isset($sitemap['CustomPages'])): ?>
		<div><?php echo $this->aom->str['99'] ?></div>	
		<ul>
		<?php foreach ($sitemap['CustomPages'] AS $custompage): ?>
			<li><a href="<?php echo $custompage['Url'] ?>"<?php echo (isset($custompage['NoFollow']) ? " rel=\"nofollow\"" : "") ?>><?php echo $custompage['Name'] ?></a></li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
		
</div>