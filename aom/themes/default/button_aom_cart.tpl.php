<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: button_aom_cart.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
$fid = "f".uniqid(rand(), true);
?>
<?php //$this->aom->dump($this->aom->t['Button']) ?>
<?php $button = $this->aom->t['Button'] ?>

<form id="<?php echo $fid ?>" class="aom_form" method="POST" action="<?php echo $this->aom->script; ?>">
<input type="hidden" name="a" value="cartadd">
<input type="hidden" name="asin" value="<?php echo $button['ASIN'] ?>">
<input type="hidden" name="offerlistingid" value="<?php echo $button['OfferListingId'] ?>">

<?php if (isset($button['Quantity'])): ?>
	<span class="aom_sl"><?php echo $this->aom->str['14'] ?></span> <input style="margin:6px 0;" type="text" name="quantity" value="1" size="2">
<?php else: ?>
	<input type="hidden" name="quantity" value="1">
<?php endif; ?>

<?php if (isset($button['InStock'])): ?>
	<span class="aom_sto"><?php echo $button['InStock'] ?></span><br>
<?php endif; ?>
<?php if (isset($button['Image'])): ?>
<input type="image" name="submit" value="<?php echo $this->aom->str['58'] ?>" alt="<?php echo $this->aom->str['58'] ?>" src="<?php echo $button['Image'] ?>">
<?php else: ?>
<div onClick="document.getElementById('<?php echo $fid ?>').submit();" id="<?php echo $button['Id'] ?>"><?php echo $button['Name'] ?></div>
<?php endif; ?>
</form>