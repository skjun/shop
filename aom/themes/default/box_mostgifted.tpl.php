<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_mostgifted.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['MostGifted']) ?>

<ul>
<?php foreach($this->aom->site['MostGifted'] AS $mostgifted): ?>
		<li class="l2">
		<?php if ($mostgifted['ImageUrl']): ?>
			<a href="<?php echo $mostgifted['Url'] ?>"<?php echo $this->aom->nofollow['GB'] ?>><img src="<?php echo $mostgifted['ImageUrl'] ?>" border="0" alt="<?php echo $mostgifted['Title'] ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $mostgifted['Url'] ?>"<?php echo $this->aom->nofollow['GB'] ?>><?php echo $mostgifted['Title'] ?></a>
		</li>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>