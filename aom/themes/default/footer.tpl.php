<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: footer.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<div style="clear:both;"></div>
<script type="text/javascript">
var pmsg = '<?php echo $this->aom->t['DisclaimerPrice']; ?>'
</script>
    <div id="footer" class="mt20">
        <div class="column1080">
             <ul class="aboutlist mt10">
                <li> 
                    LET US HELP YOU
                    <a href="#">Shipping&amp;Returns</a>
                    <a href="#">Order Tracking</a>
                    <a href="#">My Account</a>
                    <a href="#">Gift Cards</a>
                </li>
                <li> 
                    GET GONNECTED
                       <a href="https://www.facebook.com/ourbabyspeedy" target="_blank"><img src="images/facebookicon.png">Facebook</a>
                <a href="https://twitter.com/ourbabyspeedy" target="_blank"><img src="images/twittericon.png"/>Twitter</a>
                <a href="http://www.pinterest.com/ourbabyspeedy" target="_blank"><img src="images/pinticon.png"/>Pinterest</a>
                </li> 
             </ul>
             <a href="#"><img src="images/footerlogo.jpg" alt="baby buy" class="f_l"/></a>
             <div class="contactus mt10">
                <span>JOIN THE FUN</span>
                 Subscribe to stay in the know!
                 <form class="mt20">
                    <input type="text"  placeholder="Enter Email" class="inputstyle01"/><input type="button" value="" class="input-but"/>
                 </form>
             </div>
        </div>
        <div class="footercopy mt20">
            Copyright&nbsp;&copy; 2013-2014 www.buybuybaby.com All Rights Reserved.
        </div>
    </div>
<?php
if (!empty($this->aom->site['SiteTrafficCode'])) {
	echo $this->aom->site['SiteTrafficCode'];
}

// custom footer
if (!empty($this->aom->site['SiteFooter'])) {
	$page = $this->aom->page;
	include($this->aom->site['SiteFooter']);
}
// aom footer
else {
?>
<div id="aom_footer">
<?php $this->aom->displayContentVertical("PB2"); ?>
</div>
</body>
</html>
<?php
}
?>
