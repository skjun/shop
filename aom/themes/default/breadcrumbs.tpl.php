<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: breadcrumbs.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<div id="aom_breadcrumbs">
<div class="aom_l"><?php echo $this->aom->str['263'] ?>:</div> <div class="aom_lt"><?php echo $this->aom->page['Breadcrumbs'] ?></div>
<div class="aom_d"><script type="text/javascript">
<!--
displayDate('<?php echo $this->aom->site['AmazonStoreType'] ?>')
//-->
</script></div>
</div>