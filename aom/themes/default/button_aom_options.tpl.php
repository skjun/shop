<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: button_aom_options.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Button']) ?>
<?php $button = $this->aom->t['Button'] ?>

<?php if (isset($button['InStock'])): ?>
	<span class="aom_sto"><?php echo $this->aom->str['309'] ?></span><br>
<?php endif; ?>
<a href="<?php echo $button['Url'] ?>" rel="nofollow">
<?php if (isset($button['Image'])): ?>
<img src="<?php echo $button['Image'] ?>" border="0" alt="<?php echo $this->aom->str['32'] ?>">
<?php else: ?>
<div id="<?php echo $button['Id'] ?>"><?php echo $button['Name'] ?></div>
<?php endif; ?>
</a>