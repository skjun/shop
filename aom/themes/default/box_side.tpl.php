<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_side.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Box']) ?>
<?php $box = $this->aom->t['Box'] ?>

<div id="<?php echo $box['BoxId'] ?>" class="aom_box_side aom_box_<?php echo $this->aom->t['Box']['Location'] ?>">
	<?php if (!empty($box['Label'])): ?>
		<div class="aom_box_top" style="<?php echo (isset($box['BorderSize']) ? "border-top:".$box['BorderSize']."px solid ".$box['BorderColor']."; border-left:".$box['BorderSize']."px solid ".$box['BorderColor']."; border-right:".$box['BorderSize']."px solid ".$box['BorderColor'].";" : "border:0;") ?> background-color:<?php echo $box['BorderColor'] ?>; color:<?php echo $this->aom->getColor($box['BorderColor']) ?>; font-weight:bold; padding:3px;"><?php echo $box['Label'] ?></div>
	<?php endif; ?>
	<div class="aom_box_bottom" style="<?php echo (isset($box['BorderSize']) ? "border:".$box['BorderSize']."px solid ".$box['BorderColor'].";" : "border:0;") ?> background-color:<?php echo $box['BgColor'] ?>; padding:3px;"><?php $this->aom->displayContentBox($box) ?></div>
</div>
