<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: sort.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Sort']) ?>

<div id="aom_sort">
	<form method="GET" action="<?php echo $this->aom->script ?>">
	<input type="hidden" name="p" value="1">
	<input type="hidden" name="c" value="<?php echo $this->aom->clean['c'] ?>">
	<?php if (isset($this->aom->clean['n'])): ?>	
		<input type="hidden" name="n" value="<?php echo $this->aom->clean['n'] ?>">
	<?php endif; ?>	
	<?php if (isset($this->aom->clean['k'])): ?>	
		<input type="hidden" name="k" value="<?php echo $this->aom->clean['k'] ?>">
	<?php endif; ?>	
	<?php if (isset($this->aom->clean['t'])): ?>	
		<input type="hidden" name="t" value="<?php echo $this->aom->clean['t'] ?>">
	<?php endif; ?>	
	
	<span class="aom_st"><?php echo $this->aom->t['Sort']['Label'] ?></span>
	<select name="s" onChange="this.form.submit()">
	<?php foreach ($this->aom->t['Sort']['List'] AS $key => $val): ?>
		<option value="<?php echo $this->aom->sort_amazon[$key] ?>"<?php echo ((isset($this->aom->clean['s']) AND $this->aom->clean['s']==$this->aom->sort_amazon[$key]) ? " selected" : "") ?>><?php echo $val ?></option>
	<?php endforeach; ?>   
	</select>
	<input type=submit value="<?php echo $this->aom->str['273'] ?>">
	</form>		
</div>