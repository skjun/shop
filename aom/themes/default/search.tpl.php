<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: search.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<div id="aom_search">		
	<?php if ($this->aom->site['SearchAutocomplete']=="Yes"): ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#aom_sbox").autocomplete({ 
		search: function(event, ui){$(this).addClass('ui-autocomplete-loading');},
    	open: function(event, ui){$(this).removeClass('ui-autocomplete-loading');},
		source: '<?php echo $this->aom->script ?>'
	});
});
</script>
	<?php endif; ?>

<form id="aom_sf" name="search" method="GET" action="<?php echo $this->aom->script ?>" style="margin-bottom:0;">
<div id="aom_sleft">
<span class="aom_sbt"><?php echo $this->aom->str['56'] ?></span>
<input id="aom_sbox" type="text" name="k" <?php echo ((isset($this->aom->clean['k']) AND strlen($this->aom->clean['k'])>42) ? " title=\"".$this->aom->clean['k']."\"" : "") ?> value="<?php echo ((isset($this->aom->clean['k']) AND !isset($this->aom->clean['t'])) ? $this->aom->clean['k'] : "") ?>">

<?php if ($this->aom->cat_cnt_menu>1 OR $this->aom->site['SearchWithinResults']=="Yes"): ?>
<select id="c" name="c" class="aom_smenu">
	<?php if (!isset($this->aom->site['DisplaySearchAllOption']) OR $this->aom->site['DisplaySearchAllOption']=="Yes"): ?>	
		
		<?php if (isset($this->aom->site['SearchAllProducts']['Mode']) AND $this->aom->site['SearchAllProducts']['Mode']=="All"): ?>
<option value="all"><?php echo $this->aom->str['261'] ?></option>
		<?php else: ?>
<option value="blended"><?php echo $this->aom->str['261'] ?></option>
		<?php endif; ?>
	<?php endif; ?>
	<?php 
	if ($this->aom->site['SearchWithinResults']=="Yes" AND isset($this->aom->page['Name']) AND !(isset($this->aom->data['Items']['TotalResults']) AND $this->aom->data['Items']['TotalResults']==1) AND isset($this->aom->clean['c']) AND !(isset($GLOBALS['_GET']['c']) AND strstr($GLOBALS['_GET']['c'], "searchwithin")) AND isset($this->aom->clean['n']) AND isset($this->aom->cat[$this->aom->clean['c']]['Node']) AND $this->aom->cat[$this->aom->clean['c']]['Node']!=$this->aom->clean['n']):
	 ?>	
<option value="searchwithin|<?php echo $this->aom->clean['c'] ?>|<?php echo $this->aom->clean['n'] ?>" selected><?php echo $this->aom->t['SearchWithinLabel'] ?></option>
	<?php $si = 1; ?>
	<?php endif; ?>

	<?php foreach ($this->aom->site['Categories']['Category'] as $cat): ?>
		<?php if ($cat['Mode']!="ASIN" AND $cat['Mode']!="ASINFILE" AND $cat['Menu']=="On"): ?>
			<?php $selected = "" ?>
			<?php if (!isset($si) AND !isset($this->aom->app['home']) AND isset($this->aom->clean['c']) AND !(isset($GLOBALS['_GET']['c']) AND $GLOBALS['_GET']['c']=="blended") AND $this->aom->clean['c']==$cat['Id']): ?>
					<?php $selected = " selected" ?>
			<?php endif; ?>
<option value="<?php echo $cat['Id'] ?>"<?php echo $selected ?>><?php echo $cat['Name'] ?></option>
		<?php endif; ?>
	<?php endforeach; ?>
</select>
<?php else: ?>
	<?php if ($this->aom->site['DisplaySearchAllOption']=="Yes"): ?>
		<?php if ($this->aom->site['SearchAllProducts']['Mode']=="All"): ?>
			<input type="hidden" name="c" value="All">
		<?php elseif ($this->aom->site['SearchAllProducts']['Mode']=="Blended"): ?>
			<input type="hidden" name="c" value="Blended">
		<?php endif; ?>
	<?php elseif (isset($this->aom->site['Categories']['Category']) AND isset($this->aom->site['Categories']['Category'][0])): ?>
		<input type="hidden" name="c" value="<?php echo $this->aom->site['Categories']['Category'][0]['Id'] ?>">
	<?php endif; ?>
<?php endif; ?>
<?php if ($this->aom->site['SearchBtnImage']==""): ?>
<input type="submit" value="<?php echo $this->aom->str['316'] ?>">
<?php else: ?>
<input type="image" src="<?php echo $this->aom->site['SearchBtnImage'] ?>">
<?php endif; ?>
<?php if ($this->aom->site['AdvancedSearch']=="Yes"): ?>
&nbsp;<a class="aom_sbt" href="<?php echo $this->aom->t['UrlSearchAdvanced'] ?>"><?php echo $this->aom->str['2'] ?></a>
<?php endif; ?>
</div>
<div id="aom_sright">
<?php if (isset($this->aom->site['DisplaySitemapLink']) AND $this->aom->site['DisplaySitemapLink']=="Yes"): ?>
<a class="aom_sbt" href="<?php echo $this->aom->t['UrlSitemap']; ?>"><?php echo $this->aom->str['380'] ?></a>&nbsp;&nbsp;&nbsp;
<?php endif; ?>
<?php if (isset($this->aom->site['DisplayViewCartLink']) AND $this->aom->site['DisplayViewCartLink']=="Yes"): ?>
<a class="aom_sbt" href="<?php echo $this->aom->t['UrlCartView'] ?>" target="<?php echo $this->aom->t['UrlTarget']; ?>" rel="nofollow"><?php echo $this->aom->str['262'] ?></a>&nbsp;&nbsp;&nbsp;
<?php endif; ?>
<?php if (isset($this->aom->site['DisplayCheckoutLink']) AND $this->aom->site['DisplayCheckoutLink']=="Yes"): ?>
<a class="aom_sbt" href="<?php echo $this->aom->t['UrlCartCheckout'] ?>" target="<?php echo $this->aom->t['UrlTargetCheckout'] ?>" rel="nofollow"><?php echo $this->aom->str['20'] ?></a>&nbsp;&nbsp;&nbsp;
<?php endif; ?>
</div>
</form>
</div>
