<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_custom_file.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['CustomBox']) ?>
<?php $page = $this->aom->page ?>
<?php include ($this->aom->t['CustomBox']['BoxContent']) ?>
<div style="clear:both;"></div>