<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: header.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php if (empty($this->aom->site['SiteDocType'])): ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php else: ?>
<?php echo $this->aom->site['SiteDocType']?>
<?php endif; ?>
<?php if (!empty($this->aom->site['SiteHeader'])): ?>
<?php $page = $this->aom->page?>
<?php include($this->aom->site['SiteHeader'])?>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/jquery.js"></script>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/shop.js"></script>
<?php if ($this->aom->site['SearchAutocomplete']=="Yes"): ?>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/jquery-ui.js"></script>
<?php endif; ?>
<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/lightbox/jquery.colorbox.js"></script>
<link type="text/css" media="screen" rel="stylesheet"
	href="<?php echo $this->aom->path_js; ?>/lightbox/<?php echo $this->aom->site['ImageZoom']; ?>/colorbox.css">
<?php endif; ?>
<?php if ($this->aom->site['CssOverride']=="No"): ?>
<link rel="stylesheet" type="text/css"
	href="<?php echo $this->aom->tpath."/css/".$this->aom->cfg_file_css; ?>">
<?php endif; ?>
<?php if (!empty($this->aom->site['SiteCss'])): ?>
<link rel="stylesheet" type="text/css"
	href="<?php echo $this->aom->site['SiteCss']; ?>">
<?php endif; ?>
<?php if (isset($this->aom->t['Rss']['Url'])): ?>
<link rel="alternate" type="application/rss+xml"
	href="<?php echo $this->aom->t['Rss']['Url'] ?>" title="RSS">
<?php endif; ?>
<?php if (isset($this->aom->t['CanonicalLink'])): ?>
<link rel="canonical"
	href="<?php echo $this->aom->t['CanonicalLink'] ?>">
<?php endif; ?>
<?php else: ?>
<html>
<head>
<title><?php echo $this->aom->page['Title'] ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description"
	content="<?php echo $this->aom->page['MetaDescription'] ?>">
<meta name="keywords"
	content="<?php echo $this->aom->page['MetaKeywords'] ?>">
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/jquery.js"></script>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/shop.js"></script>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/jquery.flexslider-min.js"></script>
<?php if ($this->aom->site['SearchAutocomplete']=="Yes"): ?>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/jquery-ui.js"></script>
<?php endif; ?>
<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
<script type="text/javascript"
	src="<?php echo $this->aom->path_js; ?>/lightbox/jquery.colorbox.js"></script>
<link type="text/css" media="screen" rel="stylesheet"
	href="<?php echo $this->aom->path_js; ?>/lightbox/<?php echo $this->aom->site['ImageZoom']; ?>/colorbox.css">
<?php endif; ?>
<?php if (!empty($this->aom->site['SiteHead'])): ?>
<?php echo $this->aom->site['SiteHead']?>
<?php endif; ?>
<?php if ($this->aom->site['CssOverride']=="No"): ?>
<link rel="stylesheet" type="text/css"
	href="<?php echo $this->aom->tpath."/css/".$this->aom->cfg_file_css; ?>">
<?php endif; ?>
<?php if (!empty($this->aom->site['SiteCss'])): ?>
<link rel="stylesheet" type="text/css"
	href="<?php echo $this->aom->site['SiteCss']; ?>">
<?php endif; ?>
<?php if (isset($this->aom->t['Rss']['Url'])): ?>
<link rel="alternate" type="application/rss+xml"
	href="<?php echo $this->aom->t['Rss']['Url'] ?>" title="RSS">
<?php endif; ?>
<?php if (isset($this->aom->t['CanonicalLink'])): ?>
<link rel="canonical"
	href="<?php echo $this->aom->t['CanonicalLink'] ?>">
<?php endif; ?>
<link href="commonstyle/style.css" type="text/css" rel="stylesheet" />
<link href="commonstyle/reset.css" type="text/css" rel="stylesheet" />

</head>
<body>
<?php endif; ?>

 <div id="header">
		<div class="column1080">
			<div class="site-nav">
				<span class="subnav f_l">
					<ul>
						<li><a href="http://www.babyspeedy.com" target="_blank">Home</a></li>
						<li><a href="http://www.babyspeedy.com/home" target="_blank">Shop</a></li>
						<li><a href="http://like.babyspeedy.com" target="_blank">DJJunMix</a></li>
						<li><a href="http://bbs.babyspeedy.com" target="_blank">Mom's
								Assistant</a></li>
					</ul>
				</span> <span class="site-nav-r f_r">
					<ul>
						<li class="site-log">Welcome&nbsp;&nbsp;<a href="#">(Log In)</a></li>
						<li class="site-Account"><a href="#">My Account</a></li>
						<li class="site-Bag">Bag&nbsp;&nbsp;<a href="#">0</a></li>
					</ul>
				</span>
			</div>
			<a href="#"><img src="images/logov2.gif" class="indexlogo f_l"
				title="babaybuy" alt="babaybuy" /></a>
			<div class="nav-share f_r mt30">
				<form>
					<input type="text" class="inputstyle01" tabindex="1"
						placeholder="Start typing here..." /><input type="button" value=""
						class="input-but" />
				</form>
				<a href="https://www.facebook.com/ourbabyspeedy" target="_blank"><img
					src="images/subscriptionicon.png" /></a> <a
					href="https://www.facebook.com/ourbabyspeedy" target="_blank"><img
					src="images/facebookicon.png"></a> <a
					href="http://www.pinterest.com/ourbabyspeedy" target="_blank"><img
					src="images/pinticon.png" /></a> <a
					href="https://twitter.com/ourbabyspeedy" target="_blank"><img
					src="images/twittericon.png" /></a>
			</div>
		</div>
	</div>
	<nav>
	<div id="nav">
		<div class="column1080">
			<ul>
				<li><a href="http://www.babyspeedy.com/home" class="navactive">Home</a></li>
                 <?php require 'headconfig/pregnant.php'; ?>
                 <?php require 'headconfig/newborn.php'; ?>
                 <?php require 'headconfig/boyclothing.php'; ?>
                 <?php require 'headconfig/girlclothing.php'; ?>
                 <?php require 'headconfig/toy.php'; ?>
                 <?php require 'headconfig/food.php'; ?>
                </ul>
		</div>
	</div>
	</nav>

	<div id="aom_header">
<?php $this->aom->displayContentVertical("PT"); ?>
<?php $this->aom->loadTemplate("logo.tpl.php")?>
<?php /* Tabs */ ?>
<?php if ($this->aom->site['DisplayTabs']=="Yes"): ?>
	<?php $this->aom->loadTemplate("tabs.tpl.php")?>
<?php endif; ?>
<?php /* Search */ ?>
<?php if ($this->aom->site['DisplaySearchBar']=="Yes"): ?>
	<?php $this->aom->loadTemplate("search.tpl.php")?>
<?php endif; ?>
<?php /* Breadcrumbs */ ?>
<?php if (!empty($this->aom->page['Breadcrumbs'])): ?>
	<?php $this->aom->loadTemplate("breadcrumbs.tpl.php")?>
<?php endif; ?>
</div>
	<div style="clear: both;"></div>