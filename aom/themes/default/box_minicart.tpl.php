<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_minicart.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php if (!(isset($GLOBALS['_GET']['a']) AND $GLOBALS['_GET']['a']=="minicart")): ?>
<nolayer>
<iframe id="content" style="background-color:none;" allowtransparency="true" src="<?php echo $this->aom->script ?>?a=minicart" width="100%" height="50" frameborder="0" noresize="noresize" scrolling="no" vspace="0" hspace="0" marginwidth="0" marginheight="0"></iframe>
</nolayer>
<?php else: ?>
<?php //print_r($this->t['MiniCart']) ?>
<?php $minicart = $this->t['MiniCart'] ?>
<html>
<body style="background-color:transparent">
<span style="font-family:arial; font-size:11px;">
<?php if (isset($GLOBALS['_SESSION']['remote_cart']) AND isset($GLOBALS['_SESSION']['remote_cart']['SubTotal'][0]['FormattedPrice']) AND isset($GLOBALS['_SESSION']['remote_cart']['CartItem'])): ?>
	<div><?php echo $minicart['Item']['Label'] ?>: <?php echo $minicart['Item']['Count'] ?></div>
	<div><?php echo $minicart['Subtotal']['Label'] ?>: <?php echo $minicart['Subtotal']['Price'] ?></div>
	<div><a href="<?php echo $minicart['ViewCart']['Url'] ?>" target="_parent" rel="nofollow"><?php echo $minicart['ViewCart']['Label'] ?></a></div>
<?php else: ?>
	<div class="aom_cart_err"><?php echo $this->t['MiniCartError'] ?></div>
<?php endif; ?>
</span>
</body>
</html>
<?php endif; ?>