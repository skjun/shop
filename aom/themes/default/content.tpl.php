<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: content.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>

<?php $this->aom->displayContentVertical("PT2"); ?>
<div id="<?php echo $this->aom->t['content_id'] ?>">
	<div id="aom_body" class="column">
	<?php $this->aom->loadTemplate('content_body.tpl.php', 'display') ?>
	<?php if (isset($this->aom->t['Disclaimer']['Text'])): ?>
		<div style="color:<?php echo $this->aom->t['Disclaimer']['Color'] ?>;" class="aom_dr"><?php echo $this->aom->t['Disclaimer']['Text'] ?></div>
	<?php endif; ?>
	<?php $this->aom->displayPoweredBy() ?>
	<?php $this->aom->displayContentVertical("PB"); ?>
	</div>
	<?php if (isset($this->aom->t['side_left'])): ?>
	<div id="aom_left" class="column">
	<?php $this->aom->displayContentSide("L") ?>
	</div>
	<?php endif; ?>
	<?php if (isset($this->aom->t['side_right'])): ?>
	<div id="aom_right" class="column">
	<?php $this->aom->displayContentSide("R") ?>
	</div>
	<?php endif; ?>
</div>	
