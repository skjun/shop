Note:  If you are planning to make changes to the AOM Default theme, it is strongly recommended that you either make a backup copy of your default template files, or rename the theme altogether. This will ensure that the changes are not overwritten in a following upgrade. The procedure to rename a theme is very simple. For more details please read the article at the AOM Talk blog:

http://www.aomtalk.com/2011/07/the-theme-is-change.html

(special thanks to Michael Carpenter)


