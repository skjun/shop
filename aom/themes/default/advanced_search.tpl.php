<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: advanced_search.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['AdvancedSearch']) ?>
<?php $advanced = $this->aom->t['AdvancedSearch'] ?>

<div id="aom_page">
	<h1><?php echo $advanced['Title'] ?></h1>
	
	<?php foreach ($advanced['Search'] AS $type => $search): ?>
	<div>
		<span class="aom_tr"><?php echo $search['Label'] ?></span>
		<form name="search" method="GET" action="<?php echo $this->aom->script ?>">
		<input type="hidden" name="t" value="<?php echo $type ?>">
		<input type="text" name="k" size="34">
		<?php if ($search['Count']==1):
			$ck = array_keys($search['Category']);
		?>
			<input type="hidden" name="c" value="<?php echo $ck[0] ?>">
		<?php else: ?>
			<select name="c">
			<?php foreach ($search['Category'] AS $category_id => $category_name): ?>
				<option value="<?php echo $category_id ?>"><?php echo $category_name ?></option>
			<?php endforeach; ?>
			</select>
		<?php endif; ?>
		<input type="submit" value="<?php echo $this->aom->str['273'] ?>">
		</form>
							
	</div>
	<?php endforeach; ?>
	
	<div>
		<span class="aom_tr"><?php echo $this->str['243']." / ".$this->str['36'] ?></span>
		<form name="search" method="GET" action="<?php echo $this->aom->script ?>">
		<input type="text" name="i" size="34">
		<input type="submit" value="<?php echo $this->aom->str['273'] ?>">
		</form>
	</div>
</div>