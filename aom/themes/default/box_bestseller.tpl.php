<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_bestseller.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['Bestsellers']) ?>

<ul>
<?php foreach($this->aom->site['Bestsellers'] AS $bestseller): ?>
		<li class="l2">
		<?php if ($bestseller['ImageUrl']): ?>
			<a href="<?php echo $bestseller['Url'] ?>"<?php echo $this->aom->nofollow['BB'] ?>><img src="<?php echo $bestseller['ImageUrl'] ?>" border="0" alt="<?php echo $bestseller['Title'] ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $bestseller['Url'] ?>"<?php echo $this->aom->nofollow['BB'] ?>><?php echo $bestseller['Title'] ?></a>
		</li>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>
