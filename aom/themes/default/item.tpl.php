<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: item.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Item']) ?>
<?php $item = $this->aom->t['Item'] ?>

<?php if (isset($item['Title'])): ?>

<div id="aom_item_wrapper">
<h1><?php echo $item['Title'] ?></h1>
<?php if (isset($item['BestsellerImage'])): ?>
&nbsp;<img align="top" src="<?php echo $item['BestsellerImage'] ?>" alt="">
<?php endif; ?>
<?php if (isset($item['NewReleaseImage'])): ?>
&nbsp;<img align="top" src="<?php echo $item['NewReleaseImage'] ?>" alt="">
<?php endif; ?>
<div id="aom_item">
	<div id="aom_img" style="width:<?php echo $item['ImageColumnWidth'] ?>px;">
		<?php if (isset($item['zoom_lightbox'])): ?>
			<a href="javascript:void(0)" onclick="$.fn.colorbox({href:'<?php echo $item['ImageLarge']['Url'] ?>',open:true, rel:'zoom', title:'<?php echo $item['TitleClean'] ?>'});" title="<?php echo $item['TitleClean'] ?>">
		<?php elseif (isset($item['zoom_popup'])): ?>
			<a href="javascript:void(0)" onclick="popup('<?php echo $item['ImageLarge']['Url'] ?>', <?php echo $item['ImageLarge']['Width'] ?>, <?php echo $item['ImageLarge']['Height'] ?>)" rel="nofollow">
		<?php endif; ?>
		<img src="<?php echo $item['ImageLarge']['Url'] ?>"<?php echo (isset($item['ImageLarge']['Width']) ? " width=\"{$item['ImageLarge']['Width']}\"" : "") ?><?php echo (isset($item['ImageLarge']['Height']) ? " height=\"{$item['ImageLarge']['Height']}\"" : "") ?> alt="<?php echo $item['TitleClean'] ?>">
		<?php if (isset($item['zoom_lightbox']) OR isset($item['zoom_popup'])): ?>
			</a>
		<?php endif; ?>
		<?php if (isset($item['zoom_lightbox'])): ?>
			<div class="aom_z2"><a href="javascript:void(0)" onclick="$.fn.colorbox({href:'<?php echo $item['ImageLarge']['Url'] ?>',open:true, rel:'zoom', title:'<?php echo $item['TitleClean'] ?>'});" title="<?php echo $item['TitleClean'] ?>"><img src="<?php echo $this->aom->tpath_img ?>/zoom.gif" border="0" alt="<?php echo $this->aom->str['31'] ?>" align="middle"></a> <span class="aom_stn"><a href="javascript:void(0)" onclick="$.fn.colorbox({href:'<?php echo $item['ImageLarge']['Url'] ?>',open:true, rel:'zoom', title:'<?php echo $item['TitleClean'] ?>'});" title="<?php echo $item['TitleClean'] ?>"><?php echo $this->aom->str['31'] ?></a></span></div>
		<?php elseif (isset($item['zoom_popup'])): ?>
			<div class="aom_z2"><a href="javascript:void(0)" onclick="popup('<?php echo $item['ImageLarge']['Url'] ?>', <?php echo $item['ImageLarge']['Width'] ?>, <?php echo $item['ImageLarge']['Height'] ?>)" rel="nofollow"><img src="<?php echo $this->aom->tpath_img ?>/zoom.gif" border="0" alt="<?php echo $this->aom->str['31'] ?>" align="middle"></a> <span class="aom_stn"><a href="javascript:void(0)" onclick="popup('<?php echo $item['ImageLarge']['Url'] ?>', <?php echo $item['ImageLarge']['Width'] ?>, <?php echo $item['ImageLarge']['Height'] ?>)" rel="nofollow"><?php echo $this->aom->str['31'] ?></a></span></div>
		<?php endif; ?>

		<?php if (isset($item['SwatchImages'])): ?>
		<div class="aom_swatches">
			<div class="aom_sl"><?php echo $this->aom->str['60'] ?>:</div>
			<?php foreach ($item['SwatchImages'] AS $swatch): ?>
				<span class="aom_swatch">
					<div class="aom_simg">
					<?php if (isset($item['swatch_lightbox'])): ?>
						<a rel="gallery" href="javascript:void(0)" caption="<?php echo $swatch['ImageLarge']['Url'] ?>" title="<?php echo $swatch['Image']['Alt'] ?>">
					<?php elseif (isset($item['swatch_popup'])): ?>
						<a href="javascript:void(0)" onclick="popup('<?php echo $swatch['ImageLarge']['Url'] ?>', <?php echo $swatch['ImageLarge']['Width'] ?>, <?php echo $swatch['ImageLarge']['Height'] ?>)" rel="nofollow">
					<?php endif; ?>
					<img border="0" width="<?php echo $swatch['Image']['Width'] ?>" height="<?php echo $swatch['Image']['Height'] ?>" src="<?php echo $swatch['Image']['Url'] ?>" alt="<?php echo $swatch['Image']['Alt'] ?>">
					<?php if (isset($item['swatch_lightbox']) OR isset($item['swatch_popup'])): ?>
						</a>
					</div>
					<?php endif; ?>
					<?php if (isset($swatch['Image']['Label'])): ?>
						<div class="aom_ts"><?php echo $swatch['Image']['Label'] ?></div>
					<?php endif; ?>
				</span>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>

		<?php if (isset($item['OtherImages'])): ?>
		<div class="aom_swatches">
			<div class="aom_sl"><?php echo $this->aom->str['61'] ?>:</div>
			<?php foreach ($item['OtherImages'] AS $swatch): ?>
				<span class="aom_swatch_other">
				<?php if (isset($item['swatch_lightbox'])): ?>
					<a rel="gallery2" href="javascript:void(0)" caption="<?php echo $swatch['ImageLarge']['Url'] ?>">
				<?php elseif (isset($item['swatch_popup'])): ?>
					<a href="javascript:void(0)" onclick="popup('<?php echo $swatch['ImageLarge']['Url'] ?>', <?php echo $swatch['ImageLarge']['Width'] ?>, <?php echo $swatch['ImageLarge']['Height'] ?>)" rel="nofollow">
				<?php endif; ?>
				<img border="0" width="<?php echo $swatch['Image']['Width'] ?>" height="<?php echo $swatch['Image']['Height'] ?>" src="<?php echo $swatch['Image']['Url'] ?>">
				<?php if (isset($item['swatch_lightbox']) OR isset($item['swatch_popup'])): ?>
					</a>
				<?php endif; ?>
				</span>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>

		<?php $this->aom->displayContentVertical("I5"); ?>
	</div>

	<div id="aom_detail">
		<?php $this->aom->displayContentVertical("I1"); ?>

		<?php if (isset($item['ItemDetails'])): ?>
		<ul class="aom_lv2">
			<?php foreach ($item['ItemDetails'] AS $detail): ?>
				<li><span class="aom_st"><?php echo $detail['Label'] ?>:</span><span class="aom_stn"><?php foreach ($detail['List'] AS $list): ?><?php if (isset($list['Url'])): ?><a href="<?php echo $list['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><?php echo $list['Name'] ?></a> &nbsp;<?php else: ?><?php echo $list['Name'] ?><?php endif; ?><?php endforeach; ?></span></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php if (isset($item['Price'])): ?>
			<div>
			<ul class="aom_lv2">
			<?php if (isset($item['Price']['List'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['List']['Label'] ?>:</span>&nbsp;<span class="aom_pl"><strike><?php echo $item['Price']['List']['Price'] ?><?php echo (!empty($item['Price']['List']['PriceConverted']) ? " (".$item['Price']['List']['PriceConverted'].")" : "") ?></strike></span></li>
			<?php endif; ?>
			<?php if (isset($item['Price']['Buy'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['Buy']['Label'] ?>:</span>&nbsp;<span class="aom_pb"><?php echo $item['Price']['Buy']['Price'] ?>
					<?php echo (!empty($item['Price']['Buy']['PriceConverted']) ? " (".$item['Price']['Buy']['PriceConverted'].")" : "") ?>
					<?php echo (isset($item['Price']['Sale']['Price']) ? " <span class=\"aom_po\">(".$item['Price']['Sale']['Label']." ".$item['Price']['Sale']['Price'].")</span>" : "") ?>
					<?php echo (isset($item['Price']['Buy']['PriceHigh']) ? "</span> - <span class=\"aom_pb\">".$item['Price']['Buy']['PriceHigh'] : "") ?>
					<?php echo (!empty($item['Price']['Buy']['PriceHighConverted']) ? " (".$item['Price']['Buy']['PriceHighConverted'].")" : "") ?>
					<?php echo (isset($item['Price']['Sale']['PriceHigh']) ? " <span class=\"aom_po\">(".$item['Price']['Sale']['Label']." ".$item['Price']['Sale']['PriceHigh'].")</span>" : "") ?>
					</span>
				</li>
				<li>
				<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
					<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="$.fn.colorbox({html:pmsg, width:'350px'});"><?php echo $this->aom->str['356'] ?></a></span>
				<?php else: ?>
					<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="popupPricing(pmsg)");"><?php echo $this->aom->str['356'] ?></a></span>
				<?php endif; ?>
				</li>
			<?php endif; ?>
			<?php if (isset($item['Price']['Save'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['Save']['Label'] ?>:</span>&nbsp;<span class="aom_pl"><?php echo $item['Price']['Save']['Price'] ?><?php echo (!empty($item['Price']['List']['PriceConverted']) ? " (".$item['Price']['Save']['PriceConverted'].")" : "") ?><?php echo (isset($item['Price']['Save']['PercentSaved']) ? " (".$item['Price']['Save']['PercentSaved']."%)" : "") ?></span></li>
			<?php endif; ?>
			</ul>
			</div>
		<?php endif; ?>

		<?php $this->aom->displayContentVertical("I2") ?>

		<?php if (!isset($item['Variations']['Label']) AND isset($item['Button']) AND isset($item['Button']['LocationI1'])): ?>
			<div class="aom_btn"><?php $this->aom->displayBuyButton($item['Button']) ?></div>
		<?php elseif (isset($item['ErrorItemNotAvailable'])): ?>
			<div class="aom_e"><?php echo $this->aom->str['5'] ?></div>
		<?php elseif (!empty($this->aom->site['ErrorItemNotAvailable'])): ?>
			<?php echo $this->aom->site['ErrorItemNotAvailable'] ?>
		<?php endif; ?>

		<?php if (isset($item['Marketplace'])): ?>
		<div>
		<?php if (isset($item['Marketplace']['Url'])): ?>
			<a class="aom_ilr" href="<?php echo $item['Marketplace']['Url'] ?>"<?php echo $this->aom->nofollow['M'] ?>><?php echo $item['Marketplace']['Label'] ?></a>
		<?php else: ?>
			<span class="aom_ilr"><?php echo $item['Marketplace']['Label'] ?></span>
		<?php endif; ?>
		<?php if (isset($item['Marketplace']['Price'])): ?>
			<span class="aom_ilr"><?php echo $item['Marketplace']['Price'] ?><?php echo (!empty($item['Marketplace']['PriceConverted']) ? " (".$item['Marketplace']['PriceConverted'].")" : "") ?></span>
		<?php endif; ?>
		</div>
		<?php endif; ?>
		<?php if (isset($item['Variations']['Label']) AND !empty($this->aom->site['ShoppingCart']) AND $this->aom->site['ShoppingCart']=="Yes"): ?>
		<div>
			<div class="aom_sl"><?php echo $item['Variations']['Label'] ?>:</div>
			<form id="Variation" name="Variation" method="POST" action="<?php echo $this->aom->script ?>">
			<input type="hidden" name="a" value="cartadd">
			<input type="hidden" name="asin" value="<?php echo $this->aom->clean['i'] ?>">
			<select name="offerlistingid" size="<?php echo $item['Variations']['Size'] ?>">
			<?php foreach ($item['Variations']['List'] AS $variation): ?>
				<option class="<?php echo $variation['Class'] ?>" value="<?php echo $variation['Value'] ?>"><?php echo $variation['Label'] ?></option>
			<?php endforeach; ?>
			</select>
			<?php if (isset($item['Variations']['Quantity'])): ?>
				<div class="aom_sl"><?php echo $this->aom->str['14'] ?><input style="margin:6px 0;" type="text" name="quantity" value="1" size="2"><?php echo (isset($item['Variations']['InStock']) ? "<span class=\"aom_sto\">".$this->aom->str['309']."</span>" : "") ?></div>
			<?php endif; ?>
			<?php if (isset($item['Variations']['Image'])): ?>
				<div class="aom_btn"><input type="image" name="submit" value="<?php echo $this->aom->str['58'] ?>" alt="<?php echo $this->aom->str['58'] ?>" src="<?php echo $item['Variations']['Image'] ?>" onClick="return checkVariation('<?php echo $this->aom->str['326'] ?>');"></div>
			<?php else: ?>
				<div onClick="document.getElementById('Variation').submit();" id="<?php echo $item['Variations']['Id'] ?>"><?php echo $item['Variations']['Name'] ?></div>
			<?php endif; ?>
			</form>

			<?php
			if (isset($item['Variations']['Merchants'])) {
				?>
				<br>
				<div class="aom_sl"><?php echo $this->aom->str['387'] ?>:</div>
				<?php
				$mx=1;
				foreach ($item['Variations']['Merchants'] AS $merchant) {
				?>
					<div class="aom_mbox aom_mr<?php echo $mx ?>"><?php echo $merchant['Name'] ?><?php echo (isset($merchant['Availability']) ? "<div class=\"aom_tr\" style=\"padding-left:6px;\">".$merchant['Availability']."</div>" : "") ?></div>
				<?php
					$mx++;
				}
			}
			?>
		</div>
		<?php elseif (isset($item['Variations']['Label'])): ?>
			<div class="aom_tr"><?php echo $item['Variations']['Label'] ?></div>
			<?php if (isset($item['Button'])): ?>
				<?php $this->aom->displayBuyButton($item['Button']) ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php $this->aom->displayContentVertical("I3") ?>

		<?php if (isset($item['ItemAttributes'])): ?>
		<ul class="aom_lv2">
			<?php foreach ($item['ItemAttributes'] AS $attr): ?>
				<?php if (isset($attr['List'])):
					$a=1;
					$asize = sizeof($attr['List']);
				?>
					<li><span class="aom_st"><?php echo $attr['Label'] ?>:</span><?php foreach ($attr['List'] AS $aname): ?><span class="aom_stn"><?php echo $aname ?></span><?php echo ($a < $asize ? ", " : "") ?><?php $a++ ?><?php endforeach; ?></li>
				<?php else: ?>
					<li><span class="aom_st"><?php echo $attr['Label'] ?>:</span><span class="aom_stn"><?php echo $attr['Name'] ?></span></li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php if (isset($item['ItemParts'])): ?>
		<ul class="aom_lv2">
			<?php foreach ($item['ItemParts'] AS $part): ?>
				<li><span class="aom_st"><?php echo $part['Label'] ?>:</span><span class="aom_stn"><?php echo $part['Name'] ?></span></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php if (isset($item['Promotions'])): ?>
		<ul class="aom_lv2">
			<span class="aom_st"><?php echo $item['PromotionsLabel'] ?>:</span>
			<?php foreach ($item['Promotions'] AS $promo): ?>
				<li>
					<?php if (!empty($promo['Description'])): ?>
						<span class="aom_stn"><?php echo $promo['Description'] ?></span>
					<?php endif; ?>
					<?php if (isset($promo['Terms'])): ?>
						<div class="aom_stn"><a href="javascript:void(0)" onclick="$.fn.colorbox({html:'<?php echo $promo['Terms'] ?>', width:'550px'});"><?php echo $this->aom->str['249'] ?></a></div>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php if (isset($item['IsEligibleForSuperSaverShipping'])): ?>
			<span class="aom_st"><?php echo $item['IsEligibleForSuperSaverShipping']['Label'] ?>:</span><span class="aom_sh"><?php echo $item['IsEligibleForSuperSaverShipping']['Name'] ?></span><br>
		<?php endif; ?>
		<?php if (isset($item['WillShipExpedited'])): ?>
			<span class="aom_st"><?php echo $item['WillShipExpedited']['Label'] ?>:</span><span class="aom_sh"><?php echo $item['WillShipExpedited']['Name'] ?></span><br>
		<?php endif; ?>
		<?php if (isset($item['WillShipInternational'])): ?>
			<span class="aom_st"><?php echo $item['WillShipInternational']['Label'] ?>:</span><span class="aom_sh"><?php echo $item['WillShipInternational']['Name'] ?></span><br>
		<?php endif; ?>
		<?php if (isset($item['Availability'])): ?>
			<span class="aom_st"><?php echo $item['Availability']['Label'] ?>:</span><span class="aom_sh"><?php echo $item['Availability']['Name'] ?></span><br>
		<?php endif; ?>
		<?php if (isset($item['Condition'])): ?>
			<span class="aom_st"><?php echo $item['Condition']['Label'] ?>:</span><span class="aom_sh"><?php echo $item['Condition']['Name'] ?></span><br>
		<?php endif; ?>

		<?php if (isset($item['Links'])): ?>
		<ul class="aom_lv2">
			<?php foreach ($item['Links'] AS $link): ?>
				<li><span class="aom_stn"><a target="_new" href="<?php echo $link['Url'] ?>" rel="nofollow"><?php echo $link['Name'] ?></a></span></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

	</div>

	<?php $this->aom->displayContentVertical("I6") ?>

	<div style="clear:both;"></div>
</div>
<?php $this->aom->displayContentVertical("I4") ?>
<div id="aom_info">
	<?php if (isset($item['Synopsis'])): ?>
	<div>
		<hr class="aom_hr">
		<span class="aom_t"><?php echo $this->aom->str['344'] ?>:</span><span class="aom_tr"><?php echo $item['Synopsis'] ?></span>
	</div>
	<?php endif; ?>

	<?php if (isset($item['Features'])): ?>
	<div>
		<hr class="aom_hr">
		<span class="aom_t"><?php echo $this->aom->str['250'] ?>:</span>
		<ul>
		<?php foreach ($item['Features'] AS $feature): ?>
			<li><span class="aom_tr"><?php echo $feature ?></span></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($item['Discs'])): ?>
	<div>
		<hr class="aom_hr">
		<?php
		$disc_num = 1;
		$disc_cnt = sizeof($item['Discs']);
		foreach ($item['Discs'] AS $disc): ?>
			<div class="aom_t"><?php echo ($disc_cnt>1 ? $this->aom->str['252']." ".$disc_num." " : "") ?> <?php echo $this->aom->str['251'] ?></div>
			<ul>
				<?php foreach ($disc['Track'] AS $track): ?><li class="aom_tr"><?php echo $track ?></li><?php endforeach; ?>
			</ul>
			<?php
			$disc_num++;
		endforeach;
		?>
	</div>
	<?php endif; ?>

	<?php if (isset($item['AlternateVersions'])): ?>
	<div>
		<hr class="aom_hr">
		<span class="aom_t"><?php echo $this->aom->str['253'] ?>:</span>
		<ul>
		<?php foreach ($item['AlternateVersions'] AS $alt): ?>
			<li class="aom_tr"><?php echo $alt['Binding'] ?> - <a href="<?php echo $alt['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><?php echo $alt['Label'] ?></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($item['Collections'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['352'] ?>:</div>
		<ul>
		<?php foreach ($item['Collections'] AS $collection): ?>
			<li><a href="<?php echo $collection['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><img border="0" width="91" height="91" src="<?php echo $collection['Image'] ?>" alt="<?php echo $collection['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $collection['Url'] ?>"><?php echo $collection['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($item['Accessories'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['254'] ?>:</div>
		<ul>
		<?php foreach ($item['Accessories'] AS $accessory): ?>
			<li><a href="<?php echo $accessory['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><img border="0" width="91" height="91" src="<?php echo $accessory['Image'] ?>" alt="<?php echo $accessory['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $accessory['Url'] ?>"><?php echo $accessory['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($item['SimilarProducts'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['21'] ?>:</div>
		<ul>
		<?php foreach ($item['SimilarProducts'] AS $similar): ?>
			<li><a href="<?php echo $similar['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><img border="0" width="91" height="91" src="<?php echo $similar['Image'] ?>" alt="<?php echo $similar['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $similar['Url'] ?>"><?php echo $similar['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php $this->aom->displayContentVertical("I7"); ?>

	<?php if (isset($item['EditorialReviews'])): ?>
	<div>
		<hr class="aom_hr">
		<span class="aom_t"><?php echo $this->aom->str['255'] ?>:</span>
		<?php foreach ($item['EditorialReviews'] AS $review): ?>
			<div class="aom_sl"><?php echo $review['Source'] ?></div><div class="aom_tr"><?php echo $review['Review'] ?></div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if (isset($item['CustomerReviewsUrl'])): ?>
	<div style="clear:both;"></div>
	<div>
	<hr class="aom_hr">
	<iframe width="<?php echo $this->aom->site['ReviewBoxWidth']; ?>" height="<?php echo $this->aom->site['ReviewBoxHeight']; ?>" src="<?php echo $item['CustomerReviewsUrl']; ?>"></iframe>
	</div>
	<?php endif; ?>

	<?php if (!isset($item['Variations']['Label']) AND isset($item['Button']) AND isset($item['Button']['LocationI2'])): ?>
		<div class="aom_btn">
		<?php $this->aom->displayBuyButton($item['Button']) ?>
		</div>
	<?php endif; ?>

	<div style="clear:both;"></div>
</div>

</div>
<?php else: ?>
	<span class="aom_e"><?php echo $this->aom->str['5'] ?></span><br>
<?php endif; ?>

