<?php
// ASSOCIATE-O-MATIC THEME

// INFORMATION
$theme['Name'] 			= "AOM Default";
$theme['Version'] 		= "2.0";
$theme['Author'] 		= "Associate-O-Matic";
$theme['Url'] 			= "http://www.associate-o-matic.com/themes";
$theme['AomFirst'] 		= "5.4"; // earliest compatible AOM version
$theme['AomLast'] 		= "5.4"; // latest compatible AOM version

?>