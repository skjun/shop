<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: category_asins.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Category']) ?>

<?php $this->aom->loadTemplate("category.tpl.php") ?>