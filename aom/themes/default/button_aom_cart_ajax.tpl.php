<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: button_aom_cart_ajax.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Button']) ?>
<?php $button = $this->aom->t['Button'] ?>

<form class="" id="buy<?php echo $button['Index'] ?>" method="POST">
<input type="hidden" name="a" value="cartadd">
<input type="hidden" name="asin" value="<?php echo $button['ASIN'] ?>">
<input type="hidden" name="offerlistingid" value="<?php echo $button['OfferListingId'] ?>">

<?php if (isset($button['Quantity'])): ?>
	<span class="aom_sl"><?php echo $this->aom->str['14'] ?></span> <input style="margin:6px 0;" type="text" name="quantity" value="1" size="2">
<?php else: ?>
	<input type="hidden" name="quantity" value="1">
<?php endif; ?>

<?php if (isset($button['InStock'])): ?>
	<span class="aom_sto"><?php echo $button['InStock'] ?></span><br>
<?php endif; ?>
<?php if (isset($button['Image'])): ?>
<input type="image" onClick="buyAjax('<?php echo $this->aom->script ?>', '<?php echo $button['Index'] ?>', '<?php echo $button['Message'] ?>'); return false;" name="submit" id="submit<?php echo $button['Index'] ?>" value="<?php echo $this->aom->str['58']; ?>" alt="<?php echo $this->aom->str['58']; ?>" src="<?php echo $button['Image'] ?>">
<?php else: ?>
<a href="#" onClick="buyAjax('<?php echo $this->aom->script ?>', '<?php echo $button['Index'] ?>', '<?php echo $button['Message'] ?>');"><div id="<?php echo $button['Id'] ?>"><?php echo $button['Name'] ?></div></a>
<?php endif; ?>
</form>