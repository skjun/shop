<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: button_amazon_cart.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
$fid = "f".uniqid(rand(), true);
?>
<?php //$this->aom->dump($this->aom->t['Button']) ?>
<?php $button = $this->aom->t['Button'] ?>

<form id="<?php echo $fid ?>" class="aom_form" method="GET" target="<?php echo $this->aom->site['BuyButtonNewWindow'] ?>" action="http://www.<?php echo $this->aom->site['AmazonStoreType']; ?>/gp/aws/cart/add.html">
<input type="hidden" name="SubscriptionId" value="<?php echo $this->aom->site['AmazonAccessKeyId'] ?>">
<input type="hidden" name="AssociateTag" value="<?php echo $this->aom->site['AmazonAssociateId'] ?>">
<input type="hidden" name="OfferListingId.1" value="<?php echo $button['OfferListingId'] ?>">

<?php if (isset($button['Quantity'])): ?>
	<span class="aom_sl"><?php echo $this->aom->str['14'] ?></span> <input style="margin:6px 0;" type="text" name="Quantity.1" value="1" size="2">
<?php else: ?>
	<input type="hidden" name="Quantity.1" value="1">
<?php endif; ?>

<?php if (isset($button['InStock'])): ?>
	<span class="aom_sto"><?php echo $button['InStock'] ?></span><br>
<?php endif; ?>
<?php if (isset($button['Image'])): ?>
<input type="image" name="add" value="add" alt="<?php echo $this->aom->str['43'] ?>" src="<?php echo $button['Image'] ?>">
<?php else: ?>
<div onClick="document.getElementById('<?php echo $fid ?>').submit();" id="<?php echo $button['Id'] ?>"><?php echo $button['Name'] ?></div>
<?php endif; ?>
</form>