<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: pagination.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Pagination']) ?>
<?php $pg = $this->aom->t['Pagination'] ?>

<div class="aom_pagination">
	<?php if (isset($pg['Start'])): ?>
	<div class="aom_st">
         <?php echo $this->aom->str['278'] ?> <?php echo $pg['Start'] ?>-<?php echo $pg['End'] ?> <?php echo $this->aom->str['279'] ?> <?php echo $pg['TotalResults'] ?>
    </div>
	<?php endif; ?>
    <div class="aom_pg">
		
		<?php // previous ?>
		<?php if (isset($pg['Prev']['Url'])): ?>
			<a href="<?php echo $pg['Prev']['Url'] ?>" class="aom_np">&lsaquo; <?php echo $this->str['280'] ?></a>
		<?php else: ?>
			<span class="aom_np">&lsaquo; <?php echo $this->str['280'] ?></span>
		<?php endif; ?>
		
		<?php if (isset($pg['Prev']['UrlFirst'])): ?>
			<a href="<?php echo $pg['Prev']['UrlFirst'] ?>">1</a>
		<?php else: ?>
			<span class="aom_current">1</span>
		<?php endif; ?>
		
		<?php // page numbers ?>
		<?php foreach ($pg['Page'] AS $page): ?>
			<?php if (isset($page['Url'])): ?>
				<a href="<?php echo $page['Url'] ?>"><?php echo $page['Number'] ?></a>
			<?php else: ?>
				<span class="aom_current"><?php echo $page['Number'] ?></span>
			<?php endif; ?>
		<?php endforeach; ?>		
		
		<?php // next ?>
		<?php if (isset($pg['Next']['Url'])): ?>
			<a href="<?php echo $pg['Next']['Url'] ?>" class="aom_np"><?php echo $this->str['281'] ?> &rsaquo;</a>
		<?php else: ?>
			<span class="aom_np"><?php echo $this->str['281'] ?> &rsaquo;</span>
		<?php endif; ?>
		
		<?php // view all items on Amazon ?>
		<?php if (isset($pg['ViewAllItems']['Url'])): ?>
			<a href="<?php echo $pg['ViewAllItems']['Url'] ?>" class="aom_np" target="_blank" rel="nofollow"><?php echo $pg['ViewAllItems']['Label'] ?> &#187;</a>
		<?php endif; ?>		
		
	</div>
	  
</div>