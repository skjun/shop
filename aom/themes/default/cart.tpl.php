<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: cart.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Cart']) ?>
<?php $cart = $this->aom->t['Cart'] ?>

<div id="aom_cart">
	<h1><?php echo $cart['Title'] ?></h1>

	<?php if (isset($cart['ShoppingCartInstructions'])): ?>
	<div id="aom_cart_msg"><?php echo $cart['ShoppingCartInstructions'] ?></div>
	<?php endif; ?>
	<?php if (isset($cart['Message'])): ?>
	<div id="aom_cart_msg"><?php echo $cart['Message'] ?></div>
	<?php endif; ?>

	<?php if (isset($cart['Items'])): ?>
		<form id="cart" name="cart" method="POST" action="<?php echo $this->aom->script ?>">
		<input type="hidden" name="a" value="cartmodify">

		<table align="center">
	      <tr id="aom_cart_headings">
	         <?php if ($this->aom->site['DisplayImageColumn']=="Yes"): ?>
	         <td class="aom_cart_col1"><?php echo $this->aom->str['333'] ?></td>
	         <?php endif; ?>
	         <td class="aom_cart_col2"><?php echo $this->aom->str['10'] ?></td>
	         <?php if ($this->aom->site['DisplayCategoryColumn']=="Yes"): ?>
	         <td class="aom_cart_col3"><?php echo $this->aom->str['11'] ?></td>
	         <?php endif; ?>
	         <?php if ($this->aom->site['DisplaySellerColumn']=="Yes"): ?>
	         <td class="aom_cart_col4"><?php echo $this->aom->str['12'] ?></td>
	         <?php endif; ?>
	         <td class="aom_cart_col5"><?php echo $this->aom->str['13'] ?></td>
	         <td class="aom_cart_col6"><?php echo $this->aom->str['14'] ?></td>
	         <td class="aom_cart_col7"><?php echo $this->aom->str['15'] ?></td>
	         <td class="aom_cart_col8">&nbsp;</td>
	      </tr>

	      <?php foreach ($cart['Items'] AS $item): ?>
	      <tr class="aom_cart_item <?php echo $item['Class'] ?>" valign="top">
	         <?php if ($this->aom->site['DisplayImageColumn']=="Yes"): ?>
	         <td class="aom_cart_col1"><a href="<?php echo $item['Url'] ?>" rel="nofollow"><img src="<?php echo $item['Image'] ?>" border="0" alt="<?php echo $item['TitleClean'] ?>"></a></td>
	         <?php endif; ?>
	         <td class="aom_cart_col2"><a class="<?php echo $item['Class'] ?>" href="<?php echo $item['Url'] ?>" rel="nofollow"><?php echo $item['Title'] ?></a></td>
	         <?php if ($this->aom->site['DisplayCategoryColumn']=="Yes"): ?>
	         <td class="aom_cart_col3"><?php echo $item['ProductGroup'] ?></td>
	         <?php endif; ?>
	         <?php if ($this->aom->site['DisplaySellerColumn']=="Yes"): ?>
	         <td class="aom_cart_col4"><?php echo $item['Merchant'] ?></td>
	         <?php endif; ?>
	         <td class="aom_cart_col5"><?php echo $item['Price']['Buy'] ?><?php echo (!empty($item['Price']['BuyConverted']) ? " (".$item['Price']['BuyConverted'].")" : "") ?></td>
	         <td class="aom_cart_col6"><input type="hidden" id="chk_<?php echo $item['ItemId'] ?>" name="chk_<?php echo $item['ItemId'] ?>" value="<?php echo $item['Quantity'] ?>"><input style="text-align:right" size="3" maxlength="5" type="text" name="qty_<?php echo $item['ItemId'] ?>" value="<?php echo $item['Quantity'] ?>"><div style="white-space:nowrap;"><a class="aom_stn <?php echo $item['Class'] ?>" href="<?php echo $item['Add']['Url'] ?>" rel="nofollow"><img alt="<?php echo $item['Add']['Label'] ?>" src="<?php echo $item['Add']['Image'] ?>"></a><a class="aom_stn <?php echo $item['Class'] ?>" href="<?php echo $item['Sub']['Url'] ?>" rel="nofollow"><img alt="<?php echo $item['Sub']['Label'] ?>" src="<?php echo $item['Sub']['Image'] ?>"></a></div></td>
	         <td class="aom_cart_col7"><?php echo $item['Price']['Line'] ?><?php echo (!empty($item['Price']['LineConverted']) ? " (".$item['Price']['LineConverted'].")" : "") ?></td>
	         <td class="aom_cart_col8"><a class="aom_stn <?php echo $item['Class'] ?>" href="<?php echo $item['Remove']['Url'] ?>" rel="nofollow"><?php echo $item['Remove']['Label'] ?></a></td>
	      </tr>
	      <?php endforeach; ?>
	      <tr id="aom_cart_subtotal">
	         <td class="aom_sl" colspan="<?php echo $cart['Price']['SubtotalColumns'] ?>"><?php echo $this->aom->str['16'] ?></td>
	         <td id="aom_subtotal"><?php echo $cart['Price']['Subtotal'] ?><?php echo (!empty($cart['Price']['SubtotalConverted']) ? " (".$cart['Price']['SubtotalConverted'].")" : "") ?></td>
	         <td>&nbsp;</td>
		  </tr>
	   </table>

		</form>
	<?php else: ?>
		<div class="aom_cart_err"><?php echo $this->aom->str['17'] ?></div>
	<?php endif; ?>

	<?php if (isset($cart['Error'])): ?>
		<div class="aom_cart_err"><?php echo $cart['Error'] ?></div>
	<?php endif; ?>

	<?php if (isset($cart['ShoppingCartInstructions2'])): ?>
		<div id="aom_cart_msg"><?php echo $cart['ShoppingCartInstructions2'] ?></div>
	<?php endif; ?>

	<div id="aom_cart_buttons">
		<span><a href="<?php echo $cart['Button']['Continue']['Url'] ?>"><?php if (isset($cart['Button']['Continue']['Image'])): ?><img alt="<?php echo $cart['Button']['Continue']['Label'] ?>" src="<?php echo $cart['Button']['Continue']['Image'] ?>"><?php else: ?><div id="<?php echo $cart['Button']['Continue']['Id'] ?>"><?php echo $cart['Button']['Continue']['Name'] ?></div><?php endif; ?></a></span>
		<?php if (isset($cart['Button']['Update'])): ?><span><a href="<?php echo $cart['Button']['Update']['Url'] ?>" rel="nofollow"><?php if (isset($cart['Button']['Update']['Image'])): ?><img alt="<?php echo $cart['Button']['Update']['Label'] ?>" src="<?php echo $cart['Button']['Update']['Image'] ?>"><?php else: ?><div id="<?php echo $cart['Button']['Update']['Id'] ?>"><?php echo $cart['Button']['Update']['Name'] ?></div><?php endif; ?></a></span><?php endif; ?>
		<?php if (isset($cart['Button']['Update'])): ?><span><a href="<?php echo $cart['Button']['Checkout']['Url'] ?>" target="<?php echo $cart['Button']['Checkout']['Target'] ?>" rel="nofollow"><?php if (isset($cart['Button']['Checkout']['Image'])): ?><img alt="<?php echo $cart['Button']['Checkout']['Label'] ?>" src="<?php echo $cart['Button']['Checkout']['Image'] ?>"><?php else: ?><div id="<?php echo $cart['Button']['Checkout']['Id'] ?>"><?php echo $cart['Button']['Checkout']['Name'] ?></div><?php endif; ?></a></span><?php endif; ?>
	</div>

	<?php if (isset($cart['SimilarProducts'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['21'] ?>:</div>
		<ul>
		<?php foreach ($cart['SimilarProducts'] AS $similar): ?>
			<li><a href="<?php echo $similar['Url'] ?>"><img border="0" width="91" height="91" src="<?php echo $similar['Image'] ?>" alt="<?php echo $similar['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $similar['Url'] ?>"><?php echo $similar['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($cart['SimilarViewedProducts'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['329'] ?>:</div>
		<ul>
		<?php foreach ($cart['SimilarViewedProducts'] AS $similarv): ?>
			<li><a href="<?php echo $similarv['Url'] ?>"><img border="0" width="91" height="91" src="<?php echo $similarv['Image'] ?>" alt="<?php echo $similarv['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $similarv['Url'] ?>"><?php echo $similarv['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($cart['TopSellers'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['331'] ?>:</div>
		<ul>
		<?php foreach ($cart['TopSellers'] AS $top): ?>
			<li><a href="<?php echo $top['Url'] ?>"><img border="0" width="91" height="91" src="<?php echo $top['Image'] ?>" alt="<?php echo $top['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $top['Url'] ?>"><?php echo $top['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php if (isset($cart['NewReleases'])): ?>
	<hr class="aom_hr">
	<div class="aom_pics">
		<div class="aom_t"><?php echo $this->aom->str['332'] ?>:</div>
		<ul>
		<?php foreach ($cart['NewReleases'] AS $new): ?>
			<li><a href="<?php echo $new['Url'] ?>"><img border="0" width="91" height="91" src="<?php echo $new['Image'] ?>" alt="<?php echo $new['TitleClean'] ?>"></a><div class="aom_stn"><a href="<?php echo $new['Url'] ?>"><?php echo $new['Title'] ?></a></div></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>