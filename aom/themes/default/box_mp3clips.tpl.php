<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_mp3clips.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<script type='text/javascript'>
var amzn_wdgt={widget:'MP3Clips'};
amzn_wdgt.tag='<?php echo $this->aom->site['AmazonAssociateId'] ?>';
amzn_wdgt.widgetType='ASINList';
amzn_wdgt.ASIN='<?php echo $this->aom->clean['i'] ?>,<?php echo $this->aom->clean['i'] ?>';
amzn_wdgt.title='<?php echo $this->aom->t['TitleClean'] ?>';
amzn_wdgt.width='<?php echo $this->aom->t['PlayerWidth'] ?>';
amzn_wdgt.height='<?php echo $this->aom->t['PlayerHeight'] ?>';
amzn_wdgt.shuffleTracks='False';
amzn_wdgt.marketPlace='<?php echo $this->aom->t['CountryCode'] ?>';
</script>
<script type='text/javascript' src='http://wms.assoc-<?php echo strtolower($this->aom->site['AmazonStoreType']); ?>/20070822/<?php echo $this->aom->t['CountryCode'] ?>/js/swfobject_1_5.js'></script>
