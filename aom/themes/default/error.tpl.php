<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: error.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->err) ?>
<?php $page = $this->aom->page ?>
<?php if (!isset($this->aom->app['flag_fatal'])): ?>
	<br><a href="javascript:history.back();" rel="nofollow">&laquo; <?php echo $this->aom->str['286'] ?></a><br><br>
<?php endif; ?>

<?php if (!empty($this->aom->t['ErrorApiLimitFile'])): ?>
	<?php include($this->aom->t['ErrorApiLimitFile']) ?>
<?php else: ?>
<?php
$eprev = "";
foreach ($this->aom->err AS $err) {
	
	if ($err=="We did not find any matches for your request.") {
		$err = $this->aom->str['382'];
		$nr=1;
	}
	
	if ($err!=$eprev) {
	?>
	<?php echo $err ?><br><br>
	<?php	
	}
	
	if (isset($nr))
		break;
	
	$eprev=$err;
}
?>
<?php endif; ?>