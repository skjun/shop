<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: content_body.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>

<div id="<?php echo $this->aom->t['body_content_id'] ?>">
<?php $this->aom->displayContentVertical("BT"); ?>
<?php if (sizeof($this->aom->err)>0 OR isset($this->aom->data['Error'][0]['Code'])): ?>
	<?php $this->aom->displayErr(); ?>
<?php else: ?>
	<?php $this->aom->displayContentBody(); ?>
<?php endif; ?>
<?php $this->aom->displayContentVertical("BB"); ?>
</div>