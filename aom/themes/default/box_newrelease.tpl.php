<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_newrelease.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['NewReleases']) ?>

<ul>
<?php foreach($this->aom->site['NewReleases'] AS $newrelease): ?>
		<li class="l2">
		<?php if ($newrelease['ImageUrl']): ?>
			<a href="<?php echo $newrelease['Url'] ?>"<?php echo $this->aom->nofollow['NB'] ?>><img src="<?php echo $newrelease['ImageUrl'] ?>" border="0" alt="<?php echo $newrelease['Title'] ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $newrelease['Url'] ?>"<?php echo $this->aom->nofollow['NB'] ?>><?php echo $newrelease['Title'] ?></a>
		</li>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>
