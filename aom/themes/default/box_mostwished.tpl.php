<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_mostwished.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['MostWished']) ?>

<ul>
<?php foreach($this->aom->site['MostWished'] AS $mostwished): ?>
		<li class="l2">
		<?php if ($mostwished['ImageUrl']): ?>
			<a href="<?php echo $mostwished['Url'] ?>"<?php echo $this->aom->nofollow['WB'] ?>><img src="<?php echo $mostwished['ImageUrl'] ?>" border="0" alt="<?php echo $mostwished['Title'] ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $mostwished['Url'] ?>"<?php echo $this->aom->nofollow['WB'] ?>><?php echo $mostwished['Title'] ?></a>
		</li>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>