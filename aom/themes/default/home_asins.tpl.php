<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: home_asins.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>

<?php if (isset($this->aom->t['HomeWelcomeHtml'])) : ?>
	<div class="aom_home_welcome"><?php echo $this->aom->t['HomeWelcomeHtml'] ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>
<?php if (isset($this->aom->t['HomeWelcomeFile'])) : ?>
	<div class="aom_home_welcome"><?php include($this->aom->t['HomeWelcomeFile']) ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>
<?php //$this->aom->dump($this->aom->t['Category']) ?>
<?php $category = $this->aom->t['Category'] ?>
<div class="aom_category<?php echo (isset($category['Id']) ? " ".$category['Id'] : "") ?>">
	<?php $this->aom->displayContentVertical("C1"); ?>
	<div id="aom_items">
	<?php foreach ($category['Items'] AS $item): ?>
		<?php if (($item['Number'] % $this->aom->site['CategoryColumns']==1) OR $this->aom->site['CategoryColumns']==1): ?>
			<div class="aom_hr"></div>
		<?php endif; ?>
		<?php $this->aom->displayItem($item, "category"); ?>
	<?php endforeach; ?>
	<div class="aom_hr"></div>
	</div>
<div style="clear:both;"></div>
</div>