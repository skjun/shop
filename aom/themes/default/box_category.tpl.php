<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_category.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->site['Categories']['Category']) ?>

<ul>
<?php foreach ($this->aom->site['Categories']['Category'] AS $cat): ?>
	<?php if ($cat['Box']=="On"): ?>
		<li class="l1"><span class="aom_bb"><?php echo $this->aom->site['Bullet'] ?></span><span class="aom_list"><a href="<?php echo $this->aom->buildUrl( Array('c' => $cat['Id'], 'x' => $cat['Name']) ) ?>"><?php echo $cat['Name'] ?></a></span></li>
	<?php endif; ?>
<?php endforeach; ?>
</ul>
<div style="clear:both;"></div>