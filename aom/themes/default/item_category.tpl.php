<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: item_category.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Item']) ?>
<?php $item = $this->aom->t['Item'] ?>

<div class="aom_item_category">
	
<ul class="aom_item">
<li>
	<div class="aom_img">
	<a href="<?php echo $item['Url'] ?>"><img src="<?php echo $item['Image']['Url'] ?>"<?php echo (isset($item['Image']['Width']) ? " width=\"{$item['Image']['Width']}\"" : "") ?><?php echo (isset($item['Image']['Height']) ? " height=\"{$item['Image']['Height']}\"" : "") ?> alt="<?php echo $item['TitleClean'] ?>"></a>
	<?php if (isset($item['ImageLarge'])): ?>
		<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
			<div class="aom_z"><a href="javascript:void(0)" onclick="$.fn.colorbox({href:'<?php echo $item['ImageLarge']['Url'] ?>',open:true, rel:'zoom', title:'<?php echo $item['TitleClean'] ?>'});" title="<?php echo $item['TitleClean'] ?>"><img src="<?php echo $this->aom->tpath_img ?>/zoom.gif" border="0" alt="<?php echo $this->aom->str['31'] ?>" align="middle"></a> <span class="aom_stn"><a href="javascript:void(0)" onclick="$.fn.colorbox({href:'<?php echo $item['ImageLarge']['Url'] ?>',open:true, rel:'zoom', title:'<?php echo $item['TitleClean'] ?>'});" title="<?php echo $item['TitleClean'] ?>"><?php echo $this->aom->str['31'] ?></a></span></div>
		<?php else: ?>
			<div class="aom_z"><a href="javascript:void(0)" onclick="popup('<?php echo $item['ImageLarge']['Url'] ?>', <?php echo $item['ImageLarge']['Width'] ?>, <?php echo $item['ImageLarge']['Height'] ?>)" rel="nofollow"><img src="<?php echo $this->aom->tpath_img ?>/zoom.gif" border="0" alt="<?php echo $this->aom->str['31'] ?>" align="middle"></a> <span class="aom_stn"><a href="javascript:void(0)" onclick="popup('<?php echo $item['ImageLarge']['Url'] ?>', <?php echo $item['ImageLarge']['Width'] ?>, <?php echo $item['ImageLarge']['Height'] ?>)" rel="nofollow"><?php echo $this->aom->str['31'] ?></a></span></div>
		<?php endif; ?>
	<?php endif; ?>
	</div>
	<div class="aom_detail">
		<div>
		<a style="display:inline;" class="aom_il" href="<?php echo $item['Url'] ?>"><?php echo $item['Title'] ?></a>
		<?php if (isset($item['BestsellerImage'])): ?>
		&nbsp;<img align="top" src="<?php echo $item['BestsellerImage'] ?>" alt="">
		<?php endif; ?>
		<?php if (isset($item['NewReleaseImage'])): ?>
		&nbsp;<img align="top" src="<?php echo $item['NewReleaseImage'] ?>" alt="">
		<?php endif; ?>
		</div>
		
		<?php if (isset($item['Maker']['Type']) AND isset($item['Maker']['List'])): ?>
		<div>
		<ul class="aom_lh"><li class="aom_st"><?php echo $item['Maker']['Type'] ?>:</li>
		<?php foreach ($item['Maker']['List'] AS $list): ?>
			<li class="aom_stn">
			<?php if (isset($list['Url'])): ?>
				<a href="<?php echo $list['Url'] ?>"<?php echo $this->aom->nofollow['I'] ?>><?php echo $list['Name'] ?></a>
			<?php else: ?>
				<?php echo $list['Name'] ?>
			<?php endif; ?>
			</li>
		<?php endforeach; ?>
		</ul>	
		</div>
		<?php endif; ?>
		
		<?php if (isset($item['Price'])): ?>
			<div>
			<ul class="aom_lv">
			<?php if (isset($item['Price']['List'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['List']['Label'] ?>:</span>&nbsp;<span class="aom_pl"><strike><?php echo $item['Price']['List']['Price'] ?><?php echo (!empty($item['Price']['List']['PriceConverted']) ? " (".$item['Price']['List']['PriceConverted'].")" : "") ?></strike></span></li>
			<?php endif; ?>
			<?php if (isset($item['Price']['Buy'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['Buy']['Label'] ?>:</span>&nbsp;<span class="aom_pb"><?php echo $item['Price']['Buy']['Price'] ?>
					<?php echo (!empty($item['Price']['Buy']['PriceConverted']) ? " (".$item['Price']['Buy']['PriceConverted'].")" : "") ?>
					<?php echo (isset($item['Price']['Sale']['Price']) ? " <span class=\"aom_po\">(".$item['Price']['Sale']['Label']." ".$item['Price']['Sale']['Price'].")</span>" : "") ?>
					<?php echo (isset($item['Price']['Buy']['PriceHigh']) ? "</span> - <span class=\"aom_pb\">".$item['Price']['Buy']['PriceHigh'] : "") ?>
					<?php echo (!empty($item['Price']['Buy']['PriceHighConverted']) ? " (".$item['Price']['Buy']['PriceHighConverted'].")" : "") ?>
					<?php echo (isset($item['Price']['Sale']['PriceHigh']) ? " <span class=\"aom_po\">(".$item['Price']['Sale']['Label']." ".$item['Price']['Sale']['PriceHigh'].")</span>" : "") ?>
					</span>
				</li>
				<li>
				<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
					<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="$.fn.colorbox({html:pmsg, width:'350px'});"><?php echo $this->aom->str['356'] ?></a></span>
				<?php else: ?>
					<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="popupPricing(pmsg)");"><?php echo $this->aom->str['356'] ?></a></span>
				<?php endif; ?>
				</li>
			<?php endif; ?>
			<?php if (isset($item['Price']['Save'])): ?>
				<li><span class="aom_sl"><?php echo $item['Price']['Save']['Label'] ?>:</span>&nbsp;<span class="aom_pl"><?php echo $item['Price']['Save']['Price'] ?><?php echo (!empty($item['Price']['List']['PriceConverted']) ? " (".$item['Price']['Save']['PriceConverted'].")" : "") ?><?php echo (isset($item['Price']['Save']['PercentSaved']) ? " (".$item['Price']['Save']['PercentSaved']."%)" : "") ?></span></li>
			<?php endif; ?>
			</ul>
			</div>
		<?php endif; ?>
		
		<?php $this->aom->displayContentVertical("C2") ?>
		
		<?php if (isset($item['Button'])): ?>
		<div class="aom_btn">
			<?php $this->aom->displayBuyButton($item['Button']) ?>
		</div>
		<?php endif; ?>
		
	<?php if (isset($item['Marketplace'])): ?>
		<div>
		<?php if (isset($item['Marketplace']['Url'])): ?>
			<a class="aom_ilr" href="<?php echo $item['Marketplace']['Url'] ?>"<?php echo $this->aom->nofollow['M'] ?>><?php echo $item['Marketplace']['Label'] ?></a>
		<?php else: ?>
			<span class="aom_ilr"><?php echo $item['Marketplace']['Label'] ?></span>
		<?php endif; ?>
		<?php if (isset($item['Marketplace']['Price'])): ?>
			<span class="aom_ilr"><?php echo $item['Marketplace']['Price'] ?><?php echo (!empty($item['Marketplace']['PriceConverted']) ? " (".$item['Marketplace']['PriceConverted'].")" : "") ?></span>
		<?php endif; ?>
		</div>
	<?php endif; ?>
	
	<?php $this->aom->displayContentVertical("C3"); ?>
	
	<?php if (isset($item['SalesRank'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['35'] ?>:</span><span class="aom_stn"><?php echo $item['SalesRank'] ?></span></div>
	<?php endif; ?>
	<?php if (isset($item['ProductGroup'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['11'] ?>:</span><span class="aom_stn"><?php echo $item['ProductGroup'] ?></span></div>
	<?php endif; ?>
	<?php if (isset($item['ASIN'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['36'] ?>:</span><span class="aom_stn"><?php echo $item['ASIN'] ?></span></div>
	<?php endif; ?>
	<?php if (isset($item['PublicationDate'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['37'] ?>:</span><span class="aom_stn"><?php echo $item['PublicationDate'] ?></span>
		<?php if (!empty($item['PublicationNote'])): ?>
			&nbsp;&nbsp;<span class="aom_n">(<?php echo $item['PublicationNote'] ?>)</span>
		<?php endif; ?>
		</div>
	<?php elseif (isset($item['ReleaseDate'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['38'] ?>:</span><span class="aom_stn"><?php echo $item['ReleaseDate'] ?></span>
		<?php if (!empty($item['ReleaseNote'])): ?>
			&nbsp;<span class="aom_n">(<?php echo $item['ReleaseNote'] ?>)</span>
		<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php if (isset($item['IsEligibleForSuperSaverShipping'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['39'] ?>:</span><span class="aom_sh"><?php echo $this->aom->str['287'] ?></span></div>
	<?php endif; ?>
	<?php if (isset($item['Availability'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['40'] ?>:</span><span class="aom_stn"><?php echo $item['Availability'] ?></span></div>
	<?php endif; ?>
	<?php if (isset($item['ConditionNote'])): ?>
		<div><span class="aom_st"><?php echo $this->aom->str['41'] ?>:</span>&nbsp;<span class="aom_sh"><?php echo $item['ConditionNote'] ?></span>
		<?php if (!empty($item['ConditionDetails'])): ?>... <span class="aom_stn"><a href="javascript:void(0)" onclick="popupCond('<?php echo $item['ConditionNote'] ?>');" rel="nofollow"><?php echo $this->aom->str['270'] ?></a></span>
		<?php endif; ?>
		</div>
	<?php endif; ?>
	</div>

</li>
</ul>
<div style="clear:both;"></div>
</div>
