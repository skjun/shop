<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: item_blended.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Item']) ?>
<?php $item = $this->aom->t['Item'] ?>

<div class="aom_item_blended">
	<div class="aom_img_blended">
		<a href="<?php echo $item['Url'] ?>"><img src="<?php echo $item['Image']['Url'] ?>"<?php echo (isset($item['Image']['Width']) ? " width=\"{$item['Image']['Width']}\"" : "") ?><?php echo (isset($item['Image']['Height']) ? " height=\"{$item['Image']['Height']}\"" : "") ?> alt="<?php echo $item['TitleClean'] ?>"></a>
	</div>
	<div class="aom_detail_blended">
		<ul>
		<li class="aom_st"><a href="<?php echo $item['Url'] ?>"><?php echo $item['Title'] ?></a></li>
		<?php if (isset($item['Price'])): ?>
			<li>
			<span class="aom_st"><?php echo $item['Price']['BuyLabel'] ?>:</span><span class="aom_sth"><?php echo $item['Price']['BuyPrice'] ?></span>
			<?php if (strstr($this->aom->site['ImageZoom'], "lightbox")): ?>
				<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="$.fn.colorbox({html:pmsg, width:'350px'});"><?php echo $this->aom->str['356'] ?></a></span>
			<?php else: ?>
				<span class="aom_stt"><?php echo $this->aom->str['355'] ?> <?php echo $this->aom->app['timestamp'] ?> <a href="javascript:void(0)" onclick="popupPricing(pmsg)");"><?php echo $this->aom->str['356'] ?></a></span>
			<?php endif; ?>
			</li>
		<?php endif; ?>
		<?php if (isset($item['Marketplace'])): ?>
			<li>
			<?php if (isset($item['Marketplace']['Url'])): ?>
				<a class="aom_st" href="<?php echo $item['Marketplace']['Url'] ?>"<?php echo $this->aom->nofollow['M'] ?>><?php echo $item['Marketplace']['Label'] ?></a>
			<?php else: ?>
				<span class="aom_st"><?php echo $item['Marketplace']['Label'] ?></span>
			<?php endif; ?>
			<?php if (isset($item['Marketplace']['Price'])): ?>
				<span class="aom_stn"><?php echo $item['Marketplace']['Price'] ?><?php echo (!empty($item['Marketplace']['PriceConverted']) ? " (".$item['Marketplace']['PriceConverted'].")" : "") ?></span>
			<?php endif; ?>
			</li>
		<?php endif; ?>	
		<?php if (isset($item['SalesRank'])): ?>
			<li><span class="aom_st"><?php echo $item['SalesRank']['Label'] ?>:</span><span class="aom_stn"><?php echo $item['SalesRank']['Name'] ?></span></li>
		<?php endif; ?>
		</ul>
	</div>
</div>