<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: category.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['Category']) ?>
<?php $category = $this->aom->t['Category'] ?>
<div class="aom_category<?php echo (isset($category['Id']) ? " ".$category['Id'] : "") ?>">
	<?php if (isset($category['Icon'])): ?>
		<img src="<?php echo $category['Icon'] ?>" alt="<?php echo $category['Name'] ?>">
	<?php endif; ?>
	<?php if (!(isset($this->aom->t['PageType']) AND $this->aom->t['PageType']=="H")): ?>
		<h1><?php echo $category['Name'] ?></h1>
	<?php endif; ?>
	<?php if (isset($category['Rss'])): ?>
		<a style="margin-left:14px;" href="<?php echo $category['Rss']['Url'] ?>" target="<?php echo $this->aom->site['RssLinkTarget'] ?>"><img src="<?php echo $category['Rss']['Icon'] ?>" alt="RSS - <?php echo $this->aom->str['349'] ?> <?php echo $category['Name']?>"></a>
	<?php endif; ?>
	<?php $this->aom->displayContentVertical("C1"); ?>
	<?php if (isset($category['Description'])): ?>
		<div><?php echo $category['Description'] ?></div>
	<?php endif; ?>
	<div>
	<?php if (isset($category['Sort'])): ?>
		<?php $this->aom->displaySort($category['Sort']); ?>
	<?php endif; ?>
	<?php if (isset($category['Pagination'])): ?>
		<?php $this->aom->displayPagination($category['Pagination']); ?>
	<?php endif; ?>
	</div>
	<div id="aom_items">
	<?php $n=1 ?>
	<?php foreach ($category['Items'] AS $item): ?>
		<?php if ((($n % $this->aom->site['CategoryColumns'])==1) OR $this->aom->site['CategoryColumns']==1): ?>
			<div class="aom_hr"></div>
		<?php endif; ?>
		<?php $this->aom->displayItem($item, "category"); ?>
		<?php $n++ ?>
	<?php endforeach; ?>
	<div class="aom_hr"></div>
	</div>
	<?php if (isset($category['Pagination'])): ?>
		<?php $this->aom->displayPagination($category['Pagination']); ?>
	<?php endif; ?>
	<div style="clear:both;"></div>
</div>
