<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: box_custom_rss.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>
<?php //$this->aom->dump($this->aom->t['CustomBox']) ?>

<?php

if (isset($this->aom->t['CustomBox']['Rss'])) {
$rss = $this->aom->t['CustomBox']['Rss'];

if (isset($rss['channel'])) {
	$rss = $rss['channel'];
	unset($rss['channel']);
}	

// description
if (isset($rss['subtitle']) AND is_array($rss['subtitle']) AND isset($rss['subtitle']['CONTENT'])) {
	$rss['description'] = $rss['subtitle']['CONTENT'];

	// published
	if (isset($rss['lastBuildDate']))
		$rss['published'] = $rss['lastBuildDate'];
	else if (isset($rss['pubDate']))
		$rss['published'] = $rss['pubDate'];
	else if (isset($rss['updated']))
		$rss['published'] = $rss['updated'];
}

$found = 0;
if (isset($rss['item'])) {
	$found = 1;
}
else if (isset($rss['entry'])) {
	$rss['item'] = $rss['entry'];
	$found = 1;
}

if ($found) {
	
	if (isset($rss['published'])) {
		$y = date("Y");
		$rss['published'] = substr($rss['published'],0,strpos($rss['published'],$y)+4);
		?>
		&nbsp;&nbsp;&nbsp<span class="aom_rss_cb_item_pubDate"><?php echo $rss['published'] ?></span>
		<?php
	}
	?>
	<div class="aom_rss_cb_container">
	<?php
	$i_max = $this->aom->site['RssCustomBoxes'];
	$i_cnt = 0;
	
	// item loop
	if (isset($rss['item'])) {
		foreach ($rss['item'] AS $item) {
			
			// title
			if (isset($item['title']) AND is_array($item['title']) AND isset($item['title']['CONTENT']))
				$item['title'] = $item['title']['CONTENT'];
			
			// link
			if (isset($item['link']) AND is_array($item['link']) AND isset($item['link']['href']))
				$item['link'] = $item['link']['href'];
			
			if ($i_cnt==$i_max)
				break;
			if (isset($item['title']) AND isset($item['link'])) {
				
				// item description
				if (isset($item['content:encoded']))
					$item['description'] = $item['content:encoded'];
				else if (isset($item['content']) AND is_array($item['content']) AND isset($item['content']['CONTENT']))
					$item['description'] = $item['content']['CONTENT'];
												
				if (!(isset($this->aom->site['RssExternalFields']['Option']) AND in_array('item-description', $this->aom->site['RssExternalFields']['Option'])))
					unset($item['description']);
				
				// item pubDate
				if (isset($item['published'])) {
					$item['pubDate'] = $item['published'];
				}
				
				if (!(isset($this->aom->site['RssExternalFields']['Option']) AND in_array('item-pubDate', $this->aom->site['RssExternalFields']['Option'])))
					unset($item['pubDate']);
				
				// item thumbnail
				if (isset($item['media:thumbnail']) AND is_array($item['media:thumbnail']) AND isset($item['media:thumbnail']['url']))
					$item['thumbnail'] = $item['media:thumbnail']['url'];
				?>
				<div class="aom_rss_cb_item_container">
				<div><span class="aom_rss_cb_item_title"><a target="<?php echo $this->aom->site['RssLinkTarget'] ?>" class="aom_rss_cb_item_link" href="<?php echo $item['link'] ?>"<?php echo (isset($this->aom->t['CustomBox']['NF']) ? " rel=\"nofollow\"" : "") ?>><?php echo $item['title'] ?></a></span>
				<?php
				if (isset($item['pubDate'])) {
					$y = date("Y");
					$item['pubDate'] = substr($item['pubDate'],0,strpos($item['pubDate'],$y)+4);
					?>									
					<div></div>
					<span class="aom_rss_cb_item_pubDate"><?php echo $item['pubDate'] ?></span>
					<?php
				}
				?>					
				</div>
				</div><br>
				
				<?php
				$i_cnt++;
			}
		}
	}
	?>
	</div>	
	<?php
	if (isset($rss['copyright'])) {
		?>
		<br><span class="aom_rss_cb_item_copyright"><?php echo $rss['copyright'] ?></span>
		<?php
	}
}
else {
	echo "<b>Application Error:</b> RSS Feed format error. Please send data below to AOM.</b>";
	$this->aom->dump($rss);
}
}
?>
<div style="clear:both;"></div>
