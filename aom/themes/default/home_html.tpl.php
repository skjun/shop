<?php
/********************************************************************
Associate-O-Matic Theme: default
Associate-O-Matic Template: home_html.tpl.php

IMPORTANT NOTE
It is recommended that instead of editing the default template files,
you install a copy of the default template and edit those files instead.

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/
?>

<div id="aom_home">
<?php if (isset($this->aom->t['HomeWelcomeHtml'])) : ?>
	<div class="aom_home_welcome"><?php echo $this->aom->t['HomeWelcomeHtml'] ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>
<?php if (isset($this->aom->t['HomeWelcomeFile'])) : ?>
	<div class="aom_home_welcome"><?php include($this->aom->t['HomeWelcomeFile']) ?></div>
	<div style="clear:both;"></div>
<?php endif; ?>
<?php echo $this->aom->t['HomeHtml'] ?>
</div>