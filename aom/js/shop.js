// Associate-O-Matic v4.5.0

function popup(purl, pw, ph) {
	pww = pw + 30;
   	pwh = ph + 30;
	winpops = window.open('', 'imgWindow', 'width=' + pww + ',height=' + pwh);
   	winpops.document.write('<html><head><title></title></head><body style=\'cursor:pointer;\' onload=\'this.focus();\' onclick=\'window.close();\' onblur=\'window.close();\'><img alt=\'\' src=\'' + purl + '\' width=\'' + pw + '\' height=\'' + ph + '\'></body></html>');
	winpops.document.close();
}

function popupCond(msg) {
	winpops = window.open('', 'imgWindowTerms', 'width=300,height=300,resizable=1,scrollbars=yes')
	winpops.document.write('<html><head><title></title></head><body onload=\'this.focus();\' onclick=\'window.close();\' onblur=\'window.close();\'>' + msg + '</body></html>');
	winpops.document.close();
}

function popupPricing(msg) {
	winpops = window.open('','imgWindowPricing','width=230,height=230,resizable=1,scrollbars=no')
	winpops.document.write('<html><head><title></title></head><body onload=\'this.focus();\' onclick=\'window.close();\' onblur=\'window.close();\' style=\'cursor:pointer;\'><span class=\'aom_pdr\'>' + msg + '</span></body></html>')
	winpops.document.close();
}

function popupTerms(msg) {
	winpops = window.open('','imgWindowPricing','width=500,height=400,resizable=1,scrollbars=yes')
	winpops.document.write('<html><head><title></title></head><body onload=\'this.focus();\' onclick=\'window.close();\' onblur=\'window.close();\' style=\'cursor:pointer;\'><span class=\'aom_pdr\'>' + msg + '</span></body></html>')
	winpops.document.close();
}

function checkVariation(msg) {
	var selection=document.Variation.elements['offerlistingid'].selectedIndex;
	if (selection=="-1") {
		alert(msg);
		document.Variation.elements['offerlistingid'].focus();
		return false;
	}
	return true;
}

function redirectSameWindow(msg) {
	if (document.Variation.elements['offerlistingid'].value != '') {
		window.open(document.Variation.elements['offerlistingid'].value, 'win');
	}
	else {
		alert(msg);
	}
}

function redirectNewWindow(msg) {
	if (document.Variation.elements['offerlistingid'].value != '') {
		location.href(document.Variation.elements['offerlistingid'].value);
	}
	else {
		alert(msg);
	}
}

function displayDate(amz) {
	var t = new Date();
	var month = t.getMonth() + 1;
	var day = t.getDate();
	var year = t.getFullYear();

	if (amz=='Amazon.de' || amz=='Amazon.fr' || amz=='Amazon.co.uk' || amz=='Amazon.it') {
		month = String(month);
		day = String(day);
		if (month.length == 1)
			month = "0" + month;
		if (day.length == 1)
			day = "0" + day;
	}

	if (amz=='Amazon.de')
		document.write(day + '.' + month + '.' + year)
	else if (amz=='Amazon.co.uk' || amz=='Amazon.it')
		document.write(day + '/' + month + '/' + year)
	else if (amz=='Amazon.co.jp')
		document.write('<span style="white-space:nowrap;">' + year + '&#24180;' + month + '&#26376;' + day + '&#26085' + '</span>')
	else if (amz=='Amazon.cn')
		document.write('<span style="white-space:nowrap;">' + year + '年' + month + '月' + day + '日' + '</span>')
	else
		document.write(month + '/' + day + '/' + year)
}

function enableField(id) {
	document.getElementById(id).disabled=false;
}


$(document).ready(function(){
	$("a[rel='gallery']").each(function(){
        $(this).colorbox({href:$(this).attr('caption')});
    });

	$("a[rel='gallery2']").each(function(){
        $(this).colorbox({href:$(this).attr('caption')});
    });
});


function buyAjax(script, idx, msg) {
	$.ajax({
           url: script,
           type: "POST",
           data: $("#buy"+idx).serialize(),
           cache: false,
           success: function (html) {
               alert(msg)
           }
    });
}
