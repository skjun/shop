var loaded = 0;
var formTracker = 0;

$(document).ready(function(){
	$('#Cfg').bind('submit', function() {
	  $('#ajax-loading').show()
	  $('#ajax-loading2').show()
	});
});

$(document).ready(function(){
	$('.picker').jPicker();	
});

$(document).ready(function(){
	$(".msg").delay(3000).fadeOut();
	$(".msg_err").effect("highlight", {}, 3000);
});

function formSubmit(formName) {
	r = confirm('Are you sure?')
	if (r)
		document.forms[formName].submit();
}
function applyColorScheme(scheme) {
	document.getElementById('applyscheme').value = 1
	document.getElementById('scheme').value = scheme
	document.forms['Cfg'].submit();
}
			
$(function() {
	$( "#sortable" ).sortable();
});

$(function() {
	$( "#sortable" ).sortable({
		placeholder: "ui-state-highlight"
	});
});

$(function() {
    var sticky_navigation_offset_top = $('#sticky_navigation').offset().top;
    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop();
        if (scroll_top > sticky_navigation_offset_top) {
            $('#sticky_navigation').css({ 'position': 'fixed', 'top':0 });
			$('#sticky_navigation').css({ 'display': 'block' });
			$('#sticky_button').css({ 'float': 'right' });
			$('#sticky_button').css({ 'display': 'block' });
		} else {
            $('#sticky_navigation').css({ 'display': 'none' });
			$('#sticky_button').css({ 'display': 'none' });
		}  
    };
    sticky_navigation();
    $(window).scroll(function() {
         sticky_navigation();
    });
});


$(function() {
    
	var options = {
		autoOpen: false,
		resizable: false,
		width: "auto",
		height: "auto"
	};
    var n = 1;
	$(".popup").each(function(i) {
        var dlg = $('#popup-' + n).dialog(options);
        $('.opener-' + n).click(function() {
            dlg.dialog("open");
            return false;
        });
        $('#closer-' + n).click(function() {
            dlg.dialog("close");
            return false;
        });
        n++;
    });
});

$(document).ready(function(){
	$('.gallery').each(function(){
        $(this).colorbox({
			rel:'gallery',
			scrolling:'true',
			width:'600px',
			height:'600px',
			opacity: 0.5,
			href:$(this).attr('url'), 
			title:$(this).attr('title')
		});
    });
});

function DropDown(el) {
    this.dd = el;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;
        obj.dd.on('mouseenter', function(event){
            $(this).addClass('active');
            event.stopPropagation();
        }); 
    }
}

$(function() {
    var dd = new DropDown( $('#dd') );
    $(document).click(function() {
        $('.toolbox').removeClass('active');
        $('.toolbox').removeClass('dropdown');
    });
    
    $('.dropdown').mouseleave(function() {
        $('.toolbox').removeClass('active');
        $('.toolbox').removeClass('dropdown');
    });
    
    $('.toolbox').mouseleave(function() {
        $('.toolbox').removeClass('active');
        $('.toolbox').removeClass('dropdown');
    });
    
});
