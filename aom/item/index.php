<?php
/********************************************************************
Associate-O-Matic v5.5.0
http://www.associate-o-matic.com

Justin Mecham
info@associate-o-matic.com

DESCRIPTION
Amazon link masking script

Copyright (c) 2004-2013 Associate-O-Matic. All Rights Reserved.
********************************************************************/

$version = "5.5.0";

$l = urldecode($_GET['l']);
$e = urldecode($_GET['e']);

$locale = Array(
		'us' => 'amazon.com',
		'uk' => 'amazon.co.uk',
		'ca' => 'amazon.ca',
		'de' => 'amazon.de',
		'fr' => 'amazon.fr',
		'jp' => 'amazon.co.jp',
		'it' => 'amazon.it',
		'cn' => 'amazon.cn',
		'es' => 'amazon.es',
		'in' => 'amazon.in'
		);

$url = "http://www.".$locale[$l].$e;

header("Location: ".$url);

?>