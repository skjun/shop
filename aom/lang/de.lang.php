<?php
/********************************************************************
Associate-O-Matic v5.5.0
http://www.associate-o-matic.com

Justin Mecham
info@associate-o-matic.com

DESCRIPTION
Default language settings (UTF-8)

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

$version = "5.5.0";

// LANGUAGE SETTINGS

// LOCALE: Amazon.de
$str['1'] = "Start";
$str['2'] = "Erweiterte Suche";
$str['3'] = "Einkaufswagen";
$str['4'] = "Seite nicht gefunden";
$str['5'] = "Dieser Artikel ist nicht mehr vorhanden";
$str['6'] = "Diese Kategorie ist nicht mehr vorhanden";
$str['7'] = "Der Server ist momentan beschäftigt. Bitte aktualisieren Sie diese Seite in einigen Sekunden.";
$str['8'] = "Die Einkaufswagenfunktion erfordert, dass <a href=\"http://www.google.com/search?hl=en&q=enable+cookies \" target=\"_blank\">Cookies</a > erlaubt sind.<br/>";
$str['9'] = "Ihre Session ist abgelaufen, da Sie zu lange nicht aktiv waren. Bitte fangen Sie von vorne an.";
$str['10'] = "Artikel";
$str['11'] = "Kategorie";
$str['12'] = "Verkäufer";
$str['13'] = "Preis";
$str['14'] = "Menge";
$str['15'] = "Summe";
$str['16'] = "Teilsumme";
$str['17'] = "Ihr Einkaufswagen ist leer";
$str['18'] = "Klicken Sie bitte die ZURÜCK-Taste und versuchen Sie es noch einmal.";
$str['19'] = "Mit dem Einkauf fortfahren";
$str['20'] = "Zur Kasse gehen";
$str['21'] = "Ähnliche Artikel";
$str['22'] = "mehr";
$str['23'] = "weniger";
$str['24'] = "entfernen";
$str['25'] = "Entschuldigung";
$str['26'] = "ist nicht mehr von diesem Verkäufer lieferbar.";
$str['27'] = "Bitte";
$str['28'] = "nehmen Sie eine andere Auswahl vor";
$str['29'] = "zeige alle";
$str['30'] = "Artikel";
$str['31'] = "vergrössern";
$str['32'] = "Optionen wählen";
$str['33'] = "Bewertung";
$str['34'] = "Rezensionen";
$str['35'] = "Verkaufsrang";
$str['36'] = "ASIN";
$str['37'] = "Publikation";
$str['38'] = "Veröffentlichung";
$str['39'] = "Versand";
$str['40'] = "Verfügbarkeit";
$str['41'] = "Zustand";
$str['42'] = "Listenpreis";
$str['43'] = "Kaufen";
$str['44'] = "Zu Kaufen ab";
$str['45'] = "Sie Sparen";
$str['46'] = "Neu";
$str['47'] = "Gebraucht";
$str['48'] = "Sammlerstück";
$str['49'] = "Erneuert";
$str['50'] = "Zu niedrig zum Anzeigen";
$str['51'] = "ab";
$str['52'] = "durchschnittliche Bewertung";
$str['53'] = "Größe";
$str['54'] = "Farbe";
$str['55'] = "Ringgrösse";
$str['56'] = "Suche";
$str['57'] = "Auswählen";
$str['58'] = "In den Einkaufswagen";
$str['59'] = "Weitere Optionen wie Grösse, Farbe, etc. können nach dem Klick auf KAUFEN ausgewählt werden.";
$str['60'] = "Farben";
$str['61'] = "Andere Ansichten";
$str['62'] = "Autoren";
$str['63'] = "Autor";
$str['64'] = "Künstler";
$str['65'] = "Künstler";
$str['66'] = "Urheber";
$str['67'] = "Urheber";
$str['68'] = "Regie";
$str['69'] = "Regie";
$str['70'] = "Schauspieler";
$str['71'] = "Schauspieler";
$str['72'] = "Studio";
$str['73'] = "Label";
$str['74'] = "Marke";
$str['75'] = "Verleger";
$str['76'] = "Von";
$str['77'] = "Hersteller";
$str['78'] = "Kategorie";
$str['79'] = "Format";
$str['80'] = "Sprachen";
$str['81'] = "Sprache";
$str['82'] = "Artikel-Art";
$str['83'] = "Plattformen";
$str['84'] = "Plattform";
$str['85'] = "Land";
$str['86'] = "Bestandteile";
$str['87'] = "Gewebe-Art";
$str['88'] = "Art";
$str['89'] = "Bewertung";
$str['90'] = "ESRB";
$str['91'] = "Medium";
$str['92'] = "Ausgabe";
$str['93'] = "Signiert";
$str['94'] = "Erinnerungsstücke";
$str['95'] = "Zerbrechlich";
$str['96'] = "Leseniveau";
$str['97'] = "Laufzeit";
$str['98'] = "Zahl Der Einzelteile";
$str['99'] = "Seiten";
$str['100'] = "CDs/DVDs";
$str['101'] = "Seitenverhältnis";
$str['102'] = "DVD Layer";
$str['103'] = "DVD Seiten";
$str['104'] = "Bildformat";
$str['105'] = "Tracks";
$str['106'] = "Abo Ausgaben";
$str['107'] = "Abo Länge";
$str['108'] = "Ausgaben pro Jahr";
$str['109'] = "Erstlieferung";
$str['110'] = "Batterien";
$str['111'] = "Batterien im Lieferumfang enthalten";
$str['112'] = "Alter";
$str['113'] = "Höchstgewicht-Empfehlung";
$str['114'] = "CPU Hersteller";
$str['115'] = "CPU Geschwindigkeit";
$str['116'] = "CPU Art";
$str['117'] = "Prozessoren";
$str['118'] = "Systembus-Geschwindigkeit";
$str['119'] = "System Speicher";
$str['120'] = "Speicher-Art";
$str['121'] = "Maximaler Speicher";
$str['122'] = "Sekundärer Cache";
$str['123'] = "Tastatur";
$str['124'] = "Maus";
$str['125'] = "Notebook-Zeigegerät";
$str['126'] = "Soundkarte";
$str['127'] = "Lautsprecher";
$str['128'] = "Batterie-Art";
$str['129'] = "Festplatten";
$str['130'] = "Festplattengrösse";
$str['131'] = "CDRW";
$str['132'] = "DVDRW";
$str['133'] = "Diskettenlaufwerk";
$str['134'] = "Grafikkarte";
$str['135'] = "Grafik RAM";
$str['136'] = "Grafikkarte Schnittstelle";
$str['137'] = "Gehäuse";
$str['138'] = "Freie externe Erweiterungsplätze";
$str['139'] = "Freie interne Erweiterungsplätze";
$str['140'] = "Monitorgrösse";
$str['141'] = "Sichtbare Bildschirmdiagonale";
$str['142'] = "Maximale Farbtiefe";
$str['143'] = "Unterstützung externes Anzeigegerät";
$str['144'] = "Optische Auflösung";
$str['145'] = "Notebook-Display Technologie";
$str['146'] = "Modem";
$str['147'] = "Netzwerk";
$str['148'] = "PC-Card Slots";
$str['149'] = "Freie PCI-Slots";
$str['150'] = "Freie RAM-Slots";
$str['151'] = "Firewire-Ports";
$str['152'] = "NTSC Kamera-Schnittstelle";
$str['153'] = "Parallelport";
$str['154'] = "Serielle Schnittstellen";
$str['155'] = "SVideo Ausgang";
$str['156'] = "USB-Ports";
$str['157'] = "Ports USB2";
$str['158'] = "VGA Ausgänge";
$str['159'] = "inklusive Software";
$str['160'] = "Netzspannung";
$str['161'] = "Computer-Plattform";
$str['162'] = "Optischer Zoom";
$str['163'] = "Digitaler Zoom";
$str['164'] = "Auflösungs-Modi";
$str['165'] = "Bildtyp";
$str['166'] = "Konnektivität";
$str['167'] = "Anzeigegrösse";
$str['168'] = "Batterie";
$str['169'] = "Kamera-Eigenschaften lt. Hersteller";
$str['170'] = "Kompatibilität";
$str['171'] = "Geschwindigkeit Reihenaufnahme";
$str['172'] = "Verzögerung zwischen Aufnahmen";
$str['173'] = "Mit MP3-Player";
$str['174'] = "ISO Äquivalent";
$str['175'] = "Makrofokus-Brennweite";
$str['176'] = "Maximale Blendenöffnung";
$str['177'] = "Maximaler Fokus";
$str['178'] = "Minimaler Fokus";
$str['179'] = "Maximale Bildanzahl hohe Auflösung";
$str['180'] = "Maximale Horizontalauflösung";
$str['181'] = "Maximale Bildanzahl niedrige Auflösung";
$str['182'] = "Maximale Auflösung";
$str['183'] = "Minimale Verschlusszeit";
$str['184'] = "Maximale Verschlusszeit";
$str['185'] = "Maximale Vertikale Auflösung";
$str['186'] = "Minifilm";
$str['187'] = "Anzahl Schnellschüsse";
$str['188'] = "Blitz";
$str['189'] = "Wechselspeicher";
$str['190'] = "Hat Autofokus";
$str['191'] = "Hat Selbstauslöser";
$str['192'] = "Hat Einpunktbetrieb";
$str['193'] = "Hat intergrierte Editierfunktion";
$str['194'] = "Hat Video-Ausgang";
$str['195'] = "Hat Sucher";
$str['196'] = "Hat Rote Augen-Verminderung";
$str['197'] = "Hat Stativaufnahme";
$str['198'] = "Metallstempel";
$str['199'] = "Metall";
$str['200'] = "Einstellung";
$str['201'] = "Rückseite";
$str['202'] = "Band-Material";
$str['203'] = "Anzeigetafel-Material";
$str['204'] = "Gehäuse-Durchmesser";
$str['205'] = "Gehäuse-Material";
$str['206'] = "Gehäuse-Stärke";
$str['207'] = "Kette";
$str['208'] = "Haken";
$str['209'] = "Farbauswahl";
$str['210'] = "Auswahlfenster Materialart";
$str['211'] = "Edelsteinart";
$str['212'] = "synthetisch Hergestellt";
$str['213'] = "Zahl der Perlen";
$str['214'] = "Zahl der Steine";
$str['215'] = "Perlenglanz";
$str['216'] = "Perle Minimum-Farbe";
$str['217'] = "Perlenform";
$str['218'] = "Perlen Knüpfmethode";
$str['219'] = "Perle Oberflächenverunreinigungen";
$str['220'] = "Perle";
$str['221'] = "Perlen-Gleichförmigkeit";
$str['222'] = "Größe pro Perle";
$str['223'] = "Steinklarheit";
$str['224'] = "Steinfarbe";
$str['225'] = "Stein-Schnitt";
$str['226'] = "Steinform";
$str['227'] = "Steingewicht";
$str['228'] = "Diamant-Gewicht";
$str['229'] = "Edelstein-Gewicht";
$str['230'] = "Metallgewicht";
$str['231'] = "Uhrwerk";
$str['232'] = "Wasserbeständigkeit";
$str['233'] = "Kleidungsgrösse";
$str['234'] = "Ringgrösse";
$str['235'] = "Versandgewicht";
$str['236'] = "Maße (innen)";
$str['237'] = "Haftungssausschluss";
$str['238'] = "Garantie";
$str['239'] = "Hersteller-Funktionsgarantie";
$str['240'] = "Hersteller-Teilegarantie";
$str['241'] = "MPN";
$str['242'] = "Modell";
$str['243'] = "ISBN";
$str['244'] = "Dewey Dezimalzahl";
$str['245'] = "UPC";
$str['246'] = "EAN";
$str['247'] = "Freigabe-Datum";
$str['248'] = "Promotion";
$str['249'] = "AGB/Geschäftsbedingungen";
$str['250'] = "Eigenschaften";
$str['251'] = "Tracks";
$str['252'] = "CD/DVD";
$str['253'] = "Auch erhältlich in";
$str['254'] = "Zusatzgeräte";
$str['255'] = "Redaktionelle Rezensionen";
$str['256'] = "Kundenrezensionen";
$str['257'] = "Gelesen";
$str['258'] = "mehr Rezensionen";
$str['259'] = "aus";
$str['260'] = "fanden die folgende Rezension hilfreich";
$str['261'] = "Alle Produkte";
$str['262'] = "Einkaufswagen";
$str['263'] = "Standort";
$str['264'] = "Alle";
$str['265'] = "Nein";
$str['266'] = "Angebote für dieses Artikel";
$str['267'] = "Angebote gefunden auf dieser Seite";
$str['268'] = "Durchschnittliche Feedback-Bewertung";
$str['269'] = "Kommentiert";
$str['270'] = "mehr";
$str['271'] = "keine Kategorien ausgewählt";
$str['272'] = "von 5 Sternen";
$str['273'] = "Los";
$str['274'] = "Musiker";
$str['275'] = "Musik-Label";
$str['276'] = "Schauspielerin";
$str['277'] = "Sortieren nach";
$str['278'] = "Zeigen der Artikel";
$str['279'] = "von";
$str['280'] = "Zurück";
$str['281'] = "Weiter";
$str['282'] = "Zeigen der Angebote";
$str['283'] = "Rezensionen anzeigen";
$str['286'] = "Gehen Sie zur vorigen Seite zurück";
$str['287'] = "Sparversand";
$str['288'] = "Internationaler Versand möglich";
$str['289'] = "Express-Versand möglich";
$str['290'] = "Jetzt Vorbestellen";
$str['291'] = "Werktagen";
$str['292'] = "Einkaufswagen aktualisieren";
$str['293'] = "Es gibt nur {QTY} dieses Einzelteils ({TITLE}), das von diesem Verkäufer vorhanden ist.";
$str['294'] = "Ja";
$str['295'] = "Neu: Letztes 30 Tage";
$str['296'] = "Neu: Diese Woche";
$str['297'] = "Neu: Heute";
$str['298'] = "An 1 Tag";
$str['299'] = "An {DAYS} Tage";
$str['300'] = $month = Array('01' => 'Januar', '02' => 'Februar', '03' => 'März', '04' => 'April', '05' => 'Mai', '06' => 'Juni', '07' => 'Juli', '08' => 'August', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Dezember');
$str['301'] = "Aperature Modus";
$str['302'] = "Betriebssystem";
$str['303'] = "Handlagebestimmung";
$str['304'] = "Paket-Quantität";
$str['305'] = "Länge";
$str['306'] = "Breite";
$str['307'] = "Geruch";
$str['308'] = "Art";
$str['309'] = "Erhältlich";
$str['310'] = "Aroma";
$str['311'] = "Golfvereinflex";
$str['312'] = "Golfvereindachboden";
$str['313'] = "Golfvereinwelle";
$str['314'] = "Material";
$str['315'] = "Höchstgewicht";
$str['316'] = "Suche";
$str['317'] = "Neu";
$str['318'] = "Gebraucht";
$str['319'] = "Sammlerstück";
$str['320'] = "Erneuert";
$str['321'] = "Neu";
$str['322'] = "Gebraucht";
$str['323'] = "Sammlerstück";
$str['324'] = "Erneuert";
$str['325'] = "Episode";
$str['326'] = "Bitte treffen Sie eine Vorauswahl";
$str['327'] = "Genre";
$str['328'] = "Titel";
$str['329'] = "Ähnliche aufgerufene Artikel";
$str['330'] = "Ähnliche Artikel aus anderen Kategorien";
$str['331'] = "Bestseller";
$str['332'] = "Neue Releases";
$str['333'] = "Photo";
$str['334'] = "Suche innerhalb der Ergebnisse";
$str['335'] = "Anzahl der Discs";
$str['336'] = "Monate";
$str['337'] = "Wochen";
$str['338'] = "Minuten";
$str['339'] = "Region";
$str['340'] = "Keine Resultate gefunden";
$str['341'] = "Startjahr";
$str['342'] = "Jahreszeit";
$str['343'] = "Ursprüngliches Luft-Datum";
$str['344'] = "Beschreibung";
$str['345'] = "Auf den Wunschzettel";
$str['346'] = "Add to Wedding Registry";
$str['347'] = "Add to Baby Registry";
$str['348'] = "Tell-A-Friend";
$str['349'] = "Abonnieren";
$str['350'] = "Gehe zu Seite";
$str['351'] = " (Page {PAGE})"; // used with {PAGE} variable in titles
$str['352'] = "Sammlung anzeigen";
$str['353'] = "Kundenrezensionen"; // used with {REVIEWS} variable in certain titles
$str['354'] = "Marktplatz"; // used with {OFFERS} variable in certain titles
$str['355'] = "Stand"; // next to price info - required by Amazon terms of use
$str['356'] = "Einzelheiten"; // next to price info - required by Amazon terms of use
$str['357'] = "Die Produktpreise und die Produktverfügbarkeit sind zum angegebenen Datum / Zeitpunkt korrekt und können sich ändern. Die Preis- und Verfügbarkeitsangaben, die auf amazon.de zum Zeitpunkt des Kaufs angezeigt werden, gelten für den Verkauf dieses Produktes."; // next to price info - exact wording required by Amazon terms of use
$str['358'] = "Kein Suchergebnis";
$str['359'] = "Auftrags-Status";
$str['360'] = "Auftragsnummer";
$str['361'] = "Auftrags-Datum";
$str['362'] = "Auftrags-Gesamtmenge";
$str['363'] = "Status";
$str['364'] = "Jahr"; // auto parts finder
$str['365'] = "Machen"; // auto parts finder
$str['366'] = "Modell"; // auto parts finder
$str['367'] = "Veränderung Fahrzeug"; // auto parts finder
$str['368'] = "Trim"; // auto parts finder
$str['369'] = "Bed";
$str['370'] = "Body Style";
$str['371'] = "Bremsen";
$str['372'] = "Drive Type";
$str['373'] = "Motor";
$str['374'] = "Body Code";
$str['375'] = "Federtypen";
$str['376'] = "Steering";
$str['377'] = "Transmission";
$str['378'] = "Radstand";
$str['379'] = "Um sicher zu sein diese Teile passen Sie <b>{YEAR} {MAKE} {MODEL}</b>, bitte geben Sie uns weitere Informationen weiter unten.";
$str['380'] = "Sitemap";
$str['381'] = "Kategorien";
$str['382'] = "Wir konnten keine Ergebnisse für Ihre Anfrage.";
$str['383'] = "Der Artikel wurde erfolgreich in den Warenkorb gelegt";
$str['384'] = "Alle Ergebnisse anzeigen";
$str['385'] = "(entfernen {KEYWORD} aus Suche)";
$str['386'] = "-oben";
$str['387'] = "Kaufleute";
$str['388'] = "Auf deutsch";
$str['389'] = "Alle Ergebnisse anzeigen";

// DISCLAIMER REQUIRED BY AMAZON TO BE ON ALL STORE PAGES
$str['999'] = "GEWISSE INHALTE, DIE AUF DIESER WEBSITE ERSCHEINEN, STAMMEN VON AMAZON EU SARL. DIESE INHALTE WERDEN SO, WIE SIE SIND ZUR VERFÜGUNG GESTELLT UND KÖNNEN JEDERZEIT GEÄNDERT ODER ENTFERNT WERDEN.";

?>