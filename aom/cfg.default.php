<?php
/********************************************************************
Associate-O-Matic v5.5.0
http://www.associate-o-matic.com

Justin Mecham
info@associate-o-matic.com

DESCRIPTION
Default categories configuration settings

Copyright (c) 2004-2014 Associate-O-Matic. All Rights Reserved.
********************************************************************/

// *** DO NOT MODIFY ANYTHING BELOW THIS LINE UNLESS DIRECTED, AS ASSOCIATE-O-MATIC MAY NOT WORK PROPERLY ***

$version = "5.5.0";

//////////////////////////
// Categories: Amazon.com
// Amazon.com: Apparel
$cfg['Categories']['list']['Amazon.com']['Apparel']['name'] = "Apparel";
$cfg['Categories']['list']['Amazon.com']['Apparel']['node'] = "1036682";
$cfg['Categories']['list']['Amazon.com']['Apparel']['nodes']['1036682'] = Array(
	'1040660'	=>  'Women',
	'1040658'	=>  'Men',
	'1040662'	=>  'Kids & Baby',
	);
// Amazon.com: Appliances
$cfg['Categories']['list']['Amazon.com']['Appliances']['name'] = "Appliances";
$cfg['Categories']['list']['Amazon.com']['Appliances']['node'] = "2619526011";
$cfg['Categories']['list']['Amazon.com']['Appliances']['nodes']['2619526011'] = Array(
	'267554011'	=>  'Air Purifiers',
	'3741271'	=>  'Dishwashers',
	'289935'	=>  'Microwave Ovens',
	);
// Amazon.com: ArtsAndCrafts
$cfg['Categories']['list']['Amazon.com']['ArtsAndCrafts']['name'] = "Arts & Crafts";
$cfg['Categories']['list']['Amazon.com']['ArtsAndCrafts']['node'] = "2617942011";
$cfg['Categories']['list']['Amazon.com']['ArtsAndCrafts']['nodes']['2617942011'] = Array(
	'2747968011'=>  'Art Supplies',
	'378733011'	=>  'Craft Supplies',
	'12898821'	=>  'Scrapbooking',
	);
// Amazon.com: Automotive
$cfg['Categories']['list']['Amazon.com']['Automotive']['name'] = "Automotive";
$cfg['Categories']['list']['Amazon.com']['Automotive']['node'] = "15690151";
$cfg['Categories']['list']['Amazon.com']['Automotive']['nodes']['15690151'] = Array(
	'15857511' =>  'Exterior Accessories',
	'15857501' =>  'Interior Accessories',
	'15710351' =>  'Performance Parts',
	);
// Amazon.com: Baby
$cfg['Categories']['list']['Amazon.com']['Baby']['name'] = "Baby";
$cfg['Categories']['list']['Amazon.com']['Baby']['node'] = "165797011";
$cfg['Categories']['list']['Amazon.com']['Baby']['nodes']['165797011'] = Array(
	'166742011'  =>  'Bedding',
	'166842011'  =>  'Strollers',
	'166828011'  =>  'Gear',
	);
// Amazon.com: Beauty
$cfg['Categories']['list']['Amazon.com']['Beauty']['name'] = "Beauty";
$cfg['Categories']['list']['Amazon.com']['Beauty']['node'] = "11055981";
$cfg['Categories']['list']['Amazon.com']['Beauty']['nodes']['11055981'] = Array(
	'11055991' => 'Bath & Shower',
	'11056591' => 'Fragrance',
	'11057241' => 'Hair Care',
	);
// Amazon.com: Books
$cfg['Categories']['list']['Amazon.com']['Books']['name'] = "Books";
$cfg['Categories']['list']['Amazon.com']['Books']['node'] = "1000";
$cfg['Categories']['list']['Amazon.com']['Books']['nodes']['1000'] = Array(
	'3' 		=>	'Business',
	'17' 		=>	'Fiction',
	'53' 		=>	'Nonfiction',
	);
// Amazon.com: Collectibles
$cfg['Categories']['list']['Amazon.com']['Collectibles']['name'] = "Collectibles";
$cfg['Categories']['list']['Amazon.com']['Collectibles']['node'] = "4991426011";
$cfg['Categories']['list']['Amazon.com']['Collectibles']['nodes']['4991426011'] = Array(
    '5088769011'        =>  'Entertainment',
    '3250697011'        =>  'Sports',
    );
// Amazon.com: Computers
$cfg['Categories']['list']['Amazon.com']['PCHardware']['name'] = "Computers";
$cfg['Categories']['list']['Amazon.com']['PCHardware']['node'] = "541966";
$cfg['Categories']['list']['Amazon.com']['PCHardware']['nodes']['541966'] = Array(
	'565118'	=>  'Brands',
	'565098'  =>  'Desktops',
	'172455'  =>  'Computer Add-Ons',
	);
// Amazon.com: DVD
$cfg['Categories']['list']['Amazon.com']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.com']['DVD']['node'] = "2625374011";
$cfg['Categories']['list']['Amazon.com']['DVD']['nodes']['2625374011'] = Array(
	'2649512011'  	=>  'Movies',
	'2649513011'  	=>  'TV Shows',
	);
// Amazon.com: Electronics
$cfg['Categories']['list']['Amazon.com']['Electronics']['name'] = "Electronics";
$cfg['Categories']['list']['Amazon.com']['Electronics']['node'] = "493964";
$cfg['Categories']['list']['Amazon.com']['Electronics']['nodes']['493964'] = Array(
	'226721'  =>  'Brands',
	'1065836' =>  'Audio & Video',
	'172526'  =>  'GPS',
	);
// Amazon.com: Gift Cards
$cfg['Categories']['list']['Amazon.com']['GiftCards']['name'] = "Gift Cards";
$cfg['Categories']['list']['Amazon.com']['GiftCards']['node'] = "2973079011";
$cfg['Categories']['list']['Amazon.com']['GiftCards']['nodes']['2973079011'] = Array(
	'3063530011' =>  'Amazon Gift Cards',
	'2973081011' =>  'Books, Movies & Music',
	'2973090011' =>  'Restaurants',
	'2973096011' =>  'Video Games & Online Games'
	);
// Amazon.com: Gourmet Food
$cfg['Categories']['list']['Amazon.com']['GourmetFood']['name'] = "Gourmet Food";
$cfg['Categories']['list']['Amazon.com']['GourmetFood']['node'] = "2255571011";
$cfg['Categories']['list']['Amazon.com']['GourmetFood']['nodes']['2255571011'] = Array(
	'2255572011' =>  'Candy',
	'2255573011' =>  'Cheese',
	'2255574011' =>  'Chocolate',
	);
// Amazon.com: Grocery
$cfg['Categories']['list']['Amazon.com']['Grocery']['name'] = "Grocery";
$cfg['Categories']['list']['Amazon.com']['Grocery']['node'] = "16310211";
$cfg['Categories']['list']['Amazon.com']['Grocery']['nodes']['16310211'] = Array(
	'16310301' =>  'Snacks, Cookies & Candy',
	'16310251' =>  'Breakfast Foods',
	'16310331' =>  'Baking Supplies',
	);
// Amazon.com: Health/Personal Care
$cfg['Categories']['list']['Amazon.com']['HealthPersonalCare']['name'] = "Health";
$cfg['Categories']['list']['Amazon.com']['HealthPersonalCare']['node'] = "3760931";
$cfg['Categories']['list']['Amazon.com']['HealthPersonalCare']['nodes']['3760931'] = Array(
	'3760941' =>  'Health Care',
	'3764441' =>  'Nutrition',
	'3775161' =>  'Medical Supplies',
	);
// Amazon.com: HomeGarden
$cfg['Categories']['list']['Amazon.com']['HomeGarden']['name'] = "Home & Garden";
$cfg['Categories']['list']['Amazon.com']['HomeGarden']['node'] = "1063498";
$cfg['Categories']['list']['Amazon.com']['HomeGarden']['nodes']['1063498'] = Array(
	'1057792' =>  'Bed & Bath',
	'510080'  =>  'Housewares',
	'1057794' =>  'Furniture & Decor',
	);
// Amazon.com: Industrial
$cfg['Categories']['list']['Amazon.com']['Industrial']['name'] = "Industrial & Science";
$cfg['Categories']['list']['Amazon.com']['Industrial']['node'] = "16310161";
$cfg['Categories']['list']['Amazon.com']['Industrial']['nodes']['16310161'] = Array(
	'16310191' =>  'Raw Materials',
	'16310171' =>  'Fasteners',
	'16310181' =>  'Mechanical Components',
	);
// Amazon.com: Jewelry
$cfg['Categories']['list']['Amazon.com']['Jewelry']['name'] = "Jewelry";
$cfg['Categories']['list']['Amazon.com']['Jewelry']['node'] = "3880591";
$cfg['Categories']['list']['Amazon.com']['Jewelry']['nodes']['3880591'] = Array(
	'3887251'	=>  'Rings',
	'3888811'	=>  'Watches',
	'16014541'=>  'Wedding',
	);
// Amazon.com: KindleStore
$cfg['Categories']['list']['Amazon.com']['KindleStore']['name'] = "Kindle eBooks";
$cfg['Categories']['list']['Amazon.com']['KindleStore']['node'] = "358606011";
$cfg['Categories']['list']['Amazon.com']['KindleStore']['nodes']['358606011'] = Array(
	'154606011'  =>  'Kindle eBooks',
	'241646011'  =>  'Kindle Magazines',
	'165389011'  =>  'Kindle Newspapers',
	);
// Amazon.com: Kitchen
$cfg['Categories']['list']['Amazon.com']['Kitchen']['name'] = "Kitchen";
$cfg['Categories']['list']['Amazon.com']['Kitchen']['node'] = "284507";
$cfg['Categories']['list']['Amazon.com']['Kitchen']['nodes']['284507'] = Array(
	'291358'  =>  'Brands',
	'289814'  =>  'Cookware',
	'13162311'=>  'Tableware',
	);
// Amazon.com: LawnAndGarden
$cfg['Categories']['list']['Amazon.com']['LawnAndGarden']['name'] = "Lawn & Garden";
$cfg['Categories']['list']['Amazon.com']['LawnAndGarden']['node'] = "3238155011";
$cfg['Categories']['list']['Amazon.com']['LawnAndGarden']['nodes']['3238155011'] = Array(
	'3610851'  =>  'Gardening',
	'551242'  =>  'Mowers & Outdoor Power Tools',
	'553788'  =>  'Outdoor Décor'
	);
// Amazon.com: Magazines
$cfg['Categories']['list']['Amazon.com']['Magazines']['name'] = "Magazines";
$cfg['Categories']['list']['Amazon.com']['Magazines']['node'] = "599872";
$cfg['Categories']['list']['Amazon.com']['Magazines']['nodes']['599872'] = Array(
	'604876'  =>  'Titles, A-Z',
	'602352'  =>  'Men\'s Interest',
	'602372'  =>  'Women\'s Interest',
	);
// Amazon.com: Miscellaneous
$cfg['Categories']['list']['Amazon.com']['Miscellaneous']['name'] = "Miscellaneous";
$cfg['Categories']['list']['Amazon.com']['Miscellaneous']['node'] = "10304191";
$cfg['Categories']['list']['Amazon.com']['Miscellaneous']['nodes']['10304191'] = Array(
	'2478844011'  =>  'Games',
	'2478856011'  =>  'News & Weather',
	'2478860011'  =>  'Photography',
	);
// Amazon.com: MobileApps
$cfg['Categories']['list']['Amazon.com']['MobileApps']['name'] = "Mobile Apps";
$cfg['Categories']['list']['Amazon.com']['MobileApps']['node'] = "2350150011";
$cfg['Categories']['list']['Amazon.com']['MobileApps']['nodes']['2350150011'] = Array(
	'2478844011'  =>  'Games',
	'2478856011'  =>  'News & Weather',
	'2478860011'  =>  'Photography',
	);
// Amazon.com: MP3 Downloads
$cfg['Categories']['list']['Amazon.com']['MP3Downloads']['name'] = "MP3 Downloads";
$cfg['Categories']['list']['Amazon.com']['MP3Downloads']['node'] = "624868011";
$cfg['Categories']['list']['Amazon.com']['MP3Downloads']['nodes']['624868011'] = Array(
	'625092011' =>  'Pop',
	'625105011' =>  'R&B',
	'625129011' =>  'Rock',
	);
// Amazon.com: Music
$cfg['Categories']['list']['Amazon.com']['Music']['name'] = "Music";
$cfg['Categories']['list']['Amazon.com']['Music']['node'] = "301668";
$cfg['Categories']['list']['Amazon.com']['Music']['nodes']['301668'] = Array(
	'37' 		=>  'Pop',
	'40'		=>	'Rock',
	'42' 		=>  'Soundtracks',
	);
// Amazon.com: Musical Instruments
$cfg['Categories']['list']['Amazon.com']['MusicalInstruments']['name'] = "Musical Instruments";
$cfg['Categories']['list']['Amazon.com']['MusicalInstruments']['node'] = "11965861";
$cfg['Categories']['list']['Amazon.com']['MusicalInstruments']['nodes']['11965861'] = Array(
	'11970241' =>  'Drums & Percussion',
	'11971241' =>  'Guitars',
	'11970071' =>  'Pianos',
	);
// Amazon.com: Office Products
$cfg['Categories']['list']['Amazon.com']['OfficeProducts']['name'] = "Office Products";
$cfg['Categories']['list']['Amazon.com']['OfficeProducts']['node'] = "1084128";
$cfg['Categories']['list']['Amazon.com']['OfficeProducts']['nodes']['1084128'] = Array(
	'1086182'	=>  'Brands',
	'1069102'	=>  'Furniture',
	'1069242'	=>  'Office Supplies',
	);
// Amazon.com: Outdoor Living
$cfg['Categories']['list']['Amazon.com']['OutdoorLiving']['name'] = "Outdoor Living";
$cfg['Categories']['list']['Amazon.com']['OutdoorLiving']['node'] = "286168";
$cfg['Categories']['list']['Amazon.com']['OutdoorLiving']['nodes']['286168'] = Array(
	'892986'  =>  'Camping',
	'553760'  =>  'Cooking',
	'553824'  =>  'Patio Furniture',
	);
// Amazon.com: Pet Supplies
$cfg['Categories']['list']['Amazon.com']['PetSupplies']['name'] = "Pet Supplies";
$cfg['Categories']['list']['Amazon.com']['PetSupplies']['node'] = "2619534011";
$cfg['Categories']['list']['Amazon.com']['PetSupplies']['nodes']['2619534011'] = Array(
	'2975241011' =>  'Cats',
	'2975312011' =>  'Dogs',
	'2975446011' =>  'Fish',
	);
// Amazon.com: Photo & Camera
$cfg['Categories']['list']['Amazon.com']['Photo']['name'] = "Photo & Camera";
$cfg['Categories']['list']['Amazon.com']['Photo']['node'] = "502394";
$cfg['Categories']['list']['Amazon.com']['Photo']['nodes']['502394'] = Array(
	'493666'	=>  'Brands',
	'172421'	=>  'Camcorders',
	'281052'	=>  'Digital Cameras',
	);
// Amazon.com: Shoes
$cfg['Categories']['list']['Amazon.com']['Shoes']['name'] = "Shoes";
$cfg['Categories']['list']['Amazon.com']['Shoes']['node'] = "672124011";
$cfg['Categories']['list']['Amazon.com']['Shoes']['nodes']['672124011'] = Array(
	'679337011'  =>  'Womens',
	'679255011'  =>  'Mens',
	'684538011'  =>  'Accessories',
	);
// Amazon.com: Software
$cfg['Categories']['list']['Amazon.com']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.com']['Software']['node'] = "491286";
$cfg['Categories']['list']['Amazon.com']['Software']['nodes']['491286'] = Array(
	'409488'  =>  'Brands',
	'491286'  =>  'Categories',
	'300228'  =>  'Outlet',
	);
// Amazon.com: Sporting Goods
$cfg['Categories']['list']['Amazon.com']['SportingGoods']['name'] = "Sporting Goods";
$cfg['Categories']['list']['Amazon.com']['SportingGoods']['node'] = "3375301";
$cfg['Categories']['list']['Amazon.com']['SportingGoods']['nodes']['3375301'] = Array(
	'3394801'	=>  'Accessories',
	'3386071'	=>  'Fan Shop',
	'3392741'	=>  'Footwear',
	);
// Amazon.com: Tools & Hardware
$cfg['Categories']['list']['Amazon.com']['Tools']['name'] = "Tools & Hardware";
$cfg['Categories']['list']['Amazon.com']['Tools']['node'] = "468240";
$cfg['Categories']['list']['Amazon.com']['Tools']['nodes']['468240'] = Array(
	'228239'	=>  'Brands',
	'551238'	=>  'Hand Tools',
	'551236'	=>  'Power Tools',
	);
// Amazon.com: Toys
$cfg['Categories']['list']['Amazon.com']['Toys']['name'] = "Toys";
$cfg['Categories']['list']['Amazon.com']['Toys']['node'] = "165795011";
$cfg['Categories']['list']['Amazon.com']['Toys']['nodes']['165795011'] = Array(
	'165794011'		=>  'Age Ranges',
	'165993011'  	=>  'Action Figures',
    '166220011'		=>	'Games',
	);
// Amazon.com: VHS
$cfg['Categories']['list']['Amazon.com']['VHS']['name'] = "VHS";
$cfg['Categories']['list']['Amazon.com']['VHS']['node'] = "2650307011";
$cfg['Categories']['list']['Amazon.com']['VHS']['nodes']['2650307011'] = Array(

	);
// Amazon.com: Unbox
$cfg['Categories']['list']['Amazon.com']['UnboxVideo']['name'] = "Video On Demand";
$cfg['Categories']['list']['Amazon.com']['UnboxVideo']['node'] = "2625374011";
$cfg['Categories']['list']['Amazon.com']['UnboxVideo']['nodes']['2625374011'] = Array(
	'2649512011'  	=>  'Movies',
	'2649513011'  	=>  'TV Shows',
	);
// Amazon.com: PC & Video Games
$cfg['Categories']['list']['Amazon.com']['VideoGames']['name'] = "PC & Video Games";
$cfg['Categories']['list']['Amazon.com']['VideoGames']['node'] = "11846801";
$cfg['Categories']['list']['Amazon.com']['VideoGames']['nodes']['11846801'] = Array(
	'229575'   =>  'PC Games',
	'14218901' =>  'Wii',
	'14220161' =>  'Xbox 360',
	'14210751' =>  'PlayStation 3',
	);
// Amazon.com: Watches
$cfg['Categories']['list']['Amazon.com']['Watches']['name'] = "Watches";
$cfg['Categories']['list']['Amazon.com']['Watches']['node'] = "378516011";
$cfg['Categories']['list']['Amazon.com']['Watches']['nodes']['378516011'] = Array(
	'378521011'	=>	'Casual Watches',
	'378523011'	=>	'Dress Watches',
	'378526011'	=>	'Sport Watches',
	);
// Amazon.com: Wireless
$cfg['Categories']['list']['Amazon.com']['Wireless']['name'] = "Wireless";
$cfg['Categories']['list']['Amazon.com']['Wireless']['node'] = "2335753011";
$cfg['Categories']['list']['Amazon.com']['Wireless']['nodes']['2335753011'] = Array(
	'2407747011'	=>	'Phones with Plans',
	'2407748011'	=>	'No-Contract Phones',
	'2407749011'	=>	'Unlocked Phones',
	);
// Amazon.com: Wireless
$cfg['Categories']['list']['Amazon.com']['WirelessAccessories']['name'] = "Wireless Accessories";
$cfg['Categories']['list']['Amazon.com']['WirelessAccessories']['node'] = "2407755011";
$cfg['Categories']['list']['Amazon.com']['WirelessAccessories']['nodes']['2407755011'] = Array(
	'2407760011'	=>	'Cases & Covers',
	'2407761011'	=>	'Chargers',
	'2407775011'	=>	'Headsets',
	);

////////////////////////////
// Categories: Amazon.co.uk
// Amazon.co.uk: Apparel
$cfg['Categories']['list']['Amazon.co.uk']['Apparel']['name'] = "Apparel";
$cfg['Categories']['list']['Amazon.co.uk']['Apparel']['node'] = "83451031";
$cfg['Categories']['list']['Amazon.co.uk']['Apparel']['nodes']['83451031'] = Array(
	'116178031'	=>  'Shirts',
	'116180031'	=>  'Trousers & Jeans',
	'116183031'	=>  'Dresses',
	);
// Amazon.co.uk: Automotive
$cfg['Categories']['list']['Amazon.co.uk']['Automotive']['name'] = "Automotive";
$cfg['Categories']['list']['Amazon.co.uk']['Automotive']['node'] = "248878031";
$cfg['Categories']['list']['Amazon.co.uk']['Automotive']['nodes']['248878031'] = Array(
	'301308031'	=>  'Car Accessories',
	'301311031'	=>  'Motorbike Accessories & Parts',
	'301312031'	=>  'Tools, Maintenance & Care',
	);
// Amazon.co.uk: Baby
$cfg['Categories']['list']['Amazon.co.uk']['Baby']['name'] = "Baby";
$cfg['Categories']['list']['Amazon.co.uk']['Baby']['node'] = "60032031";
$cfg['Categories']['list']['Amazon.co.uk']['Baby']['nodes']['60032031'] = Array(
	'60036031'  =>  'Car Seats & Carriers',
	'60039031'  =>  'Bedding',
	'60038031'  =>  'Furniture',
	);
// Amazon.co.uk: Beauty
$cfg['Categories']['list']['Amazon.co.uk']['Beauty']['name'] = "Beauty";
$cfg['Categories']['list']['Amazon.co.uk']['Beauty']['node'] = "117333031";
$cfg['Categories']['list']['Amazon.co.uk']['Beauty']['nodes']['117333031'] = Array(
	'118423031'  =>  'Cosmetics',
	'118457031'  =>  'Fragrances',
	'118464031'  =>  'Skin Care',
	);
// Amazon.co.uk: Books
$cfg['Categories']['list']['Amazon.co.uk']['Books']['name'] = "Books";
$cfg['Categories']['list']['Amazon.co.uk']['Books']['node'] = "1025612";
$cfg['Categories']['list']['Amazon.co.uk']['Books']['nodes']['1025612'] = Array(
	'68' 		=>	'Business',
	'62' 		=>	'Fiction',
	'65' 		=>	'History',
	);
// Amazon.co.uk: DVD
$cfg['Categories']['list']['Amazon.co.uk']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.co.uk']['DVD']['node'] = "573406";
$cfg['Categories']['list']['Amazon.co.uk']['DVD']['nodes']['573406'] = Array(
	'501778'  =>  'Action',
	'501866'  =>  'Comedy',
	'501872'  =>  'Drama',
	);
// Amazon.co.uk: Electronics
$cfg['Categories']['list']['Amazon.co.uk']['Electronics']['name'] = "Electronics";
$cfg['Categories']['list']['Amazon.co.uk']['Electronics']['node'] = "560800";
$cfg['Categories']['list']['Amazon.co.uk']['Electronics']['nodes']['560800'] = Array(
	'389514011' =>  'GPS',
	'560834' 	=>  'Photography',
	'560858'  	=>  'Sound & Vision',
	);
// Amazon.co.uk: Grocery
$cfg['Categories']['list']['Amazon.co.uk']['Grocery']['name'] = "Grocery";
$cfg['Categories']['list']['Amazon.co.uk']['Grocery']['node'] = "344155031";
$cfg['Categories']['list']['Amazon.co.uk']['Grocery']['nodes']['344155031'] = Array(
	'358593031' =>  'Jams, Honey & Spreads',
	'358594031' =>  'Meat, Poultry & Sausages',
	'358605031' =>  'Sweets, Mints & Gum',
	);
// Amazon.co.uk: Health/Personal Care
$cfg['Categories']['list']['Amazon.co.uk']['HealthPersonalCare']['name'] = "Health";
$cfg['Categories']['list']['Amazon.co.uk']['HealthPersonalCare']['node'] = "66280031";
$cfg['Categories']['list']['Amazon.co.uk']['HealthPersonalCare']['nodes']['66280031'] = Array(
	'66471031'=>  'Beauty',
	'66468031' =>  'Personal Care',
	'66467031'=>  'Health Care',
	);
// Amazon.co.uk: Home/Garden
$cfg['Categories']['list']['Amazon.co.uk']['HomeGarden']['name'] = "Home/Garden";
$cfg['Categories']['list']['Amazon.co.uk']['HomeGarden']['node'] = "3147411";
$cfg['Categories']['list']['Amazon.co.uk']['HomeGarden']['nodes']['3147411'] = Array(
	'79903031'=>  'DIY & Tools',
	'11052671'=>  'Garden & Outdoors',
	'11052681'=>  'Kitchen & Dining',
	);
// Amazon.co.uk: Home Improvement
$cfg['Categories']['list']['Amazon.co.uk']['HomeImprovement']['name'] = "Home Improvement";
$cfg['Categories']['list']['Amazon.co.uk']['HomeImprovement']['node'] = "10709121";
$cfg['Categories']['list']['Amazon.co.uk']['HomeImprovement']['nodes']['10709121'] = Array(
	'14279821'=>  'Artwork',
	'10745681'=>  'Furniture',
	'10709301'=>  'Lighting',
	);
// Amazon.co.uk: Jewellery
$cfg['Categories']['list']['Amazon.co.uk']['Jewelry']['name'] = "Jewellery";
$cfg['Categories']['list']['Amazon.co.uk']['Jewelry']['node'] = "193717031";
$cfg['Categories']['list']['Amazon.co.uk']['Jewelry']['nodes']['193717031'] = Array(
	'197366031'	=>  'Earrings',
	'197378031'	=>  'Neckwear',
	'197392031'	=>  'Rings',
	);
// Amazon.co.uk: KindleStore
$cfg['Categories']['list']['Amazon.co.uk']['KindleStore']['name'] = "Kindle eBooks";
$cfg['Categories']['list']['Amazon.co.uk']['KindleStore']['node'] = "341678031";
$cfg['Categories']['list']['Amazon.co.uk']['KindleStore']['nodes']['341678031'] = Array(
	'341689031'  =>  'Books',
	'341690031'  =>  'Magazines',
	'341691031'  =>  'Newspapers',
	);
// Amazon.co.uk: Kitchen
$cfg['Categories']['list']['Amazon.co.uk']['Kitchen']['name'] = "Kitchen";
$cfg['Categories']['list']['Amazon.co.uk']['Kitchen']['node'] = "3147411";
$cfg['Categories']['list']['Amazon.co.uk']['Kitchen']['nodes']['3147411'] = Array(
	'10708921' =>  'Bakeware',
	'3147471'  =>  'Cookware',
	'10708551' =>  'Tableware',
	);
// Amazon.co.uk: Lighting
$cfg['Categories']['list']['Amazon.co.uk']['Lighting']['name'] = "Lighting";
$cfg['Categories']['list']['Amazon.co.uk']['Lighting']['node'] = "213078031";
$cfg['Categories']['list']['Amazon.co.uk']['Lighting']['nodes']['213078031'] = Array(
	'10709301'	=>  'Indoor Lighting',
	'10709361'	=>  'Outdoor Lighting',
	'227259031'	=>  'Light Bulbs',
	);
// Amazon.co.uk: MP3Downloads
$cfg['Categories']['list']['Amazon.co.uk']['MP3Downloads']['name'] = "MP3 Downloads";
$cfg['Categories']['list']['Amazon.co.uk']['MP3Downloads']['node'] = "77198031";
$cfg['Categories']['list']['Amazon.co.uk']['MP3Downloads']['nodes']['77198031'] = Array(
	'78166031'	=>  'Rock',
	'78132031' 	=>  'Pop',
	'78144031' 	=>  'R&B',
	'78180031' 	=>  'Soundtracks',
	);
// Amazon.co.uk: Music
$cfg['Categories']['list']['Amazon.co.uk']['Music']['name'] = "Music";
$cfg['Categories']['list']['Amazon.co.uk']['Music']['node'] = "520920";
$cfg['Categories']['list']['Amazon.co.uk']['Music']['nodes']['520920'] = Array(
	'231239'	=>  'Rock',
	'694208' 	=>  'Pop',
	'754576' 	=>  'R&B',
	'231249' 	=>  'Soundtracks',
	);
// Amazon.co.uk: Musical Instruments
$cfg['Categories']['list']['Amazon.co.uk']['MusicalInstruments']['name'] = "Musical Instruments";
$cfg['Categories']['list']['Amazon.co.uk']['MusicalInstruments']['node'] = "340838031";
$cfg['Categories']['list']['Amazon.co.uk']['MusicalInstruments']['nodes']['340838031'] = Array(
    '406553031' =>  'Drums & Percussion',
    '406550031' =>  'Guitars',
    '406552031' =>  'Pianos',
    );
// Amazon.co.uk: Office Products
$cfg['Categories']['list']['Amazon.co.uk']['OfficeProducts']['name'] = "Office Products";
$cfg['Categories']['list']['Amazon.co.uk']['OfficeProducts']['node'] = "192414031";
$cfg['Categories']['list']['Amazon.co.uk']['OfficeProducts']['nodes']['192414031'] = Array(
	'197745031'	=>  'Office Paper Products',
	'197743031'	=>  'Office Supplies',
	'197748031'	=>  'Pens & Pencils',
	);
// Amazon.co.uk: Outdoor Living
$cfg['Categories']['list']['Amazon.co.uk']['OutdoorLiving']['name'] = "Outdoor Living";
$cfg['Categories']['list']['Amazon.co.uk']['OutdoorLiving']['node'] = "10709021";
$cfg['Categories']['list']['Amazon.co.uk']['OutdoorLiving']['nodes']['10709021'] = Array(
	'11714121'  =>  'Barbecues',
	'11714171'  =>  'Garden Furniture',
	'11714761'  =>  'Tools',
	);
// Amazon.co.uk: Shoes
$cfg['Categories']['list']['Amazon.co.uk']['Shoes']['name'] = "Shoes";
$cfg['Categories']['list']['Amazon.co.uk']['Shoes']['node'] = "362350011";
$cfg['Categories']['list']['Amazon.co.uk']['Shoes']['nodes']['362350011'] = Array(
	'362354011'  =>  'Athletic & Outdoor',
	'362356011'  =>  'Shoes',
	'362355011'  =>  'Boots',
	);
// Amazon.co.uk: Software
$cfg['Categories']['list']['Amazon.co.uk']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.co.uk']['Software']['node'] = "1025614";
$cfg['Categories']['list']['Amazon.co.uk']['Software']['nodes']['1025614'] = Array(
	'600014'  =>  'Business',
	'600136'  =>  'Graphics',
	'600236'  =>  'Video',
	);
// Amazon.co.uk: SportingGoods
$cfg['Categories']['list']['Amazon.co.uk']['SportingGoods']['name'] = "Sports & Leisure";
$cfg['Categories']['list']['Amazon.co.uk']['SportingGoods']['node'] = "319530011";
$cfg['Categories']['list']['Amazon.co.uk']['SportingGoods']['nodes']['319530011'] = Array(
	'319535011'  =>  'Fitness',
	'324052011'  =>  'Running',
	'319537011'  =>  'Team Sports',
	);
// Amazon.co.uk: Tools
$cfg['Categories']['list']['Amazon.co.uk']['Tools']['name'] = "Tools";
$cfg['Categories']['list']['Amazon.co.uk']['Tools']['node'] = "84124031";
$cfg['Categories']['list']['Amazon.co.uk']['Tools']['nodes']['84124031'] = Array(
	'114414031'	=>  'Hand Tools',
    '114614031'	=>	'Power Tools',
	'13714471'	=>  'Automotive',
	);
// Amazon.co.uk: Toys
$cfg['Categories']['list']['Amazon.co.uk']['Toys']['name'] = "Toys";
$cfg['Categories']['list']['Amazon.co.uk']['Toys']['node'] = "595314";
$cfg['Categories']['list']['Amazon.co.uk']['Toys']['nodes']['595314'] = Array(
	'595316'  =>  'Action Figures',
	'470432'  =>  'Characters/Brands',
    '595314'  =>	'Toy Types',
	);
// Amazon.co.uk: VHS
$cfg['Categories']['list']['Amazon.co.uk']['VHS']['name'] = "VHS";
$cfg['Categories']['list']['Amazon.co.uk']['VHS']['node'] = "573400";
$cfg['Categories']['list']['Amazon.co.uk']['VHS']['nodes']['573400'] = Array(
	'283921'  =>  'Action',
	'283924'  =>  'Comedy',
	'283925'  =>  'Drama',
	);
// Amazon.co.uk: Video Games
$cfg['Categories']['list']['Amazon.co.uk']['VideoGames']['name'] = "PC & Video Games";
$cfg['Categories']['list']['Amazon.co.uk']['VideoGames']['node'] = "1025616";
$cfg['Categories']['list']['Amazon.co.uk']['VideoGames']['nodes']['1025616'] = Array(
	'579870'  =>  'PC Games',
	'304916011'=>  'Xbox 360',
	'579906'  =>  'PS2',
	);
// Amazon.co.uk: Watches
$cfg['Categories']['list']['Amazon.co.uk']['Watches']['name'] = "Watches";
$cfg['Categories']['list']['Amazon.co.uk']['Watches']['node'] = "328229011";
$cfg['Categories']['list']['Amazon.co.uk']['Watches']['nodes']['328229011'] = Array(
	'199482031'	=>	'Wristwatches',
	'199483031'	=>	'Luxury Watches',
	'199484031'	=>	'Pocket & Fob Watches',
	);

/////////////////////////
// Categories: Amazon.ca
// Amazon.ca: Baby
$cfg['Categories']['list']['Amazon.ca']['Baby']['name'] = "Baby";
$cfg['Categories']['list']['Amazon.ca']['Baby']['node'] = "3561347011";
$cfg['Categories']['list']['Amazon.ca']['Baby']['nodes']['3561347011'] = Array(
    '4624221011'    =>  'Activity & Entertainment',
    '4624226011'    =>  'Baby & Toddler Toys',
    '4624434011'    =>  'Safety Equipment',
    );
// Amazon.ca: Beauty
$cfg['Categories']['list']['Amazon.ca']['Beauty']['name'] = "Beauty";
$cfg['Categories']['list']['Amazon.ca']['Beauty']['node'] = "6205125011";
$cfg['Categories']['list']['Amazon.ca']['Beauty']['nodes']['6205125011'] = Array(
	'6344392011' 	=>	'Bath & Body',
	'6344512011' 	=>	'Hair Care',
	'6344635011' 	=>	'Makeup',
	);
// Amazon.ca: Books
$cfg['Categories']['list']['Amazon.ca']['Books']['name'] = "Books";
$cfg['Categories']['list']['Amazon.ca']['Books']['node'] = "927726";
$cfg['Categories']['list']['Amazon.ca']['Books']['nodes']['927726'] = Array(
	'935522' 	=>	'Business',
	'927728' 	=>	'History',
	'955190' 	=>	'Romance',
	);
// Amazon.ca: DVD
$cfg['Categories']['list']['Amazon.ca']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.ca']['DVD']['node'] = "952768";
$cfg['Categories']['list']['Amazon.ca']['DVD']['nodes']['952768'] = Array(
	'966110'  =>  'Action',
	'953088'  =>  'Comedy',
	'953102'  =>  'Drama',
	);
// Amazon.ca: Electronics
$cfg['Categories']['list']['Amazon.ca']['Electronics']['name'] = "Electronics";
$cfg['Categories']['list']['Amazon.ca']['Electronics']['node'] = "677211011";
$cfg['Categories']['list']['Amazon.ca']['Electronics']['nodes']['677211011'] = Array(
	'677239011' =>  'Computers & PDAs',
	'677230011' =>  'Camera, Photo & Video',
	'677219011' =>  'MP3 & Media Players',
	);
// Amazon.ca: HealthPersonalCare
$cfg['Categories']['list']['Amazon.ca']['HealthPersonalCare']['name'] = "Health & Personal Care";
$cfg['Categories']['list']['Amazon.ca']['HealthPersonalCare']['node'] = "6205178011";
$cfg['Categories']['list']['Amazon.ca']['HealthPersonalCare']['nodes']['6205178011'] = Array(
    '6369682011'    =>  'Baby & Child Care',
    '6369740011'    =>  'Nutrition & Wellness',
    '6371091011'    =>  'Personal Care',
    );
// Amazon.ca: KindleStore
$cfg['Categories']['list']['Amazon.ca']['KindleStore']['name'] = "Kindle";
$cfg['Categories']['list']['Amazon.ca']['KindleStore']['node'] = "2972706011";
$cfg['Categories']['list']['Amazon.ca']['KindleStore']['nodes']['2972706011'] = Array(
    '2980421011'    =>  'Kindle Devices',
    '2980422011'    =>  'Kindle Accessories',
    '2980423011'    =>  'Kindle eBooks',
    '5799480011'    =>  'Kindle Singles',
    );
// Amazon.ca: Kitchen
$cfg['Categories']['list']['Amazon.ca']['Kitchen']['name'] = "Kitchen";
$cfg['Categories']['list']['Amazon.ca']['Kitchen']['node'] = "2224068011";
$cfg['Categories']['list']['Amazon.ca']['Kitchen']['nodes']['2224068011'] = Array(
	'2224078011' =>  'Cookware & Baking',
	'2224122011' =>  'Small Appliances',
	'2224106011' =>  'Kitchen Utensils & Gadgets',
	);
// Amazon.ca: LawnAndGarden
$cfg['Categories']['list']['Amazon.ca']['LawnAndGarden']['name'] = "Lawn/Garden";
$cfg['Categories']['list']['Amazon.ca']['LawnAndGarden']['node'] = "6299024011";
$cfg['Categories']['list']['Amazon.ca']['LawnAndGarden']['nodes']['6299024011'] = Array(
    '2224149011'    =>  'Gardening',
    '2224158011'    =>  'Outdoor Décor',
    '2224160011'    =>  'Pest Control',
    );
// Amazon.ca: Music
$cfg['Categories']['list']['Amazon.ca']['Music']['name'] = "Music";
$cfg['Categories']['list']['Amazon.ca']['Music']['node'] = "962454";
$cfg['Categories']['list']['Amazon.ca']['Music']['nodes']['962454'] = Array(
	'962490'	=>  'Rock',
	'962486' 	=>  'Pop',
	'1034848' 	=>  'R&B',
	'962498' 	=>  'Soundtracks',
	);
// Amazon.ca: PC & Video Games
$cfg['Categories']['list']['Amazon.ca']['VideoGames']['name'] = "PC & Video Games";
$cfg['Categories']['list']['Amazon.ca']['VideoGames']['node'] = "3234221";
$cfg['Categories']['list']['Amazon.ca']['VideoGames']['nodes']['3234221'] = Array(
    '3322951' =>  'PlayStation2',
    '3321791' =>  'PC Games',
    '3323581' =>  'Xbox',
    );
// Amazon.ca: PetSupplies
$cfg['Categories']['list']['Amazon.ca']['PetSupplies']['name'] = "Pet Supplies";
$cfg['Categories']['list']['Amazon.ca']['PetSupplies']['node'] = "6291628011";
$cfg['Categories']['list']['Amazon.ca']['PetSupplies']['nodes']['6291628011'] = Array(
    '6291872011'    =>  'Cats',
    '6292194011'    =>  'Dogs',
    '6292556011'    =>  'Reptiles & Amphibians',
    );
// Amazon.ca: Software
$cfg['Categories']['list']['Amazon.ca']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.ca']['Software']['node'] = "3234171";
$cfg['Categories']['list']['Amazon.ca']['Software']['nodes']['3234171'] = Array(
	'3314471'  =>  'Business',
	'3316381'  =>  'Graphics',
	'3318701'  =>  'Video',
	);
// Amazon.ca: SportingGoods
$cfg['Categories']['list']['Amazon.ca']['SportingGoods']['name'] = "Sports & Outdoors";
$cfg['Categories']['list']['Amazon.ca']['SportingGoods']['node'] = "2242990011";
$cfg['Categories']['list']['Amazon.ca']['SportingGoods']['nodes']['2242990011'] = Array(
	'2406106011'  =>  'Cycling',
	'2406117011'  =>  'Golf',
	'2406136011'  =>  'Skiing',
	);
// Amazon.ca: VHS
$cfg['Categories']['list']['Amazon.ca']['VHS']['name'] = "VHS";
$cfg['Categories']['list']['Amazon.ca']['VHS']['node'] = "962072";
$cfg['Categories']['list']['Amazon.ca']['VHS']['nodes']['962072'] = Array(
	'972682'  =>  'Action',
	'962128'  =>  'Comedy',
	'962130'  =>  'Drama',
	);

/////////////////////////
// Categories: Amazon.de
// Amazon.de: Automotive
$cfg['Categories']['list']['Amazon.de']['Automotive']['name'] = "Auto & Motorrad";
$cfg['Categories']['list']['Amazon.de']['Automotive']['node'] = "79899031";
$cfg['Categories']['list']['Amazon.de']['Automotive']['nodes']['79899031'] = Array(
	'1071738'    =>  'Car-Hifi',
	'82943031'   =>  'Hersteller-Accessoires',
	'236861011'  =>  'Navigation',
	);
// Amazon.de: Baby
$cfg['Categories']['list']['Amazon.de']['Baby']['name'] = "Baby";
$cfg['Categories']['list']['Amazon.de']['Baby']['node'] = "357577011";
$cfg['Categories']['list']['Amazon.de']['Baby']['nodes']['357577011'] = Array(
	'357584011'  =>  'Kinderwagen & Radanhänger',
	'357580011'  =>  'Babymöbel',
	'357578011'  =>  'Autositze & Babyschalen',
	);
// Amazon.de: Books
$cfg['Categories']['list']['Amazon.de']['Books']['name'] = "Bücher";
$cfg['Categories']['list']['Amazon.de']['Books']['node'] = "541686";
$cfg['Categories']['list']['Amazon.de']['Books']['nodes']['541686'] = Array(
	'403434' 	=>	'Business',
	'122'		=>	'Kochen',
	'124' 		=>	'Computer',
	);
// Amazon.de: Apparel
$cfg['Categories']['list']['Amazon.de']['Apparel']['name'] = "Bekleidung";
$cfg['Categories']['list']['Amazon.de']['Apparel']['node'] = "78689031";
$cfg['Categories']['list']['Amazon.de']['Apparel']['nodes']['78689031'] = Array(
	'78691031'	=>  'Shirts',
	'78704031'	=>  'Jeanshosen',
	'78740031'	=>  'Jacken',
	);
// Amazon.de: Lighting
$cfg['Categories']['list']['Amazon.de']['Lighting']['name'] = "Beleuchtung";
$cfg['Categories']['list']['Amazon.de']['Lighting']['node'] = "213084031";
$cfg['Categories']['list']['Amazon.de']['Lighting']['nodes']['213084031'] = Array(
	'3780981'	=>  'Innenbeleuchtung',
	'227261031'	=>  'Leuchtmittel',
	'227262031'	=>  'Taschenlampen',
	);
// Amazon.de: OfficeProducts
$cfg['Categories']['list']['Amazon.de']['OfficeProducts']['name'] = "Bürobedarf";
$cfg['Categories']['list']['Amazon.de']['OfficeProducts']['node'] = "192417031";
$cfg['Categories']['list']['Amazon.de']['OfficeProducts']['nodes']['192417031'] = Array(
	'197751031'	=>  'Büromaterial',
	'197753031'	=>  'Papierprodukte',
	'197756031'	=>  'Schreibwaren',
	);
// Amazon.de: PC Hardware
$cfg['Categories']['list']['Amazon.de']['PCHardware']['name'] = "Computer";
$cfg['Categories']['list']['Amazon.de']['PCHardware']['node'] = "368179031";
$cfg['Categories']['list']['Amazon.de']['PCHardware']['nodes']['368179031'] = Array(
	'427954031'	=>  'Desktop-PCs',
	'427957031'	=>  'Notebooks',
	'429868031'	=>  'Monitore',
	);
// Amazon.de: DVD
$cfg['Categories']['list']['Amazon.de']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.de']['DVD']['node'] = "547664";
$cfg['Categories']['list']['Amazon.de']['DVD']['nodes']['547664'] = Array(
	'289093'  =>  'Action, Thriller & Horror',
	'290505'  =>  'Kinder & Familie',
	'290800'  =>  'Komödie & Drama',
	);
// Amazon.de: Electronics
$cfg['Categories']['list']['Amazon.de']['Electronics']['name'] = "Elektronik";
$cfg['Categories']['list']['Amazon.de']['Electronics']['node'] = "569604";
$cfg['Categories']['list']['Amazon.de']['Electronics']['nodes']['569604'] = Array(
	'700962'  =>  'Computer & Zubehör',
	'761254' 	=>  'Heimkino & Video',
	'571760'  =>  'Hifi & Audio',
	);
// Amazon.de: ForeignBooks
$cfg['Categories']['list']['Amazon.de']['ForeignBooks']['name'] = "English Books";
$cfg['Categories']['list']['Amazon.de']['ForeignBooks']['node'] = "54071011";
$cfg['Categories']['list']['Amazon.de']['ForeignBooks']['nodes']['54071011'] = Array(
	'58173011'  =>  'Business',
	'64085011'  =>  'Cooking',
	'62991011'  =>  'Computers',
	);
// Amazon.de: Video Games
$cfg['Categories']['list']['Amazon.de']['VideoGames']['name'] = "Games";
$cfg['Categories']['list']['Amazon.de']['VideoGames']['node'] = "541708";
$cfg['Categories']['list']['Amazon.de']['VideoGames']['nodes']['541708'] = Array(
	'301129' 	=>	'Computerspiele',
	'575708'  =>  'Xbox',
	'516838'  =>  'PlayStation 2',
	);
// Amazon.de: Health/Personal Care
$cfg['Categories']['list']['Amazon.de']['HealthPersonalCare']['name'] = "Gesundheit";
$cfg['Categories']['list']['Amazon.de']['HealthPersonalCare']['node'] = "64263031";
$cfg['Categories']['list']['Amazon.de']['HealthPersonalCare']['nodes']['64263031'] = Array(
	'64265031' =>  'Badausstattung',
	'64271031' =>  'Kosmetik',
	'64276031' =>  'Personenwaagen',
	);
// Amazon.de: Home/Garden
$cfg['Categories']['list']['Amazon.de']['HomeGarden']['name'] = "Haus/Garten";
$cfg['Categories']['list']['Amazon.de']['HomeGarden']['node'] = "10925241";
$cfg['Categories']['list']['Amazon.de']['HomeGarden']['nodes']['10925241'] = Array(
	'11047531' =>  'Gartendeko',
	'11048731' =>  'Grillen',
	'11048331' =>  'Rund um Tiere',
	);
// Amazon.de: Tools
$cfg['Categories']['list']['Amazon.de']['Tools']['name'] = "Heimwerken";
$cfg['Categories']['list']['Amazon.de']['Tools']['node'] = "10925051";
$cfg['Categories']['list']['Amazon.de']['Tools']['nodes']['10925051'] = Array(
	'10961041' =>  'Eisenwaren',
	'10961081' =>  'Elektrowerkzeug',
	'10961661' =>  'Handwerkzeug',
	);
// Amazon.de: Photo & Camera
$cfg['Categories']['list']['Amazon.de']['Photo']['name'] = "Kamera/Foto";
$cfg['Categories']['list']['Amazon.de']['Photo']['node'] = "571860";
$cfg['Categories']['list']['Amazon.de']['Photo']['nodes']['571860'] = Array(
	'273647011'	=>  'Camcorder',
	'901968'	=>  'Diaprojektoren',
	'1115610'	=>  'Ferngläser',
	);
// Amazon.de: KindleStore
$cfg['Categories']['list']['Amazon.de']['KindleStore']['name'] = "Kindle eBooks";
$cfg['Categories']['list']['Amazon.de']['KindleStore']['node'] = "530485031";
$cfg['Categories']['list']['Amazon.de']['KindleStore']['nodes']['530485031'] = Array(
	'530886031'  =>  'Kindle eBooks',
	'530887031'  =>  'Kindle Zeitschriften',
	'530888031'  =>  'Kindle Zeitungen',
	);
// Amazon.de: Kitchen
$cfg['Categories']['list']['Amazon.de']['Kitchen']['name'] = "Küche";
$cfg['Categories']['list']['Amazon.de']['Kitchen']['node'] = "3169011";
$cfg['Categories']['list']['Amazon.de']['Kitchen']['nodes']['3169011'] = Array(
	'3169321'  =>  'Elektrische Küchengeräte',
	'3169211'  =>  'Haushaltsgeräte',
	'3312261'  =>  'Wohnen & Lifestyle',
	);
// Amazon.de: Grocery
$cfg['Categories']['list']['Amazon.de']['Grocery']['name'] = "Lebensmittel & Getränke";
$cfg['Categories']['list']['Amazon.de']['Grocery']['node'] = "340847031";
$cfg['Categories']['list']['Amazon.de']['Grocery']['nodes']['340847031'] = Array(
	'358557031'  =>  'Brot & Backwaren',
	'358560031'  =>  'Fleisch, Geflügel & Wurstwaren',
	'358567031'  =>  'Kaffee, Tee & Kakao',
	'358568031'  =>  'Knabberartikel',
	);
// Amazon.de: MP3Downloads
$cfg['Categories']['list']['Amazon.de']['MP3Downloads']['name'] = "MP3-Downloads";
$cfg['Categories']['list']['Amazon.de']['MP3Downloads']['node'] = "180529031";
$cfg['Categories']['list']['Amazon.de']['MP3Downloads']['nodes']['180529031'] = Array(
	'180680031'	 =>  'Pop',
	'180696031'	 =>	 'Rock',
	'180690031'	 =>  'R&B & Soul',
	);
// Amazon.de: Music
$cfg['Categories']['list']['Amazon.de']['Music']['name'] = "Musik";
$cfg['Categories']['list']['Amazon.de']['Music']['node'] = "542676";
$cfg['Categories']['list']['Amazon.de']['Music']['nodes']['542676'] = Array(
	'264875' 	 =>  'Pop',
	'264886'	 =>	 'Rock',
	'255895' 	 =>  'R&B',
	'264918' 	 =>  'Soundtracks',
	);
// Amazon.de: MusicalInstruments
$cfg['Categories']['list']['Amazon.de']['MusicalInstruments']['name'] = "Musikinstrumente";
$cfg['Categories']['list']['Amazon.de']['MusicalInstruments']['node'] = "340850031";
$cfg['Categories']['list']['Amazon.de']['MusicalInstruments']['nodes']['340850031'] = Array(
	'412356031'  =>  'DJ- & VJ-Equipment',
	'412420031'	 =>	 'Gitarren & Equipment',
	'406577031'  =>  'Piano & Keyboard',
	);
// Amazon.de: Outdoor Living
$cfg['Categories']['list']['Amazon.de']['OutdoorLiving']['name'] = "Outdoor/Freizeit";
$cfg['Categories']['list']['Amazon.de']['OutdoorLiving']['node'] = "11048231";
$cfg['Categories']['list']['Amazon.de']['OutdoorLiving']['nodes']['11048231'] = Array(
	'11048301'  =>  'Camping-Ausrüstung',
	'369346011' =>  'Feuerzeuge',
	'11048261'  =>  'Taschen',
	);
// Amazon.de: Beauty
$cfg['Categories']['list']['Amazon.de']['Beauty']['name'] = "Parfüm/Kosmetik";
$cfg['Categories']['list']['Amazon.de']['Beauty']['node'] = "84231031";
$cfg['Categories']['list']['Amazon.de']['Beauty']['nodes']['84231031'] = Array(
	'122877031'  =>  'Düfte',
	'122880031'  =>  'Make-up',
	'122878031'  =>  'Hautpflege',
	);
// Amazon.de: Shoes
$cfg['Categories']['list']['Amazon.de']['Shoes']['name'] = "Schuhe";
$cfg['Categories']['list']['Amazon.de']['Shoes']['node'] = "355006011";
$cfg['Categories']['list']['Amazon.de']['Shoes']['nodes']['355006011'] = Array(
	'361190011'  =>  'Babyschuhe',
	'361182011'  =>  'Sneaker',
	'361179011'  =>  'Pumps',
	);
// Amazon.de: Jewelry
$cfg['Categories']['list']['Amazon.de']['Jewelry']['name'] = "Schmuck";
$cfg['Categories']['list']['Amazon.de']['Jewelry']['node'] = "327473011";
$cfg['Categories']['list']['Amazon.de']['Jewelry']['nodes']['327473011'] = Array(
	'342098011' =>  'Armbänder & Armreifen',
	'161700031' =>  'Charms',
	'342093011' =>  'Ringe',
	);
// Amazon.de: Software
$cfg['Categories']['list']['Amazon.de']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.de']['Software']['node'] = "542064";
$cfg['Categories']['list']['Amazon.de']['Software']['nodes']['542064'] = Array(
	'12759211' =>  'Antivirensoftware',
	'12761221' =>  'Digitalfotografie',
	'408290'   =>  'Kinder & Familie',
	);
// Amazon.de: Sporting Goods
$cfg['Categories']['list']['Amazon.de']['SportingGoods']['name'] = "Sport/Freizeit";
$cfg['Categories']['list']['Amazon.de']['SportingGoods']['node'] = "16435121";
$cfg['Categories']['list']['Amazon.de']['SportingGoods']['nodes']['16435121'] = Array(
	'16435181' =>  'Fußball',
	'16435171' =>  'Fitness',
	'391713011' =>  'Wintersport',
	);
// Amazon.de: Toys
$cfg['Categories']['list']['Amazon.de']['Toys']['name'] = "Spielwaren";
$cfg['Categories']['list']['Amazon.de']['Toys']['node'] = "12950661";
$cfg['Categories']['list']['Amazon.de']['Toys']['nodes']['12950661'] = Array(
	'12956501' =>  'Spiele',
	'13039241' =>  'Puzzles',
    '13040381' =>	 'Nach Alter',
	);
// Amazon.de: Watches
$cfg['Categories']['list']['Amazon.de']['Watches']['name'] = "Uhren";
$cfg['Categories']['list']['Amazon.de']['Watches']['node'] = "193708031";
$cfg['Categories']['list']['Amazon.de']['Watches']['nodes']['193708031'] = Array(
	'198795031'	=>  'Armbanduhren',
	'198796031'	=>  'Luxusuhren',
	'198797031'	=>  'Taschenuhren',
	);
// Amazon.de: VHS
$cfg['Categories']['list']['Amazon.de']['VHS']['name'] = "Video";
$cfg['Categories']['list']['Amazon.de']['VHS']['node'] = "547082";
$cfg['Categories']['list']['Amazon.de']['VHS']['nodes']['547082'] = Array(
	'284262'  =>  'Action, Thriller & Science Fiction',
	'284264'  =>  'Kinder & Familie',
	'284263'  =>  'Komödie & Drama',
	);
// Amazon.de: Magazines
$cfg['Categories']['list']['Amazon.de']['Magazines']['name'] = "Zeitschriften";
$cfg['Categories']['list']['Amazon.de']['Magazines']['node'] = "1161660";
$cfg['Categories']['list']['Amazon.de']['Magazines']['nodes']['1161660'] = Array(
	'1198626'  =>  'Sport',
	'1198616'  =>  'Computer',
	'1198620'  =>  'Familie',
	);

/////////////////////////
// Categories: Amazon.fr
// Amazon.fr: HomeImprovement
$cfg['Categories']['list']['Amazon.fr']['HomeImprovement']['name'] = "Amélioration de l'habitat";
$cfg['Categories']['list']['Amazon.fr']['HomeImprovement']['node'] = "590748031";
$cfg['Categories']['list']['Amazon.fr']['HomeImprovement']['nodes']['590748031'] = Array(
    '1716190031' =>  'Construction',
    '1716189031' =>  'Outillage à main et électroportatif',
    '1787885031' =>  'Sécurité',
    );
// Amazon.fr: Beauty
$cfg['Categories']['list']['Amazon.fr']['Beauty']['name'] = "Beauté & Parfum";
$cfg['Categories']['list']['Amazon.fr']['Beauty']['node'] = "197859031";
$cfg['Categories']['list']['Amazon.fr']['Beauty']['nodes']['197859031'] = Array(
	'210965031' =>	'Parfums',
	'210972031'	=>	'Maquillage',
	'211005031' =>	'Manucure & Pédicure',
	);
// Amazon.fr: Baby
$cfg['Categories']['list']['Amazon.fr']['Baby']['name'] = "Bébés et Puériculture";
$cfg['Categories']['list']['Amazon.fr']['Baby']['node'] = "206618031";
$cfg['Categories']['list']['Amazon.fr']['Baby']['nodes']['206618031'] = Array(
	'214759031' =>	'Vêtements',
	'214797031'	=>	'Eveil',
	'214866031' =>	'Poussettes & Remorques Vélo',
	);
// Amazon.fr: Jewelry
$cfg['Categories']['list']['Amazon.fr']['Jewelry']['name'] = "Bijoux";
$cfg['Categories']['list']['Amazon.fr']['Jewelry']['node'] = "193711031";
$cfg['Categories']['list']['Amazon.fr']['Jewelry']['nodes']['193711031'] = Array(
	'197025031'	=>  'Bijoux de corps',
	'197028031'	=>  'Bracelets',
	'197031031'	=>  'Pendentifs',
	);
// Amazon.fr: Shoes
$cfg['Categories']['list']['Amazon.fr']['Shoes']['name'] = "Chaussures";
$cfg['Categories']['list']['Amazon.fr']['Shoes']['node'] = "248815031";
$cfg['Categories']['list']['Amazon.fr']['Shoes']['nodes']['248815031'] = Array(
	'302497031'	=>	'Femmes',
	'302498031'	=>	'Hommes',
	'302499031'	=>	'Enfants',
	);
// Amazon.fr: Kitchen
$cfg['Categories']['list']['Amazon.fr']['Kitchen']['name'] = "Cuisine & Maison";
$cfg['Categories']['list']['Amazon.fr']['Kitchen']['node'] = "57686031";
$cfg['Categories']['list']['Amazon.fr']['Kitchen']['nodes']['57686031'] = Array(
	'57687031'  =>  'Cuisine',
	'57688031'  =>  'Soin du corps',
	'57690031'  =>  'Jardin',
	);
// Amazon.fr: DVD
$cfg['Categories']['list']['Amazon.fr']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.fr']['DVD']['node'] = "235554011";
$cfg['Categories']['list']['Amazon.fr']['DVD']['nodes']['235554011'] = Array(
	'409394' 	=>	'Action & Aventure',
	'409432'	=>	'Comedie',
	'238397011' 	=>	'Romance',
	);
// Amazon.fr: Lighting
$cfg['Categories']['list']['Amazon.fr']['Lighting']['name'] = "Luminaires et Eclairage";
$cfg['Categories']['list']['Amazon.fr']['Lighting']['node'] = "213081031";
$cfg['Categories']['list']['Amazon.fr']['Lighting']['nodes']['213081031'] = Array(
	'227255031'	=>	'Luminaires Extérieur',
	'68994031' 	=>	'Luminaires Intérieur',
	'227257031'	=>	'Ampoules',
	);
// Amazon.fr: ForeignBooks
$cfg['Categories']['list']['Amazon.fr']['ForeignBooks']['name'] = "English Books";
$cfg['Categories']['list']['Amazon.fr']['ForeignBooks']['node'] = "69633011";
$cfg['Categories']['list']['Amazon.fr']['ForeignBooks']['nodes']['69633011'] = Array(
	'73735011' 	=>	'Business & Investing',
	'80710011'	=>	'History',
	'84598011' 	=>	'Nonfiction',
	);
// Amazon.fr: OfficeProducts
$cfg['Categories']['list']['Amazon.fr']['OfficeProducts']['name'] = "Fournitures de Bureau";
$cfg['Categories']['list']['Amazon.fr']['OfficeProducts']['node'] = "192420031";
$cfg['Categories']['list']['Amazon.fr']['OfficeProducts']['nodes']['192420031'] = Array(
	'197762031'	=>  'Accessoires électroniques',
	'197759031'	=>	'Petites fournitures',
	'197761031'	=>  'Papeterie',
	);
// Amazon.fr: PCHardware
$cfg['Categories']['list']['Amazon.fr']['PCHardware']['name'] = "Informatique";
$cfg['Categories']['list']['Amazon.fr']['PCHardware']['node'] = "340859031";
$cfg['Categories']['list']['Amazon.fr']['PCHardware']['nodes']['340859031'] = Array(
    '340859031' =>  'Produits',
    '427942031' =>  'Accessoires',
    '429876031' =>  'Claviers, Souris et Tablettes',
    );
// Amazon.fr: MusicalInstruments
$cfg['Categories']['list']['Amazon.fr']['MusicalInstruments']['name'] = "Instruments de musique";
$cfg['Categories']['list']['Amazon.fr']['MusicalInstruments']['node'] = "340862031";
$cfg['Categories']['list']['Amazon.fr']['MusicalInstruments']['nodes']['340862031'] = Array(
	'406580031'	=>  'Guitares et Equipements',
	'406581031'	=>  'Basses et Equipement',
	'406582031'	=>  'Pianos et Claviers',
	);
// Amazon.fr: Toys
$cfg['Categories']['list']['Amazon.fr']['Toys']['name'] = "Jeux et Jouets";
$cfg['Categories']['list']['Amazon.fr']['Toys']['node'] = "322088011";
$cfg['Categories']['list']['Amazon.fr']['Toys']['nodes']['322088011'] = Array(
	'328515011'	=>  'Jeux électroniques',
	'328701011'	=>	'Radio commandes',
	'328233011'	=> 'Figurines',
	);
// Amazon.fr: Video Games
$cfg['Categories']['list']['Amazon.fr']['VideoGames']['name'] = "Jeux Vidéo";
$cfg['Categories']['list']['Amazon.fr']['VideoGames']['node'] = "235571011";
$cfg['Categories']['list']['Amazon.fr']['VideoGames']['nodes']['235571011'] = Array(
	'893192' 	=>	'GameCube',
	'548018'	=>	'PlayStation 2',
	'15860651' 	=>	'Xbox 360',
			  );
// Amazon.fr: Electronics
$cfg['Categories']['list']['Amazon.fr']['Electronics']['name'] = "L'électronique";
$cfg['Categories']['list']['Amazon.fr']['Electronics']['node'] = "235560011";
$cfg['Categories']['list']['Amazon.fr']['Electronics']['nodes']['235560011'] = Array(
	'13910741' 	=>	'Consommables & Accessoires',
	'13910691'	=>	'Photo & Video',
	'13910711' 	=>	'Telephonie',
	);
// Amazon.fr: Books
$cfg['Categories']['list']['Amazon.fr']['Books']['name'] = "Livres";
$cfg['Categories']['list']['Amazon.fr']['Books']['node'] = "235564011";
$cfg['Categories']['list']['Amazon.fr']['Books']['nodes']['235564011'] = Array(
	'301144' 	=>	'Art, Musique et Cinema',
	'302050'	=>	'Cuisine et Vins',
	'301132' 	=>	'Litterature',
	);
// Amazon.fr: Software
$cfg['Categories']['list']['Amazon.fr']['Software']['name'] = "Logiciels";
$cfg['Categories']['list']['Amazon.fr']['Software']['node'] = "235570011";
$cfg['Categories']['list']['Amazon.fr']['Software']['nodes']['235570011'] = Array(
	'3795781' =>	'Antivirus & Securite',
	'547982'  =>	'Bureautique & Utilitaires',
	'547984'  =>	'Graphisme & Multimedia',
	);
// Amazon.fr: Watches
$cfg['Categories']['list']['Amazon.fr']['Watches']['name'] = "Montres";
$cfg['Categories']['list']['Amazon.fr']['Watches']['node'] = "60937031";
$cfg['Categories']['list']['Amazon.fr']['Watches']['nodes']['60937031'] = Array(
	'62619031'	=>  'Montres bracelet',
	'199638031'	=>  'Montres de Luxe',
	'62620031'	=>  'Accessoires',
	);
// Amazon.fr: Music
$cfg['Categories']['list']['Amazon.fr']['Music']['name'] = "Musique";
$cfg['Categories']['list']['Amazon.fr']['Music']['node'] = "235565011";
$cfg['Categories']['list']['Amazon.fr']['Music']['nodes']['235565011'] = Array(
	'301174' 	=>	'Classique',
	'301166'	=>	'Pop Rock',
	'301168' 	=>	'Techno',
	);
// Amazon.fr: HealthPersonalCare
$cfg['Categories']['list']['Amazon.fr']['HealthPersonalCare']['name'] = "Santé & Corps";
$cfg['Categories']['list']['Amazon.fr']['HealthPersonalCare']['node'] = "197862031";
$cfg['Categories']['list']['Amazon.fr']['HealthPersonalCare']['nodes']['197862031'] = Array(
	'212036031' =>	'Nutrition & Diététique',
	'211040031'	=>	'Hygiène, Soins & Bien-être',
	'212189031' =>	'Premiers soins & Equipements médicaux',
	);
// Amazon.fr: SportingGoods
$cfg['Categories']['list']['Amazon.fr']['SportingGoods']['name'] = "Sports et Loisirs";
$cfg['Categories']['list']['Amazon.fr']['SportingGoods']['node'] = "325615031";
$cfg['Categories']['list']['Amazon.fr']['SportingGoods']['nodes']['325615031'] = Array(
	'339892031' =>	'Football',
	'340091031'	=>	'Vêtements de sport',
	'339983031' =>	'Multisport',
	);
// Amazon.fr: MP3Downloads
$cfg['Categories']['list']['Amazon.fr']['MP3Downloads']['name'] = "Téléchargements MP3";
$cfg['Categories']['list']['Amazon.fr']['MP3Downloads']['node'] = "206442031";
$cfg['Categories']['list']['Amazon.fr']['MP3Downloads']['nodes']['206442031'] = Array(
	'211521031' =>	'Electro',
	'211594031'	=>	'Pop',
	'211610031' =>	'Rock',
	);
// Amazon.fr: Apparel
$cfg['Categories']['list']['Amazon.fr']['Apparel']['name'] = "Vêtements";
$cfg['Categories']['list']['Amazon.fr']['Apparel']['node'] = "340856031";
$cfg['Categories']['list']['Amazon.fr']['Apparel']['nodes']['340856031'] = Array(
	'436559031' =>	'Femme',
	'436560031'	=>	'Homme',
	'436563031'	=>	'Bébé',
	);
// Amazon.fr: VHS
$cfg['Categories']['list']['Amazon.fr']['VHS']['name'] = "Vidéo";
$cfg['Categories']['list']['Amazon.fr']['VHS']['node'] = "235555011";
$cfg['Categories']['list']['Amazon.fr']['VHS']['nodes']['235555011'] = Array(
	'301177' 	=>	'Action & Aventure',
	'301180'	=>	'Comedie',
	'301179' 	=>	'Drame',
	);

///////////////////////////
// Categories: Amazon.co.jp
// Amazon.co.jp: Apparel
$cfg['Categories']['list']['Amazon.co.jp']['Apparel']['name'] = "&#34915;&#26381;";
$cfg['Categories']['list']['Amazon.co.jp']['Apparel']['node'] = "361298011";
$cfg['Categories']['list']['Amazon.co.jp']['Apparel']['nodes']['361298011'] = Array(
	'2133302051'=>	'&#12513;&#12531;&#12474;',
	'2133303051'=>	'&#12524;&#12487;&#12451;&#12540;&#12473;',
	'345991011' =>	'&#12505;&#12499;&#12540;&#26381;',
	);
// Amazon.co.jp: Appliances
$cfg['Categories']['list']['Amazon.co.jp']['Appliances']['name'] = "&#12450;&#12503;&#12521;&#12452;&#12450;&#12531;&#12473;";
$cfg['Categories']['list']['Amazon.co.jp']['Appliances']['node'] = "2277725051";
$cfg['Categories']['list']['Amazon.co.jp']['Appliances']['nodes']['2277725051'] = Array(
	'16299621'=>	'&#20919;&#34101;&#24235;&#12539;&#20919;&#20941;&#24235;',
	'16298571'=>	'&#12456;&#12450;&#12467;&#12531;',
	'2353812051'=>	'&#12499;&#12523;&#12488;&#12452;&#12531;&#12461;&#12483;&#12481;&#12531;&#23478;&#38651;'
	);
// Amazon.co.jp: Beauty
$cfg['Categories']['list']['Amazon.co.jp']['Beauty']['name'] = "&#32654;&#12375;&#12373;";
$cfg['Categories']['list']['Amazon.co.jp']['Beauty']['node'] = "202770011";
$cfg['Categories']['list']['Amazon.co.jp']['Beauty']['nodes']['202770011'] = Array(
	'170039011' =>	'&#12501;&#12455;&#12452;&#12473;&#65286;&#12508;&#12487;&#12451;&#12465;&#12450;',
	'169642011'	=>	'&#12496;&#12473;&#12539;&#12465;&#12450;&#29992;&#21697;',
	'169911011' =>	'&#32654;&#12375;&#12373;',
	);
// Amazon.co.jp: Books
$cfg['Categories']['list']['Amazon.co.jp']['Books']['name'] = "&#26360;&#31821;";
$cfg['Categories']['list']['Amazon.co.jp']['Books']['node'] = "202188011";
$cfg['Categories']['list']['Amazon.co.jp']['Books']['nodes']['202188011'] = Array(
	'466284' 	=>	'&#25991;&#23398;&#12539;&#35413;&#35542;',
	'3148931'	=>	'&#28459;&#30011;&#12539;&#12450;&#12491;&#12513;&#12539;BL',
	'466280' 	=>	'&#25945;&#32946;&#12539;&#23398;&#21442;&#12539;&#21463;&#39443;',
	);
// Amazon.co.jp: DVD
$cfg['Categories']['list']['Amazon.co.jp']['DVD']['name'] = "DVD&#12522;&#12540;";
$cfg['Categories']['list']['Amazon.co.jp']['DVD']['node'] = "202766011";
$cfg['Categories']['list']['Amazon.co.jp']['DVD']['nodes']['202766011'] = Array(
	'562016' 	=>	'&#22806;&#22269;&#26144;&#30011;',
	'562014'	=>	'&#26085;&#26412;&#26144;&#30011;',
	'562020' 	=>	'&#12450;&#12491;&#12513;',
	);
// Amazon.co.jp: Electronics
$cfg['Categories']['list']['Amazon.co.jp']['Electronics']['name'] = "&#12456;&#12524;&#12463;&#12488;&#12525;&#12491;&#12463;&#12473;";
$cfg['Categories']['list']['Amazon.co.jp']['Electronics']['node'] = "202762011";
$cfg['Categories']['list']['Amazon.co.jp']['Electronics']['nodes']['202762011'] = Array(
	'16462091' 	=>	'&#12459;&#12513;&#12521;&#12539;&#12487;&#12472;&#12479;&#12523;&#12459;&#12513;&#12521;',
	'3371401'	=>	'&#38651;&#23376;&#36766;&#26360;&#12539;PDA',
	'16462081' 	=>	'&#12458;&#12540;&#12487;&#12451;&#12458;',
	);
// Amazon.co.jp: Grocery
$cfg['Categories']['list']['Amazon.co.jp']['Grocery']['name'] = "&#39135;&#26009;&#21697;";
$cfg['Categories']['list']['Amazon.co.jp']['Grocery']['node'] = "57240051";
$cfg['Categories']['list']['Amazon.co.jp']['Grocery']['nodes']['57240051'] = Array(
	'70903051' 	=>	'&#39135;&#21697;',
	'71442051'	=>	'&#12489;&#12522;&#12531;&#12463;',
	'71588051'	=>	'&#12362;&#37202;',
	'71700051' 	=>	'&#39135;&#21697;&#12462;&#12501;&#12488;&#12473;&#12488;&#12450;',
	);
// Amazon.co.jp: HealthPersonalCare
$cfg['Categories']['list']['Amazon.co.jp']['HealthPersonalCare']['name'] = "&#20581;&#24247;";
$cfg['Categories']['list']['Amazon.co.jp']['HealthPersonalCare']['node'] = "202770011";
$cfg['Categories']['list']['Amazon.co.jp']['HealthPersonalCare']['nodes']['202770011'] = Array(
	'170039011' =>	'&#12501;&#12455;&#12452;&#12473;&#65286;&#12508;&#12487;&#12451;&#12465;&#12450;',
	'169976011'	=>	'&#12480;&#12452;&#12456;&#12483;&#12488;',
	'169911011' =>	'&#12504;&#12523;&#12473;&#12465;&#12450;',
	);
// Amazon.co.jp: HomeImprovement
$cfg['Categories']['list']['Amazon.co.jp']['HomeImprovement']['name'] = "&#12507;&#12540;&#12512;&#25913;&#21892;";
$cfg['Categories']['list']['Amazon.co.jp']['HomeImprovement']['node'] = "202761011";
$cfg['Categories']['list']['Amazon.co.jp']['HomeImprovement']['nodes']['202761011'] = Array(
	'13938481' 	=>	'&#12461;&#12483;&#12481;&#12531;&#65286;&#12486;&#12540;&#12502;&#12523;&#12454;&#12455;&#12450;',
	'13938531'	=>	'&#12452;&#12531;&#12486;&#12522;&#12450;&#12539;&#21454;&#32013;&#12539;&#23517;&#20855;',
	'13945231' 	=>	'&#12460;&#12540;&#12487;&#12531;',
	);
// Amazon.co.jp: Jewelry
$cfg['Categories']['list']['Amazon.co.jp']['Jewelry']['name'] = "&#29577;&#39166;&#12426;";
$cfg['Categories']['list']['Amazon.co.jp']['Jewelry']['node'] = "86422051";
$cfg['Categories']['list']['Amazon.co.jp']['Jewelry']['nodes']['86422051'] = Array(
	'86228051' 	=>	'&#12493;&#12483;&#12463;&#12524;&#12473;&#65286;&#12506;&#12531;&#12480;&#12531;&#12488;',
	'86233051'	=>	'&#12522;&#12531;&#12464;',
	'86246051' 	=>	'&#12513;&#12531;&#12474;&#12450;&#12463;&#12475;&#12469;&#12522;&#12540;',
	);
// Amazon.co.jp: Kitchen
$cfg['Categories']['list']['Amazon.co.jp']['Kitchen']['name'] = "&#21488;&#25152;";
$cfg['Categories']['list']['Amazon.co.jp']['Kitchen']['node'] = "202761011";
$cfg['Categories']['list']['Amazon.co.jp']['Kitchen']['nodes']['202761011'] = Array(
	'13938481' 	=>	'&#12461;&#12483;&#12481;&#12531;&#65286;&#12486;&#12540;&#12502;&#12523;&#12454;&#12455;&#12450;',
	'13938531'	=>	'&#12452;&#12531;&#12486;&#12522;&#12450;&#12539;&#21454;&#32013;&#12539;&#23517;&#20855;',
	'124048011' =>	'&#29983;&#27963;&#23478;&#38651;',
	);
// Amazon.co.jp: MP3Downloads
$cfg['Categories']['list']['Amazon.co.jp']['MP3Downloads']['name'] = "MP3&#12480;&#12454;&#12531;&#12525;&#12540;&#12489;";
$cfg['Categories']['list']['Amazon.co.jp']['MP3Downloads']['node'] = "2129334051";
$cfg['Categories']['list']['Amazon.co.jp']['MP3Downloads']['nodes']['2129334051'] = Array(
	'2129337051' 	=>	'&#12450;&#12523;&#12496;&#12512;',
	'2129338051' 	=>	'&#26354;',
	);
// Amazon.co.jp: Music
$cfg['Categories']['list']['Amazon.co.jp']['Music']['name'] = "&#38899;&#27005;";
$cfg['Categories']['list']['Amazon.co.jp']['Music']['node'] = "202765011";
$cfg['Categories']['list']['Amazon.co.jp']['Music']['nodes']['202765011'] = Array(
	'569170' 	=>	'J-POP',
	'569290'	=>	'&#12509;&#12483;&#12503;&#12473;',
	'569318' 	=>	'&#12477;&#12454;&#12523;&#12539;R&B',
	'569292' 	=>	'&#12525;&#12483;&#12463;',
	);
// Amazon.co.jp: OfficeProducts
$cfg['Categories']['list']['Amazon.co.jp']['OfficeProducts']['name'] = "Office&#35069;&#21697;";
$cfg['Categories']['list']['Amazon.co.jp']['OfficeProducts']['node'] = "89680051";
$cfg['Categories']['list']['Amazon.co.jp']['OfficeProducts']['nodes']['89680051'] = Array(
	'89085051' 	=>	'&#12494;&#12540;&#12488;&#12539;&#32025;&#35069;&#21697;',
	'89083051'	=>	'&#20107;&#21209;&#29992;&#21697;',
	'89088051' 	=>	'&#31558;&#35352;&#20855;',
	);
// Amazon.co.jp: Shoes
$cfg['Categories']['list']['Amazon.co.jp']['Shoes']['name'] = "&#38772;";
$cfg['Categories']['list']['Amazon.co.jp']['Shoes']['node'] = "2016927051";
$cfg['Categories']['list']['Amazon.co.jp']['Shoes']['nodes']['2016927051'] = Array(
	'2221079051'=>	'&#12513;&#12531;&#12474;',
	'2221080051'=>	'&#12524;&#12487;&#12451;&#12540;&#12473;',
	'2226307051'=>	'&#12461;&#12483;&#12474;&#12539;&#12505;&#12499;&#12540;',
	);
// Amazon.co.jp: Software
$cfg['Categories']['list']['Amazon.co.jp']['Software']['name'] = "&#12477;&#12501;&#12488;";
$cfg['Categories']['list']['Amazon.co.jp']['Software']['node'] = "202764011";
$cfg['Categories']['list']['Amazon.co.jp']['Software']['nodes']['202764011'] = Array(
	'1040116' 	=>	'&#12454;&#12452;&#12523;&#12473;&#23550;&#31574;&#12539;&#12475;&#12461;&#12517;&#12522;&#12486;&#12451;',
	'637644'	=>	'&#12499;&#12472;&#12493;&#12473;&#12539;&#12458;&#12501;&#12451;&#12473;',
	'637652' 	=>	'&#12487;&#12470;&#12452;&#12531;&#12539;&#12464;&#12521;&#12501;&#12451;&#12483;&#12463;',
	);
// Amazon.co.jp: SportingGoods
$cfg['Categories']['list']['Amazon.co.jp']['SportingGoods']['name'] = "&#12473;&#12509;&#12540;&#12484;&#29992;&#21697;";
$cfg['Categories']['list']['Amazon.co.jp']['SportingGoods']['node'] = "202769011";
$cfg['Categories']['list']['Amazon.co.jp']['SportingGoods']['nodes']['202769011'] = Array(
	'14315501' 	=>	'&#12501;&#12451;&#12483;&#12488;&#12493;&#12473;&#12539;&#12488;&#12524;&#12540;&#12491;&#12531;&#12464;',
	'14315411'	=>	'&#12450;&#12454;&#12488;&#12489;&#12450;',
	'14315441' 	=>	'&#12468;&#12523;&#12501;',
	);
// Amazon.co.jp: Toys
$cfg['Categories']['list']['Amazon.co.jp']['Toys']['name'] = "&#29609;&#20855;";
$cfg['Categories']['list']['Amazon.co.jp']['Toys']['node'] = "202768011";
$cfg['Categories']['list']['Amazon.co.jp']['Toys']['nodes']['202768011'] = Array(
	'13321861' 	=>	'&#12507;&#12499;&#12540;',
	'13321751'	=>	'&#12501;&#12449;&#12483;&#12471;&#12519;&#12531;&#12539;&#12450;&#12463;&#12475;&#12469;&#12522;',
	'13321741' 	=>	'&#12396;&#12356;&#12368;&#12427;&#12415;',
	);
// Amazon.co.jp: VHS
$cfg['Categories']['list']['Amazon.co.jp']['VHS']['name'] = "VHS&#29256;";
$cfg['Categories']['list']['Amazon.co.jp']['VHS']['node'] = "202767011";
$cfg['Categories']['list']['Amazon.co.jp']['VHS']['nodes']['202767011'] = Array(
	'564546' 	=>	'Import (&#36664;&#20837;&#29256;)',
	'561990'	=>	'&#12450;&#12491;&#12513;',
	'2112001051'=>	'&#12524;&#12540;&#12505;&#12523;&#21029;',
	);
// Amazon.co.jp: VideoGames
$cfg['Categories']['list']['Amazon.co.jp']['VideoGames']['name'] = "&#12499;&#12487;&#12458;&#12466;&#12540;&#12512;";
$cfg['Categories']['list']['Amazon.co.jp']['VideoGames']['node'] = "202763011";
$cfg['Categories']['list']['Amazon.co.jp']['VideoGames']['nodes']['202763011'] = Array(
	'15782591' 	=>	'&#12503;&#12524;&#12452;&#12473;&#12486;&#12540;&#12471;&#12519;&#12531;3',
	'15783231' 	=>	'Xbox 360',
	'193662011'	=>	'Wii',
	'1192242' 	=>	'PC&#12466;&#12540;&#12512;',
	);
// Amazon.co.jp: Watches
$cfg['Categories']['list']['Amazon.co.jp']['Watches']['name'] = "&#26178;&#35336;";
$cfg['Categories']['list']['Amazon.co.jp']['Watches']['node'] = "333336011";
$cfg['Categories']['list']['Amazon.co.jp']['Watches']['nodes']['333336011'] = Array(
	'333010011' =>	'&#12524;&#12487;&#12451;&#12540;&#12473; &#33109;&#26178;&#35336;',
	'333009011'	=>	'&#12513;&#12531;&#12474; &#33109;&#26178;&#35336;',
	'2016222051'=>	'&#12461;&#12483;&#12474;&#33109;&#26178;&#35336;',
	);

/////////////////////////
// Categories: Amazon.it

// Amazon.it: Kitchen
$cfg['Categories']['list']['Amazon.it']['Kitchen']['name'] = "Casa e cucina";
$cfg['Categories']['list']['Amazon.it']['Kitchen']['node'] = "524016031";
$cfg['Categories']['list']['Amazon.it']['Kitchen']['nodes']['524016031'] = Array(
	'731676031'	=>	'Decorazioni per interni',
	'652615031'	=>	'Stoviglie',
	'731677031'	=>	'Tessili per la casa',
	);
// Amazon.it: DVD
$cfg['Categories']['list']['Amazon.it']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.it']['DVD']['node'] = "412607031";
$cfg['Categories']['list']['Amazon.it']['DVD']['nodes']['412607031'] = Array(
	'489064031' =>	'Commedie',
	'489066031'	=>	'Drammatici',
	'435482031'	=>	'Serie TV',
	);
// Amazon.it: Electronics
$cfg['Categories']['list']['Amazon.it']['Electronics']['name'] = "Elettronica";
$cfg['Categories']['list']['Amazon.it']['Electronics']['node'] = "412610031";
$cfg['Categories']['list']['Amazon.it']['Electronics']['nodes']['412610031'] = Array(
	'435505031' =>	'Foto e videocamere',
	'473601031'	=>	'Informatica',
	'473238031'	=>	'Telefonia',
	);
// Amazon.it: ForeignBooks
$cfg['Categories']['list']['Amazon.it']['ForeignBooks']['name'] = "English Books";
$cfg['Categories']['list']['Amazon.it']['ForeignBooks']['node'] = "433843031";
$cfg['Categories']['list']['Amazon.it']['ForeignBooks']['nodes']['433843031'] = Array(
	'509265031'	=>	'Humour',
	'509215031'	=>	'Narrativa',
	'509220031'	=>	'Romanzi rosa',
	);
// Amazon.it: Garden
$cfg['Categories']['list']['Amazon.it']['Garden']['name'] = "Giardino";
$cfg['Categories']['list']['Amazon.it']['Garden']['node'] = "635017031";
$cfg['Categories']['list']['Amazon.it']['Garden']['nodes']['635017031'] = Array(
	'731468031' =>	'Arredamento da giardino',
	'731467031'	=>	'Decorazioni per il giardino',
	'731469031' =>	'Utensili per il giardinaggio',
	);
// Amazon.it: Toys
$cfg['Categories']['list']['Amazon.it']['Toys']['name'] = "Giocattoli";
$cfg['Categories']['list']['Amazon.it']['Toys']['node'] = "523998031";
$cfg['Categories']['list']['Amazon.it']['Toys']['nodes']['523998031'] = Array(
	'632667031' =>	'Bambole e accessori',
	'632623031'	=>	'Costruzioni',
	'632649031' =>	'Modellini e veicoli',
	);
// Amazon.it: Books
$cfg['Categories']['list']['Amazon.it']['Books']['name'] = "Libri";
$cfg['Categories']['list']['Amazon.it']['Books']['node'] = "411664031";
$cfg['Categories']['list']['Amazon.it']['Books']['nodes']['411664031'] = Array(
	'508820031'	=>	'Humour',
	'508770031' =>	'Narrativa',
	'508775031'	=>	'Romanzi rosa',
	);
// Amazon.it: Music
$cfg['Categories']['list']['Amazon.it']['Music']['name'] = "Musica";
$cfg['Categories']['list']['Amazon.it']['Music']['node'] = "412601031";
$cfg['Categories']['list']['Amazon.it']['Music']['nodes']['412601031'] = Array(
	'489722031' =>	'Dance ed Elettronica',
	'489784031'	=>	'Pop',
	'489814031' =>	'Rock',
	);
// Amazon.it: Watches
$cfg['Categories']['list']['Amazon.it']['Watches']['name'] = "Orologi";
$cfg['Categories']['list']['Amazon.it']['Watches']['node'] = "524010031";
$cfg['Categories']['list']['Amazon.it']['Watches']['nodes']['524010031'] = Array(
	'601913031' =>	'Orologi da polso',
	'601912031'	=>	'Orologi da tasca',
	'601911031'	=>	'Orologi di lusso',
	);
// Amazon.it: Shoes
$cfg['Categories']['list']['Amazon.it']['Shoes']['name'] = "Scarpe e borse";
$cfg['Categories']['list']['Amazon.it']['Shoes']['node'] = "524007031";
$cfg['Categories']['list']['Amazon.it']['Shoes']['nodes']['524007031'] = Array(
	'700516031' =>	'Borse',
	'700578031'	=>	'Scarpe',
	'700542031' =>	'Valigeria e accessori viaggio',
	);
// Amazon.it: Software
$cfg['Categories']['list']['Amazon.it']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.it']['Software']['node'] = "412613031";
$cfg['Categories']['list']['Amazon.it']['Software']['nodes']['412613031'] = Array(
	'486461031' =>	'Computer Security & Privacy',
	'486549031'	=>	'Foto, media e design',
	'486541031' =>	'Sistemi operativi',
	);
// Amazon.it: Video Games
$cfg['Categories']['list']['Amazon.it']['VideoGames']['name'] = "Videogiochi";
$cfg['Categories']['list']['Amazon.it']['VideoGames']['node'] = "412604031";
$cfg['Categories']['list']['Amazon.it']['VideoGames']['nodes']['412604031'] = Array(
	'435495031' =>	'Giochi per PC e Mac',
	'435490031'	=>	'PlayStation 3',
	'435492031'	=>	'Wii',
	'435491031'	=>	'Xbox 360',
	);

/////////////////////////
// Categories: Amazon.cn

// Amazon.cn: Apparel
$cfg['Categories']['list']['Amazon.cn']['Apparel']['name'] = "服装";
$cfg['Categories']['list']['Amazon.cn']['Apparel']['node'] = "2016157051";
$cfg['Categories']['list']['Amazon.cn']['Apparel']['nodes']['2016157051'] = Array(
	'2152154051' => '女装',
	'2152155051' => '男装',
	'2152156051' => '女童服装'
	);
// Amazon.cn: Appliances
$cfg['Categories']['list']['Amazon.cn']['Appliances']['name'] = "家电";
$cfg['Categories']['list']['Amazon.cn']['Appliances']['node'] = "80208071";
$cfg['Categories']['list']['Amazon.cn']['Appliances']['nodes']['80208071'] = Array(
	'81948071' => '冰箱',
	'81949071' => '冰柜',
	'2121147051' => '洗衣机\烘干机'
	);
// Amazon.cn: Automotive
$cfg['Categories']['list']['Amazon.cn']['Automotive']['name'] = "汽车";
$cfg['Categories']['list']['Amazon.cn']['Automotive']['node'] = "1947900051";
$cfg['Categories']['list']['Amazon.cn']['Automotive']['nodes']['1947900051'] = Array(
	'2127789051' => 'GPS导航\配件',
	'2127790051' => '车载影音',
	'1947902051' => '内部饰品\用品\设备'
	);
// Amazon.cn: Baby
$cfg['Categories']['list']['Amazon.cn']['Baby']['name'] = "婴儿";
$cfg['Categories']['list']['Amazon.cn']['Baby']['node'] = "42693071";
$cfg['Categories']['list']['Amazon.cn']['Baby']['nodes']['42693071'] = Array(
	'79139071' => '婴幼儿尿裤',
	'79146071' => '婴儿护理',
	'79182071' => '安全防护'
	);
// Amazon.cn: Beauty
$cfg['Categories']['list']['Amazon.cn']['Beauty']['name'] = "美容";
$cfg['Categories']['list']['Amazon.cn']['Beauty']['node'] = "746777051";
$cfg['Categories']['list']['Amazon.cn']['Beauty']['nodes']['746777051'] = Array(
	'746781051' => '香水\香氛',
	'746782051' => '面部护理',
	'747007051' => '眼部护理'
	);
// Amazon.cn: Books
$cfg['Categories']['list']['Amazon.cn']['Books']['name'] = "书籍";
$cfg['Categories']['list']['Amazon.cn']['Books']['node'] = "658391051";
$cfg['Categories']['list']['Amazon.cn']['Books']['nodes']['658391051'] = Array(
	'658393051' => '小说',
	'658394051' => '文学',
	'658395051' => '艺术'
	);
// Amazon.cn: Electronics
$cfg['Categories']['list']['Amazon.cn']['Electronics']['name'] = "电子产品";
$cfg['Categories']['list']['Amazon.cn']['Electronics']['node'] = "2016117051";
$cfg['Categories']['list']['Amazon.cn']['Electronics']['nodes']['2016117051'] = Array(
	'664978051' => '手机／通讯',
	'755653051' => '摄影／摄像',
	'760233051' => '数码影音'
	);
// Amazon.cn: Grocery
$cfg['Categories']['list']['Amazon.cn']['Grocery']['name'] = "杂货";
$cfg['Categories']['list']['Amazon.cn']['Grocery']['node'] = "2127216051";
$cfg['Categories']['list']['Amazon.cn']['Grocery']['nodes']['2127216051'] = Array(
	'2141094051' => '饮料',
	'2140457051' => '茶',
	'43234071' => '酒'
	);
// Amazon.cn: Health
$cfg['Categories']['list']['Amazon.cn']['HealthPersonalCare']['name'] = "健康";
$cfg['Categories']['list']['Amazon.cn']['HealthPersonalCare']['node'] = "852804051";
$cfg['Categories']['list']['Amazon.cn']['HealthPersonalCare']['nodes']['852804051'] = Array(
	'853744051' => '美体塑身',
	'836322051' => '成人用品',
	'2133896051' => '隐形眼镜'
	);
// Amazon.cn: HomeGarden
$cfg['Categories']['list']['Amazon.cn']['Home']['name'] = "首页";
$cfg['Categories']['list']['Amazon.cn']['Home']['node'] = "2016127051";
$cfg['Categories']['list']['Amazon.cn']['Home']['nodes']['2016127051'] = Array(
	'813108051' => '厨具',
	'814224051' => '小家电',
	'831780051' => '家居'
	);
// Amazon.cn: HomeImprovement
$cfg['Categories']['list']['Amazon.cn']['HomeImprovement']['name'] = "家装";
$cfg['Categories']['list']['Amazon.cn']['HomeImprovement']['node'] = "1952921051";
$cfg['Categories']['list']['Amazon.cn']['HomeImprovement']['nodes']['1952921051'] = Array(
	'1952934051' => '建筑材料',
	'1952925051' => '电气',
	'1952923051' => '五金'
	);
// Amazon.cn: Jewelry
$cfg['Categories']['list']['Amazon.cn']['Jewelry']['name'] = "珠宝";
$cfg['Categories']['list']['Amazon.cn']['Jewelry']['node'] = "816483051";
$cfg['Categories']['list']['Amazon.cn']['Jewelry']['nodes']['816483051'] = Array(
	'816602051' => '项链\吊坠',
	'816603051' => '戒指',
	'816604051' => '手链\手镯'
	);
// Amazon.cn: Music
$cfg['Categories']['list']['Amazon.cn']['Music']['name'] = "音乐";
$cfg['Categories']['list']['Amazon.cn']['Music']['node'] = "754387051";
$cfg['Categories']['list']['Amazon.cn']['Music']['nodes']['754387051'] = Array(
	'754389051' => '流行音乐',
	'754391051' => '古典',
	'754393051' => '休闲音乐'
	);
// Amazon.cn: OfficeProducts
$cfg['Categories']['list']['Amazon.cn']['OfficeProducts']['name'] = "办公用品";
$cfg['Categories']['list']['Amazon.cn']['OfficeProducts']['node'] = "2127222051";
$cfg['Categories']['list']['Amazon.cn']['OfficeProducts']['nodes']['2127222051'] = Array(
	'2142103051' => '办公设备',
	'2142225051' => '日常办公用品',
	'2142127051' => '办公耗材'
	);
// Amazon.cn: Photography
$cfg['Categories']['list']['Amazon.cn']['Photo']['name'] = "摄影";
$cfg['Categories']['list']['Amazon.cn']['Photo']['node'] = "755653051";
$cfg['Categories']['list']['Amazon.cn']['Photo']['nodes']['755653051'] = Array(
	'755654051' => '数码照相机',
	'755655051' => '配件\镜头\周边',
	'755657051' => '数码摄像机'
	);
// Amazon.cn: Shoes
$cfg['Categories']['list']['Amazon.cn']['Shoes']['name'] = "鞋";
$cfg['Categories']['list']['Amazon.cn']['Shoes']['node'] = "2029190051";
$cfg['Categories']['list']['Amazon.cn']['Shoes']['nodes']['2029190051'] = Array(
	'2112003051' => '女鞋',
	'2112046051' => '男鞋',
	'2112084051' => '童鞋'
	);
// Amazon.cn: Software
$cfg['Categories']['list']['Amazon.cn']['Software']['name'] = "软件";
$cfg['Categories']['list']['Amazon.cn']['Software']['node'] = "863873051";
$cfg['Categories']['list']['Amazon.cn']['Software']['nodes']['863873051'] = Array(
	'2157035051' => '计算机安全',
	'863912051' => '操作系统',
	'863913051' => '办公软件'
	);
// Amazon.cn: SportingGoods
$cfg['Categories']['list']['Amazon.cn']['SportingGoods']['name'] = "体育用品";
$cfg['Categories']['list']['Amazon.cn']['SportingGoods']['node'] = "836313051";
$cfg['Categories']['list']['Amazon.cn']['SportingGoods']['nodes']['836313051'] = Array(
	'836334051' => '乒乓球',
	'836330051' => '羽毛球',
	'836333051' => '网球'
	);
// Amazon.cn: Toys
$cfg['Categories']['list']['Amazon.cn']['Toys']['name'] = "玩具";
$cfg['Categories']['list']['Amazon.cn']['Toys']['node'] = "647071051";
$cfg['Categories']['list']['Amazon.cn']['Toys']['nodes']['647071051'] = Array(
	'1982054051' => '婴幼玩具',
	'2039913051' => '儿童家具',
	'1982057051' => '电动玩具'
	);
// Amazon.cn: Video
$cfg['Categories']['list']['Amazon.cn']['Video']['name'] = "视频";
$cfg['Categories']['list']['Amazon.cn']['Video']['node'] = "2016137051";
$cfg['Categories']['list']['Amazon.cn']['Video']['nodes']['2016137051'] = Array(
	'811074051' => '影视',
	'897771051' => '教育音像',
	);
// Amazon.cn: VideoGames
$cfg['Categories']['list']['Amazon.cn']['VideoGames']['name'] = "视频游戏";
$cfg['Categories']['list']['Amazon.cn']['VideoGames']['node'] = "897416051";
$cfg['Categories']['list']['Amazon.cn']['VideoGames']['nodes']['897416051'] = Array(
	'897470051' => '角色扮演游戏',
	'897474051' => '动作_冒险_射击游戏',
	'897473051' => '策略游戏'
	);
// Amazon.cn: Watches
$cfg['Categories']['list']['Amazon.cn']['Watches']['name'] = "手表";
$cfg['Categories']['list']['Amazon.cn']['Watches']['node'] = "1953165051";
$cfg['Categories']['list']['Amazon.cn']['Watches']['nodes']['1953165051'] = Array(
	'2040033051' => '腕表',
	'816492051' => '钟',
	'1982068051' => '儿童钟表'
	);

/////////////////////////
// Categories: Amazon.es

// Amazon.es: Automotive
$cfg['Categories']['list']['Amazon.es']['Automotive']['name'] = "Automotor";
$cfg['Categories']['list']['Amazon.es']['Automotive']['node'] = "1951051031";
$cfg['Categories']['list']['Amazon.es']['Automotive']['nodes']['1951051031'] = Array(
    '2424746031' => 'Accesorios para coche',
    '1461677031' => 'Electrónica para coche',
    '2424910031' => 'Neumáticos y llantas para coche'
    );
// Amazon.es: Baby
$cfg['Categories']['list']['Amazon.es']['Baby']['name'] = "Bebé";
$cfg['Categories']['list']['Amazon.es']['Baby']['node'] = "1703495031";
$cfg['Categories']['list']['Amazon.es']['Baby']['nodes']['1703495031'] = Array(
    '1862261031' => 'Carritos y sillas de paseo',
    '1642110031' => 'Bebés y primera infancia',
    '1862257031' => 'Sillas de coche y accesorios'
    );
// Amazon.es: Kitchen
$cfg['Categories']['list']['Amazon.es']['Kitchen']['name'] = "Cocina";
$cfg['Categories']['list']['Amazon.es']['Kitchen']['node'] = "599392031";
$cfg['Categories']['list']['Amazon.es']['Kitchen']['nodes']['599392031'] = Array(
    '675728031' => 'Pequeño Electrodoméstico'
    );
// Amazon.es: DVD
$cfg['Categories']['list']['Amazon.es']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.es']['DVD']['node'] = "599380031";
$cfg['Categories']['list']['Amazon.es']['DVD']['nodes']['599380031'] = Array(
	'916701031' => 'Películas',
	'665293031' => 'TV'
	);
// Amazon.es: Electronics
$cfg['Categories']['list']['Amazon.es']['Electronics']['name'] = "Electrónica";
$cfg['Categories']['list']['Amazon.es']['Electronics']['node'] = "667050031";
$cfg['Categories']['list']['Amazon.es']['Electronics']['nodes']['667050031'] = Array(
	'937753031' => 'Accesorios',
	'937912031' => 'Componentes',
	'938008031' => 'Portátiles'
	);
// Amazon.es: Toys
$cfg['Categories']['list']['Amazon.es']['Toys']['name'] = "Juguetes";
$cfg['Categories']['list']['Amazon.es']['Toys']['node'] = "599386031";
$cfg['Categories']['list']['Amazon.es']['Toys']['nodes']['599386031'] = Array(
    '943800031' => 'Juguetes'
    );
// Amazon.es: VideoGames
$cfg['Categories']['list']['Amazon.es']['VideoGames']['name'] = "Juegos video";
$cfg['Categories']['list']['Amazon.es']['VideoGames']['node'] = "599383031";
$cfg['Categories']['list']['Amazon.es']['VideoGames']['nodes']['599383031'] = Array(
    '664652031' => 'PlayStation 3',
    '664654031' => 'Wii',
    '664653031' => 'Xbox 360'
    );
// Amazon.es: KindleStore
$cfg['Categories']['list']['Amazon.es']['KindleStore']['name'] = "eBooks Kindle";
$cfg['Categories']['list']['Amazon.es']['KindleStore']['node'] = "827231031";
$cfg['Categories']['list']['Amazon.es']['KindleStore']['nodes']['827231031'] = Array(
    '1335546031'  => 'Biografías',
    '1335565031' => 'Literatura y ficción',
    '1335567031'  => 'Política'
    );
// Amazon.es: Books
$cfg['Categories']['list']['Amazon.es']['Books']['name'] = "Libros";
$cfg['Categories']['list']['Amazon.es']['Books']['node'] = "599365031";
$cfg['Categories']['list']['Amazon.es']['Books']['nodes']['599365031'] = Array(
    '902600031' => 'Historia',
    '902674031' => 'Literatura y ficción',
    '902702031' => 'Religión'
    );
// Amazon.es: ForeignBooks
$cfg['Categories']['list']['Amazon.es']['ForeignBooks']['name'] = "Libros extranjeros";
$cfg['Categories']['list']['Amazon.es']['ForeignBooks']['node'] = "599368031";
$cfg['Categories']['list']['Amazon.es']['ForeignBooks']['nodes']['599368031'] = Array(
	'903393031' => 'Historia',
	'903467031' => 'Literatura y ficción',
	'903495031' => 'Religión'
	);
// Amazon.es: Music
$cfg['Categories']['list']['Amazon.es']['Music']['name'] = "Música";
$cfg['Categories']['list']['Amazon.es']['Music']['node'] = "599374031";
$cfg['Categories']['list']['Amazon.es']['Music']['nodes']['599374031'] = Array(
	'917395031' => 'Flamenco y folclore español',
	'917456031' => 'Pop',
	'917489031' => 'Rock'
	);
// Amazon.es: MP3Downloads
$cfg['Categories']['list']['Amazon.es']['MP3Downloads']['name'] = "MP3";
$cfg['Categories']['list']['Amazon.es']['MP3Downloads']['node'] = "1748201031";
$cfg['Categories']['list']['Amazon.es']['MP3Downloads']['nodes']['1748201031'] = Array(
    '1806794031' => 'Dance y electrónica',
    '1806804031' => 'Música latina',
    '1806808031' => 'Pop',
    );
// Amazon.es: Watches
$cfg['Categories']['list']['Amazon.es']['Watches']['name'] = "Relojes";
$cfg['Categories']['list']['Amazon.es']['Watches']['node'] = "599389031";
$cfg['Categories']['list']['Amazon.es']['Watches']['nodes']['599389031'] = Array(
    '951037031' => 'Relojes',
    );
// Amazon.es: Software
$cfg['Categories']['list']['Amazon.es']['Software']['name'] = "Software";
$cfg['Categories']['list']['Amazon.es']['Software']['node'] = "599377031";
$cfg['Categories']['list']['Amazon.es']['Software']['nodes']['599377031'] = Array(
	'665284031' => 'Foto, multimedia y diseño',
	'912845031' => 'Programación y desarrollo web',
	'665497031' => 'Sistemas operativos'
	);
// Amazon.es: Shoes
$cfg['Categories']['list']['Amazon.es']['Shoes']['name'] = "Zapatos";
$cfg['Categories']['list']['Amazon.es']['Shoes']['node'] = "1571263031";
$cfg['Categories']['list']['Amazon.es']['Shoes']['nodes']['1571263031'] = Array(
    '2008145031' => 'Zapatos para hombre',
    '2008205031' => 'Zapatos para mujer',
    '2008083031' => 'Zapatos para niña'
    );

/////////////////////////
// Categories: Amazon.in

// Amazon.in: Books
$cfg['Categories']['list']['Amazon.in']['Books']['name'] = "Books";
$cfg['Categories']['list']['Amazon.in']['Books']['node'] = "976390031";
$cfg['Categories']['list']['Amazon.in']['Books']['nodes']['976390031'] = Array(
	'1318068031' =>	'Business & Economics',
	'1318157031' =>	'Literature & Fiction',
	'1318202031' =>	'School Books',
	);
// Amazon.in: DVD
$cfg['Categories']['list']['Amazon.in']['DVD']['name'] = "DVD";
$cfg['Categories']['list']['Amazon.in']['DVD']['node'] = "976417031";
$cfg['Categories']['list']['Amazon.in']['DVD']['nodes']['976417031'] = Array(
	'1375484031' =>  'Movies',
	'1375485031' =>  'TV Shows',
	);
// Amazon.in: Electronics
$cfg['Categories']['list']['Amazon.in']['Electronics']['name'] = "Electronics";
$cfg['Categories']['list']['Amazon.in']['Electronics']['node'] = "976420031";
$cfg['Categories']['list']['Amazon.in']['Electronics']['nodes']['976420031'] = Array(
	'1388977031' =>	'Cameras & Photography',
	'1458204031' =>	'Computers & Accessories',
	'1389401031' =>	'Mobiles & Accessories',
	);

// sort
$cfg['Sort']['section'] = "-999";
$cfg['Sort']['required'] = TRUE;

////////////////////
// SORT: Amazon.com
$cfg['Sort']['list']['Amazon.com']['Apparel'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'-launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'pricerank' 		=> 'Price (Low to High)',
	'inverseprice'		=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Appliances'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['ArtsAndCrafts'] = Array(
	'relevancerank' 	=> 'Relevance',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Automotive'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Baby'] = Array(
	'psrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['Beauty'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'-launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Books'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Collectibles'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'reviewrank'        => 'Reviews (High to Low)',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    );
$cfg['Sort']['list']['Amazon.com']['DVD'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-video-release-date' 	=> 'Release Date (Newer to Older)',
	'-releasedate'		=> 'Release Date (Older to Newer)',
	);
$cfg['Sort']['list']['Amazon.com']['Electronics'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['GiftCards'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank'        => 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)'
	);
$cfg['Sort']['list']['Amazon.com']['GourmetFood'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'pricerank' 		=> 'Price (Low to High)',
	'inverseprice'		=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Grocery'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'inverseprice'		=> 'Price (Low to High)',
	'pricerank' 		=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['HealthPersonalCare'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'inverseprice' 		=> 'Price (Low to High)',
	'pricerank'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['HomeGarden'] = Array(
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Industrial'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Jewelry'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'launch-date' 		=> 'Newest Arrivals',
	'pricerank' 		=> 'Price (Low to High)',
	'inverseprice'		=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['KindleStore'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'pricerank' 			=> 'Price (Low to High)',
	'inverse-pricerank'			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Kitchen'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['LawnAndGarden'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'reviewrank' 		=> 'Reviews (High to Low)'
	);
$cfg['Sort']['list']['Amazon.com']['Magazines'] = Array(
	'subslot-salesrank'	=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.com']['MobileApps'] = Array(
	'relevancerank' 	=> 'Relevance',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'reviewrank' 		=> 'Reviews (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['MP3Downloads'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-releasedate'		=> 'Release Date (Older to Newer)',
	);
$cfg['Sort']['list']['Amazon.com']['Music'] = Array(
	'psrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'artistrank'		=> 'Artist Name',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	'orig-rel-date'		=> 'Original Release Date (Newer to Older)',
	'release-date'		=> 'Release Date (Newer to Older)',
	'-releasedate'		=> 'Release Date (Older to Newer)',
	);
$cfg['Sort']['list']['Amazon.com']['MusicalInstruments'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'-launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['OfficeProducts'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['OutdoorLiving'] = Array(
	'psrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['PCHardware'] = Array(
	'psrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['PetSupplies'] = Array(
	'+pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Photo'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Shoes'] = Array(
	'pmrank' 			=> 'Relevance',
	'xsrelevancerank'	=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-launch-date' 		=> 'Newest Arrivals',
	);
$cfg['Sort']['list']['Amazon.com']['Software'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['SportingGoods'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'launch-date' 		=> 'Newest Arrivals',
	'sale-flag' 		=> 'On Sale',
	'inverseprice' 		=> 'Price (Low to High)',
	'pricerank'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Tools'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.com']['Toys'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-age-min' 			=> 'Age (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['UnboxVideo'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-video-release-date' 	=> 'Release Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.com']['VHS'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-video-release-date' 	=> 'Release Date (Newer to Older)',
	'-releasedate'		=> 'Release Date (Older to Newer)',
	);
$cfg['Sort']['list']['Amazon.com']['VideoGames'] = Array(
	'pmrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.com']['Watches'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.com']['Wireless'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank' => 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	'daterank' 			=> 'Release Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.com']['WirelessAccessories'] = Array(
	'psrank' 			=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);

//////////////////////
// SORT: Amazon.co.uk
$cfg['Sort']['list']['Amazon.co.uk']['Apparel'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-launch-date' 		=> 'Newest Arrivals',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Automotive'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Baby'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Beauty'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Books'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'pubdate' 			=> 'Publication Date (Older to Newer)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['DVD'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	'daterank' 			=> 'Release Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Electronics'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Grocery'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['HealthPersonalCare'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['HomeGarden'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['HomeImprovement'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Jewelry'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-launch-date' 		=> 'Newest Arrivals',
	);
$cfg['Sort']['list']['Amazon.co.uk']['KindleStore'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Kitchen'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Lighting'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['MP3Downloads'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-releasedate'		=> 'Release Date (Older to Newer)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Music'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'releasedate' 	    => 'Release Date (Newer to Older)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank' => 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['MusicalInstruments'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'reviewrank'        => 'Reviews (High to Low)',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    );
$cfg['Sort']['list']['Amazon.co.uk']['OfficeProducts'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price' 			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['OutdoorLiving'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price' 			=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Shoes'] = Array(
	'pmrank' 			=> 'Relevance',
	'xsrelevancerank'	=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'-launch-date' 		=> 'Newest Arrivals',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Software'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['SportingGoods'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price' 			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Tools'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'daterank' 			=> 'Release Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Toys'] = Array(
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'mfg-age-min' 		=> 'Age (Low to High)',
	'-mfg-age-min' 		=> 'Age (High to Low)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['VHS'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['VideoGames'] = Array(
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.co.uk']['Watches'] = Array(
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);

///////////////////
// SORT: Amazon.ca
$cfg['Sort']['list']['Amazon.ca']['Baby'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    'reviewrank'        => 'Reviews (High to Low)',
    );
$cfg['Sort']['list']['Amazon.ca']['Beauty'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    'reviewrank'        => 'Reviews (High to Low)',
    );
$cfg['Sort']['list']['Amazon.ca']['Books'] = Array(
	'salesrank'			=> 'Bestselling',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'daterank' 			=> 'Publication Date (Newer to Older)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.ca']['DVD'] = Array(
	'salesrank'			=> 'Bestselling',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.ca']['Electronics'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.ca']['HealthPersonalCare'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    'reviewrank'        => 'Reviews (High to Low)',
    );
$cfg['Sort']['list']['Amazon.ca']['KindleStore'] = Array(
    'salesrank'         => 'Bestselling',
    'pricerank'         => 'Price (Low to High)',
    'inverse-pricerank' => 'Price (High to Low)',
    'daterank'          => 'Publication Date (Newer to Older)',
    'titlerank'         => 'Alphabetical (A-Z)',
    );
$cfg['Sort']['list']['Amazon.ca']['Kitchen'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.ca']['LawnAndGarden'] = Array(
    'relevancerank'     => 'Relevance',
    'salesrank'         => 'Bestselling',
    'reviewrank'        => 'Reviews (High to Low)',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    );
$cfg['Sort']['list']['Amazon.ca']['Music'] = Array(
	'salesrank'			=> 'Bestselling',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'orig-rel-date' 	=> 'Publication Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.ca']['PetSupplies'] = Array(
    'relevance'         => 'Relevance',
    'salesrank'         => 'Bestselling',
    'reviewrank'        => 'Reviews (High to Low)',
    'price'             => 'Price (Low to High)',
    '-price'            => 'Price (High to Low)',
    );
$cfg['Sort']['list']['Amazon.ca']['Software'] = Array(
	'salesrank'			=> 'Bestselling',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'-daterank' 		=> 'Publication Date (Older to Newer)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	);
$cfg['Sort']['list']['Amazon.ca']['SportingGoods'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.ca']['VHS'] = Array(
	'salesrank'			=> 'Bestselling',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);
$cfg['Sort']['list']['Amazon.ca']['VideoGames'] = Array(
	'salesrank'			=> 'Bestselling',
	'pricerank' 		=> 'Price (Low to High)',
	'inverse-pricerank'	=> 'Price (High to Low)',
	'titlerank' 		=> 'Alphabetical (A-Z)',
	'-titlerank' 		=> 'Alphabetical (Z-A)',
	);

///////////////////
// SORT: Amazon.de
$cfg['Sort']['list']['Amazon.de']['Automotive'] = Array(
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['Apparel'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['Baby'] = Array(
	'psrank' 			=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	);
$cfg['Sort']['list']['Amazon.de']['Beauty'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['Books'] = Array(
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'pricerank' 		=> 'Preis (Aufsteigend)',
	'inverse-pricerank'	=> 'Preis (Absteigend)',
	'-pubdate' 			=> 'Erscheinungsdatum (neu bis älter)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['DVD'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Electronics'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['ForeignBooks'] = Array(
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'pricerank' 		=> 'Preis (Aufsteigend)',
	'inverse-pricerank'	=> 'Preis (Absteigend)',
	'-pubdate' 			=> 'Erscheinungsdatum (neu bis älter)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Grocery'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['HealthPersonalCare'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['HomeGarden'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Jewelry'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['KindleStore'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Kitchen'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Lighting'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['Magazines'] = Array(
	'salesrank'			=> 'Topseller',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['MP3Downloads'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'artistalbumrank' 	=> 'Künstler: A bis Z',
	'-artistalbumrank' 	=> 'Künstler: Z bis A',
	'albumrank' 		=> 'Album: A bis Z',
	'-albumrank' 		=> 'Album: Z bis A',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'-releasedate'		=> 'Erscheinungstermin',
	);
$cfg['Sort']['list']['Amazon.de']['Music'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'pubdate' 			=> 'Erscheinungsdatum (neu bis älter)',
	'-pubdate' 			=> 'Erscheinungsdatum (älter bis neu)',
	);
$cfg['Sort']['list']['Amazon.de']['MusicalInstruments'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'reviewrank' 		=> 'Kundenbewertung',
	);
$cfg['Sort']['list']['Amazon.de']['OfficeProducts'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	);
$cfg['Sort']['list']['Amazon.de']['OutdoorLiving'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['PCHardware'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Photo'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['Shoes'] = Array(
	'relevancerank' 	=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
 	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'-launch-date'		=> 'Erscheinungsdatum (neu bis älter)',
	);
$cfg['Sort']['list']['Amazon.de']['SportingGoods'] = Array(
	'salesrank'			=> 'Topseller',
	'reviewrank' 		=> 'Kundenbewertung',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'release-date' 		=> 'Erscheinungsdatum (neu bis älter)',
	'-release-date' 	=> 'Erscheinungsdatum (älter bis neu)',
	);
$cfg['Sort']['list']['Amazon.de']['Software'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'-date' 			=> 'Erscheinungsdatum (neu bis älter)',
	);
$cfg['Sort']['list']['Amazon.de']['Tools'] = Array(
	'pmrank' 			=> 'Gekennzeichnet',
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'-date' 			=> 'Erscheinungsdatum (neu bis älter)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	);
$cfg['Sort']['list']['Amazon.de']['Toys'] = Array(
	'+salesrank'		=> 'Topseller',
	'+price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'+titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'-date' 			=> 'Erscheinungsdatum (neu bis älter)',
	);
$cfg['Sort']['list']['Amazon.de']['VHS'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);
$cfg['Sort']['list']['Amazon.de']['VideoGames'] = Array(
	'salesrank'			=> 'Topseller',
	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	'-date' 			=> 'Erscheinungsdatum (neu bis älter)',
	);
$cfg['Sort']['list']['Amazon.de']['Watches'] = Array(
	'salesrank'			=> 'Topseller',
 	'price' 			=> 'Preis (Aufsteigend)',
	'-price'			=> 'Preis (Absteigend)',
	'titlerank' 		=> 'Alphabet (A bis Z)',
	'-titlerank' 		=> 'Alphabet (Z bis A)',
	);

///////////////////
// SORT: Amazon.fr
$cfg['Sort']['list']['Amazon.fr']['Apparel'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Beauty'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Baby'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	);
$cfg['Sort']['list']['Amazon.fr']['Books'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'pricerank' 		=> 'Prix (Croissant)',
	'inverse-pricerank'	=> 'Prix (Décroissant)',
	'-daterank' 		=> 'Date de publication (De la plus ancienne à la plus récente)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['PCHardware'] = Array(
	'psrank' 			=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['DVD'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['Electronics'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['ForeignBooks'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'pricerank' 		=> 'Prix (Croissant)',
	'inverse-pricerank'	=> 'Prix (Décroissant)',
	'-daterank' 		=> 'Date de publication (De la plus ancienne à la plus récente)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['HomeImprovement'] = Array(
    'relevancerank'     => 'Pertinence',
    'salesrank'         => 'Meilleures Ventes',
    'price'             => 'Prix (Croissant)',
    '-price'            => 'Prix (Décroissant)',
    'reviewrank'        => 'Note moyenne des commentaires',
    );
$cfg['Sort']['list']['Amazon.fr']['HealthPersonalCare'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Jewelry'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Kitchen'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	);
$cfg['Sort']['list']['Amazon.fr']['Lighting'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['MusicalInstruments'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['MP3Downloads'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'titlerank'			=> 'Titre chanson (A à Z)',
	'-titlerank'		=> 'Titre chanson (Z à A)',
	'artistalbumrank' 	=> 'Artiste (A à Z)',
	'-artistalbumrank' 	=> 'Artiste (Z à A)',
	'albumrank' 		=> 'Album (A à Z)',
	'-albumrank' 		=> 'Album (Z à A)',
	'relevancerank' 	=> 'Pertinence',
	'reviewrank'		=> 'Note moyenne des commentaires',
	'-releasedate'		=> 'Date de sortie',
	);
$cfg['Sort']['list']['Amazon.fr']['Music'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'pricerank' 		=> 'Prix (Croissant)',
	'inverse-pricerank'	=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['OfficeProducts'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Shoes'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Software'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-pricerank'		=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['SportingGoods'] = Array(
	'relevancerank' 	=> 'Pertinence',
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'reviewrank'		=> 'Note moyenne des commentaires',
	);
$cfg['Sort']['list']['Amazon.fr']['Toys'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
 	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
 	);
$cfg['Sort']['list']['Amazon.fr']['VHS'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);
$cfg['Sort']['list']['Amazon.fr']['VideoGames'] = Array(
	'salesrank'			=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	'date' 				=> 'Date (De la plus récente à la plus ancienne)',
	);
$cfg['Sort']['list']['Amazon.fr']['Watches'] = Array(
	'salesrank'		=> 'Meilleures Ventes',
	'price' 			=> 'Prix (Croissant)',
	'-price'			=> 'Prix (Décroissant)',
	'titlerank' 		=> 'Alphabétique (A à Z)',
	'-titlerank' 		=> 'Alphabétique (Z à A)',
	);

///////////////////
// SORT: Amazon.co.jp
$cfg['Sort']['list']['Amazon.co.jp']['Apparel'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Appliances'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Beauty'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Books'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['DVD'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Electronics'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Grocery'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['HealthPersonalCare'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['HomeImprovement'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Jewelry'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Kitchen'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['MP3Downloads'] = Array(
    'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Music'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['OfficeProducts'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Shoes'] = Array(
	'relevancerank' 	=> '&#12362;&#12377;&#12377;&#12417;&#21830;&#21697;',
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'reviewrank' 		=> '&#12524;&#12499;&#12517;&#12540;&#65288;[&#20302;]&#12395;&#39640;&#65289;',
	'-launch-date' 		=> 'Newest Arrivals',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Software'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['SportingGoods'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'release-date' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-release-date'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Toys'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['VHS'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['VideoGames'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'releasedate' 		=> '&#21476;&#12356;&#12395;&#30330;&#22770;&#26085;&#65288;&#26032;&#12375;&#12356;&#65289;',
	'-releasedate'		=> '&#12381;&#12428;&#20197;&#38477;&#12395;&#30330;&#22770;&#26085;&#65288;&#21476;&#12356;&#65289;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);
$cfg['Sort']['list']['Amazon.co.jp']['Watches'] = Array(
	'salesrank'			=> '&#22770;&#12428;&#12390;&#12356;&#12427;&#38918;&#30058;',
	'price' 			=> '&#20385;&#26684;&#12398;&#23433;&#12356;&#38918;&#30058;',
	'-price'			=> '&#20385;&#26684;&#12398;&#39640;&#12356;&#38918;&#30058;',
	'titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#26119;&#38918;',
	'-titlerank' 		=> '&#12450;&#12523;&#12501;&#12449;&#12505;&#12483;&#12488;&#38918;&#38477;&#38918;',
	);

///////////////////
// SORT: Amazon.it
$cfg['Sort']['list']['Amazon.it']['Books'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank_authority' 		=> 'Media recensioni clienti',
	'-pubdate' 		=> 'Data di pubblicazione'
	);
$cfg['Sort']['list']['Amazon.it']['DVD'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-releasedate'		=> 'Disponibile da'
	);
$cfg['Sort']['list']['Amazon.it']['Electronics'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	);
$cfg['Sort']['list']['Amazon.it']['ForeignBooks'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-pubdate' 			=> 'Data di pubblicazione'
	);
$cfg['Sort']['list']['Amazon.it']['Garden'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-pubdate' 			=> 'Data di pubblicazione'
	);
$cfg['Sort']['list']['Amazon.it']['Kitchen'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	);
$cfg['Sort']['list']['Amazon.it']['Music'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-releasedate'		=> 'Disponibile da'
	);
$cfg['Sort']['list']['Amazon.it']['Shoes'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	);
$cfg['Sort']['list']['Amazon.it']['Software'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-releasedate'		=> 'Disponibile da'
	);
$cfg['Sort']['list']['Amazon.it']['Toys'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	);
$cfg['Sort']['list']['Amazon.it']['VideoGames'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	'-releasedate'		=> 'Disponibile da'
	);
$cfg['Sort']['list']['Amazon.it']['Watches'] = Array(
	'salesrank'			=> 'Popolarità',
	'price' 			=> 'Prezzo: crescente',
	'-price'			=> 'Prezzo: decrescente',
	'reviewrank' 		=> 'Media recensioni clienti',
	);


///////////////////
// SORT: Amazon.cn
$cfg['Sort']['list']['Amazon.cn']['Apparel'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	'-pct-off'			=> '折扣',
	'reviewrank_authority' 		=> '用户评分',
	'-launch-date' 		=> '上架时间',
	);
$cfg['Sort']['list']['Amazon.cn']['Appliances'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	'-pct-off'			=> '折扣',
	'reviewrank_authority' 		=> '用户评分',
	'-launch-date' 		=> '上架时间',
	);
$cfg['Sort']['list']['Amazon.cn']['Automotive'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	'-pct-off'			=> '折扣',
	'reviewrank_authority' 		=> '用户评分',
	'-launch-date' 		=> '上架时间',
	);
$cfg['Sort']['list']['Amazon.cn']['Baby'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	'reviewrank_authority' 		=> '用户评分',
	);
$cfg['Sort']['list']['Amazon.cn']['Beauty'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Books'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	'-publication_date' 			=> '出版日期'
	);
$cfg['Sort']['list']['Amazon.cn']['Electronics'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Grocery'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['HealthPersonalCare'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Home'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['HomeImprovement'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Jewelry'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Music'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['OfficeProducts'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Photo'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Shoes'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Software'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['SportingGoods'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Toys'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Video'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['VideoGames'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);
$cfg['Sort']['list']['Amazon.cn']['Watches'] = Array(
	'salesrank'			=> '受欢迎程度',
	'price' 			=> '价格：由低到高',
	'-price'			=> '价格：由高到低',
	);

///////////////////
// SORT: Amazon.es
$cfg['Sort']['list']['Amazon.es']['Automotive'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
    '-price'			=> 'Precio: de más alto a más bajo',
    'reviewrank_authority'	=> 'Opinión media de los clientes'
   	);
$cfg['Sort']['list']['Amazon.es']['Baby'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes'
	);
$cfg['Sort']['list']['Amazon.es']['Books'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-pubdate'		=> 'Fecha de publicación'
	);
$cfg['Sort']['list']['Amazon.es']['DVD'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-releasedate'		=> 'Fecha de lanzamiento'
	);
$cfg['Sort']['list']['Amazon.es']['Electronics'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     );
$cfg['Sort']['list']['Amazon.es']['ForeignBooks'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-pubdate'		=> 'Fecha de publicación'
	);
$cfg['Sort']['list']['Amazon.es']['KindleStore'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-pubdate'		=> 'Fecha de publicación'
	);
$cfg['Sort']['list']['Amazon.es']['Kitchen'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     );
$cfg['Sort']['list']['Amazon.es']['Music'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-releasedate'		=> 'Fecha de lanzamiento'
	);
$cfg['Sort']['list']['Amazon.es']['MP3Downloads'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-releasedate'		=> 'Fecha de lanzamiento'
	);
$cfg['Sort']['list']['Amazon.es']['Software'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-releasedate'		=> 'Fecha de lanzamiento'
	);
$cfg['Sort']['list']['Amazon.es']['Toys'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     );
$cfg['Sort']['list']['Amazon.es']['VideoGames'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     '-releasedate'		=> 'Fecha de lanzamiento'
	);
$cfg['Sort']['list']['Amazon.es']['Watches'] = Array(
	'relevancerank'	=> 'Importancia',
	'salesrank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
     '-price'			=> 'Precio: de más alto a más bajo',
     'reviewrank_authority'	=> 'Opinión media de los clientes',
     );
$cfg['Sort']['list']['Amazon.es']['Shoes'] = Array(
	'relevancerank'	=> 'Importancia',
	'popularity-rank'		=> 'Popularidad',
	'price'			=> 'Precio: de más bajo a más alto',
    '-price'			=> 'Precio: de más alto a más bajo',
    'reviewrank_authority'	=> 'Opinión media de los clientes',
	'-launch-date'		=> 'Fecha de lanzamiento'
    );


///////////////////
// SORT: Amazon.in
$cfg['Sort']['list']['Amazon.in']['Books'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);
$cfg['Sort']['list']['Amazon.in']['DVD'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'releasedate'		=> 'Release Date (Older to Newer)',
	'daterank' 			=> 'Release Date (Newer to Older)',
	);
$cfg['Sort']['list']['Amazon.in']['Electronics'] = Array(
	'relevancerank' 	=> 'Relevance',
	'salesrank'			=> 'Bestselling',
	'reviewrank' 		=> 'Reviews (High to Low)',
	'price' 			=> 'Price (Low to High)',
	'-price'			=> 'Price (High to Low)',
	);

?>